var ataJSON = [];
var atdJSON = [];
var lastMileRateJSON = [];
var originToDestJSON = [];
var prefixTabsListName = 'MGF_RFQ_AFF_';
var prefixSuprtListName = 'LGS_ARFQ_';
var supportListOriginDest = [];

var supportListAFFMapCode = '';
var masterList = [];
var RFQ_ID = 101;
var summaryData = ['Lowest Bidder', 'Lowest Rate', '2nd Lowest Bidder', '2nd Lowest Rate'];
var zones = ['zone 1', 'zone 2', 'zone 3', 'zone 4', 'zone 5'];
var overAll = ['Count of Lowest Bidder', 'Frequency', '2nd Count of Lowest Bidder', 'Frequency','Count of 2nd Lowest Bidder', 'Frequency'];

var ataRateLowestBidder;
var ataRateFreq = 0;
var atdRateLowestBidder;
var atdRateFreq = 0;
var lastMileLowestBidder;
var lastMileFreq = 0;
var originToDestLowestBidder;
var originToDestFreq = 0;

NWF$(document).ready(function () {

    var header = '<tr><th>Overall</th><th></th></tr>';
    NWF$("#overAllT thead").append(header);

    var tBody = '';
    overAll.forEach(element => {
        tBody+='<tr>';
        tBody+='<td class="overAllTxt">'+element+'</td>';
        tBody+='<td class="overAllVal"></td>';
        tBody+='</tr>';
    });

    NWF$("#overAllT tbody").append(tBody);

    NWF$("#" + DeptRole2).change(function () {
        NWF$(".nd-dialog-panel").css("display","block");
        GetMasterRecord();
        GetAFFLaneDTKEYAsync();
    });

    NWF$(".customtab").change(function (e) {

        if (e.target.defaultValue == 'All in rate comparison - ATA Rate table') {
           if(ataJSON.length == 0) {
               retrieveATARate();
           }
           overAllUpdate(ataRateFreq, ataRateLowestBidder);
        } else if (e.target.defaultValue == 'All in rate comparison - ATD Rate table') {
            if(atdJSON.length == 0) {
                retrieveATDRate();
            }
            overAllUpdate(atdRateFreq, atdRateLowestBidder);
        } else if (e.target.defaultValue == 'All in rate comparison - Last mile Rate table') {
            if(lastMileRateJSON.length == 0) {
                retrieveLastMile();
            }
           overAllUpdate(lastMileFreq, lastMileLowestBidder);
        } else if (e.target.defaultValue == 'From Origin to Destination CFS') {
            if(originToDestJSON.length == 0) {
                retrieveOriginToDestination();
            }
           overAllUpdate(originToDestFreq, originToDestLowestBidder);
        }


    });


});


function retrieveATARate() {


    ataJSON = [];

    for (let ia = 0; ia < AirRFQJSON.length; ia++) {
        const elementAirRFQ = AirRFQJSON[ia];

        if (elementAirRFQ['ATARate'] > 0) {

            for (let index = 0; index < supportListOriginDest.length; index++) {
                const element = supportListOriginDest[index];
                if (elementAirRFQ['OriginCity'] == element['Origin'] && elementAirRFQ['Dstn'] == element['Dstn']) {
                    ataJSON.push({
                        'Carrier': elementAirRFQ['AFF'],
                        'Origin Country': elementAirRFQ['OriginCountry'],
                        'Origin City': elementAirRFQ['OriginCity'],
                        'Destination': elementAirRFQ['Dstn'],
                        'Service Level': elementAirRFQ['ServiceLevel'],
                        'ATARate': elementAirRFQ['ATARate'],

                    })
                    break;
                }

            }

        }

    }

    var holdServiceLevel = [];
    var holdOriginCity = [];
    ataJSON.forEach(element => {
        
        if(holdServiceLevel.indexOf(element['Service Level']) == -1) {
            holdServiceLevel.push(element['Service Level']);
        }

        if(holdOriginCity.indexOf(element['Origin City']) == -1) {
            holdOriginCity.push(element['Origin City']);
        }
        
    });

   var thservice = '';
    holdServiceLevel.forEach(element => {
        thservice+= '<th class = "serviceLevel '+element+'">';
        thservice+= element;
        thservice+= '</th>';
    });

    var header = '<tr><th>Carrier</th><th>Origin Country</th><th>Origin City</th><th>Destination</th>'+thservice+'</tr>';

    NWF$("#ataRateComparison thead").append(header);

    var tBody = '';
    ataJSON.forEach(element=>{

        var checkDestAndOriginCity = false;
        NWF$("#ataRateComparison tbody tr").each(function() {
            var originCityTB = NWF$(this).find('.OriginCityTB').text();
            var destnTB = NWF$(this).find('.DestTB').text();
            if(destnTB==element['Destination'] && originCityTB==element['Origin City']) {
                NWF$(this).find('.'+element['Service Level']).html(element['ATARate']);
                ataRateFreq+=1;
                ataRateLowestBidder = element['Carrier'];
                checkDestAndOriginCity = true;
            }
        });

        if(!checkDestAndOriginCity) {
            tBody = '';
            tBody+='<tr>';
            tBody+='<td>'+element['Carrier']+'</td>';
            tBody+='<td class = "OriginCountryTB">'+element['Origin Country']+'</td>';
            tBody+='<td class = "OriginCityTB">'+element['Origin City']+'</td>';
            tBody+='<td class = "DestTB">'+element['Destination']+'</td>';
            NWF$("#ataRateComparison .serviceLevel").each(function() {
                if(NWF$(this).text() ==element['Service Level']) {
                    tBody+='<td class = '+NWF$(this).text()+'>'+element['ATARate']+'</td>';
                    ataRateFreq+=1;
                    atdRateLowestBidder = element['Carrier'];
                } else {
                    tBody+='<td class = '+NWF$(this).text()+'></td>';
                }
            })
            tBody+='</tr>';
            NWF$("#ataRateComparison tbody").append(tBody);  
        }
       
    })

   

   

    //ataRateComparisonLowestBidder

    var thOriginCity = '';
    holdOriginCity.forEach(element => {
        var removedSPaceCity = element.replace(/ /g,"_");
        removedSPaceCity = removedSPaceCity.replace(/,/g, '');
        thOriginCity+= '<th class = "originCity '+removedSPaceCity+'">';
        thOriginCity+= element;
        thOriginCity+= '</th>';
    });

    var header = '<tr><th>Origin/Destination</th><th>Service Level</th><th></th>'+thOriginCity+'</tr>';
    
    NWF$("#ataRateComparisonLowestBidder thead").append(header);

    var tBody = '';
    ataJSON.forEach(element=>{

        var checkDestAndServiceLevel = false;
        NWF$("#ataRateComparisonLowestBidder tbody tr").each(function() {
            var serviceLevelTB = NWF$(this).find('.ServiceLevelTB').text();
            var destnTB = NWF$(this).find('.DestTB').text();
            if(destnTB==element['Destination'] && serviceLevelTB==element['Service Level']) {
                var removedSPaceCity = element['Origin City'].replace(/ /g,"_");
                removedSPaceCity = removedSPaceCity.replace(/,/g, '');
                if(NWF$(this).find('.SummaryDataTB').text() =='Lowest Rate') {
                    NWF$(this).find('.'+removedSPaceCity).html(element['ATARate']);
                } else if(NWF$(this).find('.SummaryDataTB').text() =='Lowest Bidder') {
                    NWF$(this).find('.'+removedSPaceCity).html(element['Carrier']);
                }
                checkDestAndServiceLevel = true;
            }
        });

        if(!checkDestAndServiceLevel) {

            summaryData.forEach(elementData => {
                tBody = '';
                tBody+='<tr>';
                tBody+='<td class="DestTB firstColF">'+element['Destination']+'</td>';
                tBody+='<td class="ServiceLevelTB">'+element['Service Level']+'</td>';
                //tBody+='<td>'+element['Carrier']+'</td>';
                tBody+='<td class="SummaryDataTB">'+elementData+'</td>';
                // tBody+='<td>'+element['Origin City']+'</td>';
            
                NWF$("#ataRateComparisonLowestBidder .originCity").each(function() {

                    var removedSpaceOrgnCity =  NWF$(this).attr('class').split(' ')[1]; 
                    removedSpaceOrgnCity = removedSpaceOrgnCity.replace(/,/g, '');
                    if(NWF$(this).text() == element['Origin City'] && elementData =='Lowest Rate') {
                        tBody+='<td class = '+removedSpaceOrgnCity+'>'+element['ATARate']+'</td>';
                    } else if(elementData == 'Lowest Bidder' && NWF$(this).text() ==element['Origin City']) {
                        tBody+='<td class = '+removedSpaceOrgnCity+'>'+element['Carrier']+'</td>';
                    } else {
                        tBody+='<td class = '+removedSpaceOrgnCity+'></td>';
                    }
                })
                tBody+='</tr>';
                NWF$("#ataRateComparisonLowestBidder tbody").append(tBody);
            });

       }
        
    })
    

}

function retrieveATDRate() {

    atdJSON = [];
    for (let ia = 0; ia < AirRFQJSON.length; ia++) {
        const elementAirRFQ = AirRFQJSON[ia];
       var AtdzoneFlag = elementAirRFQ['ATDRate_Zone1'] > 0 || elementAirRFQ['ATDRate_Zone2'] > 0 || elementAirRFQ['ATDRate_Zone3'] > 0 || elementAirRFQ['ATDRate_Zone4'] > 0 || elementAirRFQ['ATDRate_Zone5'] > 0;

        if (AtdzoneFlag) {

            for (let index = 0; index < supportListOriginDest.length; index++) {
                const element = supportListOriginDest[index];
                if (elementAirRFQ['OriginCity'] == element['Origin'] && elementAirRFQ['Dstn'] == element['Dstn']) {
                    atdJSON.push({
                        'Carrier': elementAirRFQ['AFF'],
                        'Origin Country': elementAirRFQ['OriginCountry'],
                        'Origin City': elementAirRFQ['OriginCity'],
                        'Destination': elementAirRFQ['Dstn'],
                        'Service Level': elementAirRFQ['ServiceLevel'],
                        'ATDRate_Zone1': elementAirRFQ['ATDRate_Zone1'],
                        'ATDRate_Zone2': elementAirRFQ['ATDRate_Zone2'],
                        'ATDRate_Zone3': elementAirRFQ['ATDRate_Zone3'],
                        'ATDRate_Zone4': elementAirRFQ['ATDRate_Zone4'],
                        'ATDRate_Zone5': elementAirRFQ['ATDRate_Zone5'],

                    })
                    break;
                }

            }

        }

    }

    var holdServiceLevel = [];
    var holdOriginCity = [];

    atdJSON.forEach(element => {
        
        if(holdServiceLevel.indexOf(element['Service Level']) == -1) {
            holdServiceLevel.push(element['Service Level']);
        }

        if(holdOriginCity.indexOf(element['Origin City']) == -1) {
            holdOriginCity.push(element['Origin City']);
        }
        
    });

    var thservice = '';
    holdServiceLevel.forEach(element => {
        thservice+= '<th colspan = "5" class = "serviceLevelATD '+element+'">';
        thservice+= element;
        thservice+= '</th>';
    });

    var header = '<tr><th colspan="4"></th>'+thservice+'</tr>';
    //var header = '<tr><th>Carrier</th><th>Origin Country</th><th>Origin City</th><th>Destination</th>'+thservice+'</tr>';
    var thservice = '';
    holdServiceLevel.forEach(element => {
        thservice+= '<th class = "serviceLevelATDZone '+element+'">Zone 1</th>';
        thservice+= '<th class = "serviceLevelATDZone '+element+'">Zone 2</th>';
        thservice+= '<th class = "serviceLevelATDZone '+element+'">Zone 3</th>';
        thservice+= '<th class = "serviceLevelATDZone '+element+'">Zone 4</th>';
        thservice+= '<th class = "serviceLevelATDZone '+element+'">Zone 5</th>';
    });

    header += '<tr><th>Carrier</th><th>Origin Country</th><th>Origin City</th><th>Destination</th>'+thservice+'</tr>';

    NWF$("#atdRateComparison thead").append(header);

    var tBody = '';
    atdJSON.forEach(element=>{

        var checkDestAndOriginCity = false;
        NWF$("#atdRateComparison tbody tr").each(function() {
            var originCityTB = NWF$(this).find('.OriginCityTB').text();
            var destnTB = NWF$(this).find('.DestTB').text();
            if(destnTB==element['Destination'] && originCityTB==element['Origin City']) {
               // NWF$(this).find('.'+element['Service Level']).html(element['ATARate']);
               for (let ite = 0; ite <=5; ite++) {
                var servClass = element['Service Level']+'_'+ite;
                NWF$(this).find('.'+servClass).html(element['ATDRate_Zone'+ite]);
                atdRateFreq+=1;
               }
                checkDestAndOriginCity = true;
            }
        });

        if(!checkDestAndOriginCity) {

            tBody = '';
            tBody+='<tr>';
            tBody+='<td>'+element['Carrier']+'</td>';
            tBody+='<td>'+element['Origin Country']+'</td>';
            tBody+='<td class="OriginCityTB">'+element['Origin City']+'</td>';
            tBody+='<td class="DestTB">'+element['Destination']+'</td>';
            NWF$("#atdRateComparison .serviceLevelATDZone").each(function() {
                var getZoneIndex = NWF$(this).text().split(' ')[1];
                var addClass = NWF$(this).attr('class').split(' ')[1]+'_'+getZoneIndex;
                if(NWF$(this).attr('class').split(' ')[1] == element['Service Level']) {
                    var atdZoneProp = 'ATDRate_Zone'+getZoneIndex;
                    tBody+='<td class='+addClass+'>'+element[atdZoneProp]+'</td>';
                    atdRateFreq+=1;
                    atdRateLowestBidder = element['Carrier'];
                } else {
                    tBody+='<td class='+addClass+'></td>';
                }
            })
            tBody+='</tr>';
            NWF$("#atdRateComparison tbody").append(tBody);

        }
      
    })
   

   /**ATD Lowest Bidder Starts here */
    var thOriginCity = '';
    holdOriginCity.forEach(element => {
        var removedSPaceCity = element.replace(/ /g,"_");
        removedSPaceCity = removedSPaceCity.replace(/,/g, '');
        thOriginCity+= '<th class = "originCity '+removedSPaceCity+'">';
        thOriginCity+= element;
        thOriginCity+= '</th>';
    });

    var header = '<tr><th>Origin/Destination</th><th>Service Level</th><th>Zone</th><th></th>'+thOriginCity+'</tr>';
    
    NWF$("#atdRateComparisonLowestBidder thead").append(header);

    var tBody = '';
    atdJSON.forEach(element=>{

        var checkDestAndServiceLevel = false;
        NWF$("#atdRateComparisonLowestBidder tbody tr").each(function() {
            var serviceLevelTB = NWF$(this).find('.ServiceLevelTB').text();
            var destnTB = NWF$(this).find('.DestTB').text();
            var tdZoneVal = NWF$(this).find('.zoneTB').text();
            var getZoneIndex = tdZoneVal.split(' ')[1];
           // zones.forEach(elementZone =>{
               
                if(destnTB==element['Destination'] && serviceLevelTB==element['Service Level']) {
                    var removedSPaceCity = element['Origin City'].replace(/ /g,"_");
                    removedSPaceCity = removedSPaceCity.replace(/,/g, '');
                    var atdZoneProp = 'ATDRate_Zone'+getZoneIndex;
                    if(NWF$(this).find('.SummaryDataTB').text() =='Lowest Rate') {
                        NWF$(this).find('.'+removedSPaceCity).html(element[atdZoneProp]);
                    } else if(NWF$(this).find('.SummaryDataTB').text() =='Lowest Bidder') {
                        NWF$(this).find('.'+removedSPaceCity).html(element['Carrier']);
                    }
                    checkDestAndServiceLevel = true;
                }
           // });    
           
        });

       if(!checkDestAndServiceLevel) {

            zones.forEach(elementZone =>{
                var getZoneIndex = elementZone.split(' ')[1];
                summaryData.forEach(elementData => {
                    tBody ='';
                    tBody+='<tr>';
                    tBody+='<td class="DestTB firstColF">'+element['Destination']+'</td>';
                    tBody+='<td class="ServiceLevelTB">'+element['Service Level']+'</td>';
                    tBody+='<td class="zoneTB">'+elementZone+'</td>';
                    tBody+='<td class="SummaryDataTB">'+elementData+'</td>';

                    NWF$("#atdRateComparisonLowestBidder .originCity").each(function() {
                        var removedSpaceOrgnCity =  NWF$(this).attr('class').split(' ')[1]; 
                        removedSpaceOrgnCity = removedSpaceOrgnCity.replace(/,/g, '');
                        if(NWF$(this).text() ==element['Origin City'] && elementData =='Lowest Rate') {
                            var atdZoneProp = 'ATDRate_Zone'+getZoneIndex;
                            tBody+='<td class = '+removedSpaceOrgnCity+'>'+element[atdZoneProp]+'</td>';
                        } else if(elementData == 'Lowest Bidder' && NWF$(this).text() ==element['Origin City']) {
                            tBody+='<td class = '+removedSpaceOrgnCity+'>'+element['Carrier']+'</td>';
                        } else {
                            tBody+='<td class = '+removedSpaceOrgnCity+'></td>';
                        }
                    })
                    tBody+='</tr>';
                    NWF$("#atdRateComparisonLowestBidder tbody").append(tBody);
                });
            })

       }
        
    })

}

function retrieveLastMile() {

    lastMileRateJSON = [];
    for (let ia = 0; ia < AirRFQJSON.length; ia++) {
        const elementAirRFQ = AirRFQJSON[ia];
       var lastMileFlag = elementAirRFQ['LastMileRate_Zone1'] >= 0 || elementAirRFQ['LastMileRate_Zone2'] >= 0 || elementAirRFQ['LastMileRate_Zone3'] >= 0 || elementAirRFQ['LastMileRate_Zone4'] >= 0 || elementAirRFQ['LastMileRate_Zone5'] >= 0;

        if (lastMileFlag) {

            for (let index = 0; index < supportListOriginDest.length; index++) {
                const element = supportListOriginDest[index];
                if (elementAirRFQ['OriginCity'] == element['Origin'] && elementAirRFQ['Dstn'] == element['Dstn']) {
                    lastMileRateJSON.push({
                        'Carrier': elementAirRFQ['AFF'],
                        'Origin Country': elementAirRFQ['OriginCountry'],
                        'Origin City': elementAirRFQ['OriginCity'],
                        'Destination': elementAirRFQ['Dstn'],
                        'Service Level': elementAirRFQ['ServiceLevel'],
                        'LastMileRate_Zone1': elementAirRFQ['LastMileRate_Zone1'],
                        'LastMileRate_Zone2': elementAirRFQ['LastMileRate_Zone2'],
                        'LastMileRate_Zone3': elementAirRFQ['LastMileRate_Zone3'],
                        'LastMileRate_Zone4': elementAirRFQ['LastMileRate_Zone4'],
                        'LastMileRate_Zone5': elementAirRFQ['LastMileRate_Zone5'],

                    })
                    break;
                }

            }

        }

    }

    var holdServiceLevel = [];
    var holdOriginCity = [];

    lastMileRateJSON.forEach(element => {
        
        if(holdServiceLevel.indexOf(element['Service Level']) == -1) {
            holdServiceLevel.push(element['Service Level']);
        }

        if(holdOriginCity.indexOf(element['Origin City']) == -1) {
            holdOriginCity.push(element['Origin City']);
        }
        
    });

    var thservice = '';
    holdServiceLevel.forEach(element => {
        thservice+= '<th colspan = "5" class = "serviceLevelLastMile '+element+'">';
        thservice+= element;
        thservice+= '</th>';
    });

    var header = '<tr><th colspan="4"></th>'+thservice+'</tr>';
    //var header = '<tr><th>Carrier</th><th>Origin Country</th><th>Origin City</th><th>Destination</th>'+thservice+'</tr>';
    var thservice = '';
    holdServiceLevel.forEach(element => {
        thservice+= '<th class = "serviceLevelLastMileZone '+element+'">Zone 1</th>';
        thservice+= '<th class = "serviceLevelLastMileZone '+element+'">Zone 2</th>';
        thservice+= '<th class = "serviceLevelLastMileZone '+element+'">Zone 3</th>';
        thservice+= '<th class = "serviceLevelLastMileZone '+element+'">Zone 4</th>';
        thservice+= '<th class = "serviceLevelLastMileZone '+element+'">Zone 5</th>';
    });

    header += '<tr><th>Carrier</th><th>Origin Country</th><th>Origin City</th><th>Destination</th>'+thservice+'</tr>';

    NWF$("#lastMileComparison thead").append(header);

    var tBody = '';
    lastMileRateJSON.forEach(element=>{

        var checkDestAndOriginCity = false;
        NWF$("#lastMileComparison tbody tr").each(function() {
            var originCityTB = NWF$(this).find('.OriginCityTB').text();
            var destnTB = NWF$(this).find('.DestTB').text();
            if(destnTB==element['Destination'] && originCityTB==element['Origin City']) {
               // NWF$(this).find('.'+element['Service Level']).html(element['ATARate']);
               for (let ite = 0; ite <=5; ite++) {
                var servClass = element['Service Level']+'_'+ite;
                NWF$(this).find('.'+servClass).html(element['LastMileRate_Zone'+ite]);
                lastMileFreq+=1;
               }
                checkDestAndOriginCity = true;
            }
        });

        if(!checkDestAndOriginCity) {
            tBody='';
            tBody+='<tr>';
            tBody+='<td>'+element['Carrier']+'</td>';
            tBody+='<td>'+element['Origin Country']+'</td>';
            tBody+='<td class="OriginCityTB">'+element['Origin City']+'</td>';
            tBody+='<td class="DestTB">'+element['Destination']+'</td>';
            NWF$("#lastMileComparison .serviceLevelLastMileZone").each(function() {
    
                var getZoneIndex = NWF$(this).text().split(' ')[1];
                var addClass = NWF$(this).attr('class').split(' ')[1]+'_'+getZoneIndex;
                if(NWF$(this).attr('class').split(' ')[1] == element['Service Level']) {
                    var lastMileZoneProp = 'LastMileRate_Zone'+getZoneIndex;
                    tBody+='<td class='+addClass+'>'+element[lastMileZoneProp]+'</td>';
                    lastMileFreq+=1;
                    lastMileLowestBidder = element['Carrier'];
                } else {
                    tBody+='<td class='+addClass+'></td>';
                }
            })
            tBody+='</tr>';
            NWF$("#lastMileComparison tbody").append(tBody);

        }
       
    })
    

   /**lastmile Lowest Bidder Starts here */
    var thOriginCity = '';
    holdOriginCity.forEach(element => {
        var removedSPaceCity = element.replace(/ /g,"_");
        removedSPaceCity = removedSPaceCity.replace(/,/g, '');
        thOriginCity+= '<th class = "originCity '+removedSPaceCity+'">';
        thOriginCity+= element;
        thOriginCity+= '</th>';
    });

    var header = '<tr><th>Origin/Destination</th><th>Service Level</th><th>Zone</th><th></th>'+thOriginCity+'</tr>';
    
    NWF$("#lastMileComparisonLowestBidder thead").append(header);

    var tBody = '';
    lastMileRateJSON.forEach(element=>{

        var checkDestAndServiceLevel = false;
        NWF$("#lastMileComparisonLowestBidder tbody tr").each(function() {
            var serviceLevelTB = NWF$(this).find('.ServiceLevelTB').text();
            var destnTB = NWF$(this).find('.DestTB').text();
            var tdZoneVal = NWF$(this).find('.zoneTB').text();
            var getZoneIndex = tdZoneVal.split(' ')[1];
           // zones.forEach(elementZone =>{
               
                if(destnTB==element['Destination'] && serviceLevelTB==element['Service Level']) {
                    var removedSPaceCity = element['Origin City'].replace(/ /g,"_");
                    removedSPaceCity = removedSPaceCity.replace(/,/g, '');
                    var lastMileZoneProp = 'LastMileRate_Zone'+getZoneIndex;
                    if(NWF$(this).find('.SummaryDataTB').text() =='Lowest Rate') {
                        NWF$(this).find('.'+removedSPaceCity).html(element[lastMileZoneProp]);
                    } else if(NWF$(this).find('.SummaryDataTB').text() =='Lowest Bidder') {
                        NWF$(this).find('.'+removedSPaceCity).html(element['Carrier']);
                    }
                    checkDestAndServiceLevel = true;
                }
           // });    
           
        });

        if(!checkDestAndServiceLevel) {

            zones.forEach(elementZone =>{
                var getZoneIndex = elementZone.split(' ')[1];
                summaryData.forEach(elementData => {
                    tBody = '';
                    tBody+='<tr>';
                    tBody+='<td class="DestTB firstColF">'+element['Destination']+'</td>';
                    tBody+='<td class="ServiceLevelTB">'+element['Service Level']+'</td>';
                    tBody+='<td class="zoneTB">'+elementZone+'</td>';
                    tBody+='<td class="SummaryDataTB">'+elementData+'</td>';
    
                    NWF$("#lastMileComparisonLowestBidder .originCity").each(function() {
    
                        var removedSpaceOrgnCity =  NWF$(this).attr('class').split(' ')[1]; 
                        removedSpaceOrgnCity = removedSpaceOrgnCity.replace(/,/g, '');
                        if(NWF$(this).text() ==element['Origin City'] && elementData =='Lowest Rate') {
                            var lastMileZoneProp = 'LastMileRate_Zone'+getZoneIndex;
                            tBody+='<td class='+removedSpaceOrgnCity+'>'+element[lastMileZoneProp]+'</td>';
                        } else if(elementData == 'Lowest Bidder' && NWF$(this).text() ==element['Origin City']) {
                            tBody+='<td class='+removedSpaceOrgnCity+'>'+element['Carrier']+'</td>';
                        } else {
                            tBody+='<td class='+removedSpaceOrgnCity+'></td>';
                        }
                    })
                    tBody+='</tr>';
                    NWF$("#lastMileComparisonLowestBidder tbody").append(tBody);
                });
            })
        }

    })


}

function retrieveOriginToDestination() {

    //alert("here");

    var dstnzonemapping = {
        "LCK": "1",
        "ATL": "2",
        "ORD": "5",
        "SEA": "1",
        "YYZ": "1",
        "JFK": "4",
        "LAX": "1"
    };

    originToDestJSON = [];
    for (let ia = 0; ia < AirRFQJSON.length; ia++) {
        const elementAirRFQ = AirRFQJSON[ia];
       var AtdzoneFlag = elementAirRFQ['ATDRate_Zone1'] > 0 || elementAirRFQ['ATDRate_Zone2'] > 0 || elementAirRFQ['ATDRate_Zone3'] > 0 || elementAirRFQ['ATDRate_Zone4'] > 0 || elementAirRFQ['ATDRate_Zone5'] > 0;

        if (AtdzoneFlag && elementAirRFQ['ServiceLevel'] =='STD') {

            for (let index = 0; index < supportListOriginDest.length; index++) {
                const element = supportListOriginDest[index];
                if (elementAirRFQ['OriginCity'] == element['Origin'] && elementAirRFQ['Dstn'] == element['Dstn']) {
                    originToDestJSON.push({
                        'Carrier': elementAirRFQ['AFF'],
                        'Origin Country': elementAirRFQ['OriginCountry'],
                        'Origin City': elementAirRFQ['OriginCity'],
                        'Destination': elementAirRFQ['Dstn'],
                        'Service Level': elementAirRFQ['ServiceLevel'],
                        'ATDRate_Zone1': elementAirRFQ['ATDRate_Zone1'],
                        'ATDRate_Zone2': elementAirRFQ['ATDRate_Zone2'],
                        'ATDRate_Zone3': elementAirRFQ['ATDRate_Zone3'],
                        'ATDRate_Zone4': elementAirRFQ['ATDRate_Zone4'],
                        'ATDRate_Zone5': elementAirRFQ['ATDRate_Zone5'],

                    })
                    break;
                }

            }

        }

    }

    //console.log(originToDestJSON);

    var holdDestinationLevel = [];
    var holdOriginCity = [];

    originToDestJSON.forEach(element => {
        
        if(holdDestinationLevel.indexOf(element['Destination']) == -1) {
            holdDestinationLevel.push(element['Destination']);
        }

        if(holdOriginCity.indexOf(element['Origin City']) == -1) {
            holdOriginCity.push(element['Origin City']);
        }
        
    });


    var thDestination = '';
    holdDestinationLevel.forEach(element => {
        thDestination+= '<th class = "Destination '+element+'">';
        thDestination+= element;
        thDestination+= '</th>';
    });

    var header = '<tr><th>Carrier</th><th>Origin Country</th><th>Origin City</th>'+thDestination+'</tr>';

    NWF$("#originToDestinationCFS thead").append(header);

    var tBody = '';
    originToDestJSON.forEach(element=>{

        var checkOriginCity = false;
        NWF$("#originToDestinationCFS tbody tr").each(function() {
            var originCityTB = NWF$(this).find('.OriginCityTB').text();
            //var destnTB = NWF$(this).find('.DestTB').text();
            if(originCityTB==element['Origin City']) {
                var dstnMapzoneVal = dstnzonemapping[element['Destination']];
                var ATDZoneProp = 'ATDRate_Zone'+dstnMapzoneVal;
                NWF$(this).find('.'+element['Destination']).html(element[ATDZoneProp]);
                checkOriginCity = true;
                originToDestFreq+=1;
            }
        });

        if(!checkOriginCity) {
            tBody='';
            tBody+='<tr>';
            tBody+='<td>'+element['Carrier']+'</td>';
            tBody+='<td>'+element['Origin Country']+'</td>';
            tBody+='<td class = "OriginCityTB">'+element['Origin City']+'</td>';
            NWF$("#originToDestinationCFS .Destination").each(function() {
                if(NWF$(this).text()==element['Destination']) {
                    var dstnMapzoneVal = dstnzonemapping[element['Destination']];
                    var ATDZoneProp = 'ATDRate_Zone'+dstnMapzoneVal;
                    tBody+='<td class='+NWF$(this).text()+'>'+element[ATDZoneProp]+'</td>';
                    originToDestFreq+=1;
                    originToDestLowestBidder = element['Carrier'];
                } else {
                    tBody+='<td class='+NWF$(this).text()+'></td>';
                }
            })
            tBody+='</tr>';
            NWF$("#originToDestinationCFS tbody").append(tBody);    

        }
       

    });
   

    

    /**lowestbidder starts here */

    var thOriginCity = '';
    holdOriginCity.forEach(element => {
        var removedSPaceCity = element.replace(/ /g,"_");
        removedSPaceCity = removedSPaceCity.replace(/,/g, '');
        thOriginCity+= '<th class = "originCity '+removedSPaceCity+'">';
        thOriginCity+= element;
        thOriginCity+= '</th>';
    });

    var header = '<tr><th>Origin/Destination</th><th></th>'+thOriginCity+'</tr>';
    
    NWF$("#originToDestinationCFSLowestBidder thead").append(header);

    var tBody = '';
    originToDestJSON.forEach(element=>{

        var checkDestAndServiceLevel = false;
        NWF$("#originToDestinationCFSLowestBidder tbody tr").each(function() {

            var destnTB = NWF$(this).find('.DestTB').text().split(' ')[0];
            if(destnTB==element['Destination']) {
                var removedSPaceCity = element['Origin City'].replace(/ /g,"_");
                removedSPaceCity = removedSPaceCity.replace(/,/g, '');
                if(NWF$(this).find('.SummaryDataTB').text() =='Lowest Rate') {
                    var dstnMapzoneVal = dstnzonemapping[element['Destination']];
                    var ATDZoneProp = 'ATDRate_Zone'+dstnMapzoneVal;
                    NWF$(this).find('.'+removedSPaceCity).html(element[ATDZoneProp]);
                } else if(NWF$(this).find('.SummaryDataTB').text() =='Lowest Bidder') {
                    NWF$(this).find('.'+removedSPaceCity).html(element['Carrier']);
                }
                checkDestAndServiceLevel = true;
            }
        });


        if(!checkDestAndServiceLevel) {
            summaryData.forEach(elementData => {
                tBody='';
                tBody+='<tr>';
                tBody+='<td class = "DestTB firstColF">'+element['Destination']+' (Zone'+dstnzonemapping[element['Destination']]+')</td>';
                tBody+='<td class="SummaryDataTB">'+elementData+'</td>';
            
                NWF$("#originToDestinationCFSLowestBidder .originCity").each(function() {
                    var removedSpaceOrgnCity =  NWF$(this).attr('class').split(' ')[1]; 
                    removedSpaceOrgnCity = removedSpaceOrgnCity.replace(/,/g, '');
                    if(NWF$(this).text() ==element['Origin City'] && elementData =='Lowest Rate') {
                        var dstnMapzoneVal = dstnzonemapping[element['Destination']];
                        var ATDZoneProp = 'ATDRate_Zone'+dstnMapzoneVal;
                        tBody+='<td class='+removedSpaceOrgnCity+'>'+element[ATDZoneProp]+'</td>';
                    } else if(elementData == 'Lowest Bidder' && NWF$(this).text() ==element['Origin City']) {
                        tBody+='<td class='+removedSpaceOrgnCity+'>'+element['Carrier']+'</td>';
                    } else {
                        tBody+='<td class='+removedSpaceOrgnCity+'></td>';
                    }
                })
                tBody+='</tr>';
                NWF$("#originToDestinationCFSLowestBidder tbody").append(tBody);
            });
        }
       
    })

}

function GetAFFLaneDTKEYAsync() {

    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle(prefixSuprtListName + 'MapOriginDestination');
    var camlQuery = new SP.CamlQuery();
    this.originDestinationListItem = oList.getItems(camlQuery);
    clientContext.load(originDestinationListItem);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQueryOriginDestination),
        Function.createDelegate(this, this.onQueryFailed));

}

function onQueryOriginDestination() {

    var listItemEnumerator = originDestinationListItem.getEnumerator();
    var OriginDestList = [];

    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();

        OriginDestList.push(
            {
                'ID': oListItem.get_item('ID'),
                'Header': oListItem.get_item('_x004b_ey0'),
                'Origin': oListItem.get_item('_x004b_ey2'),
                'Value0': oListItem.get_item('Value0'),
                'Value1': oListItem.get_item('Value1'),
                'Value2': oListItem.get_item('Value2'),
                'Value3': oListItem.get_item('Value3'),
                'Value4': oListItem.get_item('Value4'),
                'Value5': oListItem.get_item('Value5'),
                'Value6': oListItem.get_item('Value6'),
            }
        )
    }

    supportListOriginDest = [];

    for (let index = 1; index < OriginDestList.length; index++) {
        const element = OriginDestList[index];

        if (element["Header"] == 'KEY') {
            for (let inc = 0; inc < 7; inc++) {

                if (element["Value" + inc] == 'Yes') {
                    supportListOriginDest.push({
                        "Origin": element['Origin'],
                        "Dstn": OriginDestList[0]["Value" + inc].split(" ")[0]
                    })

                }

            }

        }

    }
    //console.log(supportListOriginDest);
}

function GetMasterRecord() {

    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName + 'MasterFormList');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View>" +
        "<Query>" +
        "<Where>" +
        "<Eq>" +
        "<FieldRef Name='RFQ_AFFID' />" +
        "<Value Type='Lookup'>" + RFQ_ID + "</Value>" +
        "</Eq>" +
        "</Where>" +
        "</Query>" +
        "</View>");

    this.masterListItem = oList.getItems(camlQuery);
    clientContext.load(masterListItem);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQueryMaster),
        Function.createDelegate(this, this.onQueryFailed));

}

function onQueryMaster() {

    var listItemEnumerator = masterListItem.getEnumerator();
    masterList = [];

    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();

        masterList.push(
            {
                'RoundNumber': oListItem.get_item('IsRound2Listed'),
                'AFF': oListItem.get_item('AFF'),
            }
        )
    }

    GetAFFMappingAsync();
}

function GetAFFMappingAsync() {

    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle('LGS_STG_MapCarrierCd');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View><Query><Where><And><Eq><FieldRef Name='Title' /><Value Type='Text'>AIR</Value></Eq><Eq><FieldRef Name='Active' /><Value Type='Boolean'>1</Value></Eq></And></Where></Query></View>");

    this.affMappingListItem = oList.getItems(camlQuery);
    clientContext.load(affMappingListItem);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQueryAFFMapping),
        Function.createDelegate(this, this.onQueryFailed));

}

function onQueryAFFMapping() {

    var listItemEnumerator = affMappingListItem.getEnumerator();
    var ListAFFMap = [];

    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();

        ListAFFMap.push(
            {
                'Name': oListItem.get_item('Carrier'),
                'Code': oListItem.get_item('CarrierCD'),
            }
        )
    }

    ListAFFMap.forEach(element => {

        if (element['Name'] == masterList[0]['AFF']) {
            supportListAFFMapCode = element['Code'];
        }

    });
    //console.log(supportListAFFMapCode);
    retrieveAirRFQ();
}

function retrieveAirRFQ() {

    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName + 'AirRFQ');
    var camlQuery = new SP.CamlQuery();
    var roundNumber = masterList[0]['RoundNumber'];
    roundNumber += 1;
    camlQuery.set_viewXml("<View>" +
        "<Query>" +
        "<Where>" +
        "<And>" +
        "<Eq>" +
        "<FieldRef Name='RFQ_AFFID' />" +
        "<Value Type='Lookup'>" + RFQ_ID + "</Value>" +
        "</Eq>" +
        "<Eq>" +
        "<FieldRef Name='RoundNum' />" +
        "<Value Type='Number'>" + roundNumber + "</Value>" +
        "</Eq>" +
        "</And>" +
        "</Where>" +
        "</Query>" +
        "</View>");

    this.airRFQListItem = oList.getItems(camlQuery);
    clientContext.load(airRFQListItem);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQuerySucceededAirRFQ),
        Function.createDelegate(this, this.onQueryFailed));

}

function onQuerySucceededAirRFQ() {

    AirRFQJSON = [];
    var listItemEnumerator = airRFQListItem.getEnumerator();
    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();
        AirRFQJSON.push({
            "AFF": supportListAFFMapCode,
            "ServiceLevel": oListItem.get_item('ServiceLevel'),
            "OriginCountry": oListItem.get_item('OriginCountry'),
            "OriginCity": oListItem.get_item('OriginCity'),
            "Dstn": oListItem.get_item('Dstn'),
            "ATARate": oListItem.get_item('ATARate'),
            "LastMileRate_Zone1": oListItem.get_item('LastMileRate_Zone1'),
            "LastMileRate_Zone2": oListItem.get_item('LastMileRate_Zone2'),
            "LastMileRate_Zone3": oListItem.get_item('LastMileRate_Zone3'),
            "LastMileRate_Zone4": oListItem.get_item('LastMileRate_Zone4'),
            "LastMileRate_Zone5": oListItem.get_item('LastMileRate_Zone5'),
            "ATDRate_Zone1": oListItem.get_item('ATDRate_Zone1'),
            "ATDRate_Zone2": oListItem.get_item('ATDRate_Zone2'),
            "ATDRate_Zone3": oListItem.get_item('ATDRate_Zone3'),
            "ATDRate_Zone4": oListItem.get_item('ATDRate_Zone4'),
            "ATDRate_Zone5": oListItem.get_item('ATDRate_Zone5'),
            "NQ": oListItem.get_item('IsNQ'),
        })
    }

    NWF$(".nd-dialog-panel").css("display","none");
    //console.log(AirRFQJSON);

}


function exportExcel(flag) {

    const fileName = flag;
    if (flag == 'All in rate comparison - ATA Rate table') {
        data = exportTabs('ataRateComparison');
    } else if (flag == 'All in rate comparison - ATA Rate table lowest bidder') {
        data = exportTabs('ataRateComparisonLowestBidder');
    } else if (flag == 'All in rate comparison - ATD Rate table') {
        data = exportTabZones('atdRateComparison',1);
    } else if (flag == 'All in rate comparison - ATD Rate table lowest bidder') {
        data = exportTabs('atdRateComparisonLowestBidder');
    } else if (flag == 'All in rate comparison - Last Mile Rate table') {
        data = exportTabZones('lastMileComparison',1);
    } else if (flag == 'All in rate comparison - Last Mile Rate table lowest bidder') {
        data = exportTabs('lastMileComparisonLowestBidder');
    } else if (flag == 'From Origin to Destination CFS') {
        data = exportTabs('originToDestinationCFS');
    } else if (flag == 'From Origin to Destination CFS lowest bidder') {
        data = exportTabs('originToDestinationCFSLowestBidder');
    } else {
        data = exportTabs('overAllT');
    }

    // data = [{
    //     "ID": '',
    //     "Country": '',
    // }];

    const exportType = 'csv';
    window.exportFromJSON({
        data,
        fileName,
        exportType
    });

}

function exportTabs(idParm) {

    var header = [];
    var body = [];
    NWF$("#"+idParm+ " thead tr th").each(function() {

        var Headerval = NWF$(this).text().replace(/,/g, '_');
        header.push(Headerval);
    });


    NWF$("#"+idParm+ " tbody tr").each(function() {
        let element = [];
        NWF$(this).find('td').each((index, td)=> {
            element.push(NWF$(td).text());
        })
        body.push(element);
    });

    var data = [];
    for (let index = 0; index < body.length; index++) {
        const element = body[index];
        let obj = {};
        header.forEach((k, i) => {obj[k] = element[i]})
        data.push(obj);
    }

    return data;
}

//:eq(" + index + ")"

function exportTabZones(idParm, index) {

    var header = [];
    var body = [];
    NWF$("#"+idParm+ " thead tr:eq("+index+") th").each(function() {

       // console.log(NWF$(this).text());
        var Headerval = NWF$(this).text().replace(/,/g, '_');
        var classVal = '';
        classVal = NWF$(this).attr('class');
         if(!!classVal) {
             classVal = classVal.split(' ')[1];
         }
        header.push(classVal+' '+Headerval);
    });

    console.log(header);

    NWF$("#"+idParm+ " tbody tr").each(function() {
        let element = [];
        NWF$(this).find('td').each((index, td)=> {
            element.push(NWF$(td).text());
        })
        body.push(element);
    });

    var data = [];
    for (let index = 0; index < body.length; index++) {
        const element = body[index];
        let obj = {};
        header.forEach((k, i) => {obj[k] = element[i]})
        data.push(obj);
    }

    return data;

}

function overAllUpdate(freq, carrier) {

      /** Add Count*/

    //ataRateFreq
    //atdRateLowestBidder

    NWF$("#overAllT tbody tr").each(function(index) {

        if(index ==0) {
            NWF$(this).find('.overAllVal').html(carrier);
        } else if(index ==1) {
            NWF$(this).find('.overAllVal').html(freq);
        }

    });

}

RegExp.escape = function (r) { return r.replace(/[-/\\^$*+?.()|[\]{}]/g, "\\$&") }, function () { var r; (r = "undefined" != typeof NWF$ && NWF$ ? NWF$ : {}).csv = { defaults: { separator: ",", delimiter: '"', headers: !0 }, hooks: { castToScalar: function (r, e) { if (isNaN(r)) return r; if (/\./.test(r)) return parseFloat(r); var a = parseInt(r); return isNaN(a) ? null : a } }, parsers: { parse: function (r, e) { var a = e.separator, t = e.delimiter; e.state.rowNum || (e.state.rowNum = 1), e.state.colNum || (e.state.colNum = 1); var o = [], s = [], n = 0, i = "", l = !1; function u() { if (n = 0, i = "", e.start && e.state.rowNum < e.start) return s = [], e.state.rowNum++, void (e.state.colNum = 1); if (void 0 === e.onParseEntry) o.push(s); else { var r = e.onParseEntry(s, e.state); !1 !== r && o.push(r) } s = [], e.end && e.state.rowNum >= e.end && (l = !0), e.state.rowNum++, e.state.colNum = 1 } function c() { if (void 0 === e.onParseValue) s.push(i); else if (e.headers && 1 === e.state.rowNum) s.push(i); else { var r = e.onParseValue(i, e.state); !1 !== r && s.push(r) } i = "", n = 0, e.state.colNum++ } var f = RegExp.escape(a), d = RegExp.escape(t), m = /(D|S|\r\n|\n|\r|[^DS\r\n]+)/, p = m.source; return p = (p = p.replace(/S/g, f)).replace(/D/g, d), m = new RegExp(p, "gm"), r.replace(m, function (r) { if (!l) switch (n) { case 0: if (r === a) { i += "", c(); break } if (r === t) { n = 1; break } if (/^(\r\n|\n|\r)$/.test(r)) { c(), u(); break } i += r, n = 3; break; case 1: if (r === t) { n = 2; break } i += r, n = 1; break; case 2: if (r === t) { i += r, n = 1; break } if (r === a) { c(); break } if (/^(\r\n|\n|\r)$/.test(r)) { c(), u(); break } throw Error("CSVDataError: Illegal State [Row:" + e.state.rowNum + "][Col:" + e.state.colNum + "]"); case 3: if (r === a) { c(); break } if (/^(\r\n|\n|\r)$/.test(r)) { c(), u(); break } if (r === t) throw Error("CSVDataError: Illegal Quote [Row:" + e.state.rowNum + "][Col:" + e.state.colNum + "]"); throw Error("CSVDataError: Illegal Data [Row:" + e.state.rowNum + "][Col:" + e.state.colNum + "]"); default: throw Error("CSVDataError: Unknown State [Row:" + e.state.rowNum + "][Col:" + e.state.colNum + "]") } }), 0 !== s.length && (c(), u()), o }, splitLines: function (e, a) { if (e) { var t = (a = a || {}).separator || r.csv.defaults.separator, o = a.delimiter || r.csv.defaults.delimiter; a.state = a.state || {}, a.state.rowNum || (a.state.rowNum = 1); var s = [], n = 0, i = "", l = !1, u = RegExp.escape(t), c = RegExp.escape(o), f = /(D|S|\n|\r|[^DS\r\n]+)/, d = f.source; return d = (d = d.replace(/S/g, u)).replace(/D/g, c), f = new RegExp(d, "gm"), e.replace(f, function (r) { if (!l) switch (n) { case 0: if (r === t) { i += r, n = 0; break } if (r === o) { i += r, n = 1; break } if ("\n" === r) { m(); break } if (/^\r$/.test(r)) break; i += r, n = 3; break; case 1: if (r === o) { i += r, n = 2; break } i += r, n = 1; break; case 2: var e = i.substr(i.length - 1); if (r === o && e === o) { i += r, n = 1; break } if (r === t) { i += r, n = 0; break } if ("\n" === r) { m(); break } if ("\r" === r) break; throw Error("CSVDataError: Illegal state [Row:" + a.state.rowNum + "]"); case 3: if (r === t) { i += r, n = 0; break } if ("\n" === r) { m(); break } if ("\r" === r) break; if (r === o) throw Error("CSVDataError: Illegal quote [Row:" + a.state.rowNum + "]"); throw Error("CSVDataError: Illegal state [Row:" + a.state.rowNum + "]"); default: throw Error("CSVDataError: Unknown state [Row:" + a.state.rowNum + "]") } }), "" !== i && m(), s } function m() { if (n = 0, a.start && a.state.rowNum < a.start) return i = "", void a.state.rowNum++; if (void 0 === a.onParseEntry) s.push(i); else { var r = a.onParseEntry(i, a.state); !1 !== r && s.push(r) } i = "", a.end && a.state.rowNum >= a.end && (l = !0), a.state.rowNum++ } }, parseEntry: function (r, e) { var a = e.separator, t = e.delimiter; e.state.rowNum || (e.state.rowNum = 1), e.state.colNum || (e.state.colNum = 1); var o = [], s = 0, n = ""; function i() { if (void 0 === e.onParseValue) o.push(n); else { var r = e.onParseValue(n, e.state); !1 !== r && o.push(r) } n = "", s = 0, e.state.colNum++ } if (!e.match) { var l = RegExp.escape(a), u = RegExp.escape(t), c = /(D|S|\n|\r|[^DS\r\n]+)/.source; c = (c = c.replace(/S/g, l)).replace(/D/g, u), e.match = new RegExp(c, "gm") } return r.replace(e.match, function (r) { switch (s) { case 0: if (r === a) { n += "", i(); break } if (r === t) { s = 1; break } if ("\n" === r || "\r" === r) break; n += r, s = 3; break; case 1: if (r === t) { s = 2; break } n += r, s = 1; break; case 2: if (r === t) { n += r, s = 1; break } if (r === a) { i(); break } if ("\n" === r || "\r" === r) break; throw Error("CSVDataError: Illegal State [Row:" + e.state.rowNum + "][Col:" + e.state.colNum + "]"); case 3: if (r === a) { i(); break } if ("\n" === r || "\r" === r) break; if (r === t) throw Error("CSVDataError: Illegal Quote [Row:" + e.state.rowNum + "][Col:" + e.state.colNum + "]"); throw Error("CSVDataError: Illegal Data [Row:" + e.state.rowNum + "][Col:" + e.state.colNum + "]"); default: throw Error("CSVDataError: Unknown State [Row:" + e.state.rowNum + "][Col:" + e.state.colNum + "]") } }), i(), o } }, helpers: { collectPropertyNames: function (r) { var e = [], a = [], t = []; for (e in r) for (a in r[e]) r[e].hasOwnProperty(a) && t.indexOf(a) < 0 && "function" != typeof r[e][a] && t.push(a); return t } }, toArray: function (e, a, t) { if (void 0 !== a && "function" == typeof a) { if (void 0 !== t) return console.error("You cannot 3 arguments with the 2nd argument being a function"); t = a, a = {} } a = void 0 !== a ? a : {}; var o = {}; o.callback = void 0 !== t && "function" == typeof t && t, o.separator = "separator" in a ? a.separator : r.csv.defaults.separator, o.delimiter = "delimiter" in a ? a.delimiter : r.csv.defaults.delimiter; var s = void 0 !== a.state ? a.state : {}; a = { delimiter: o.delimiter, separator: o.separator, onParseEntry: a.onParseEntry, onParseValue: a.onParseValue, state: s }; var n = r.csv.parsers.parseEntry(e, a); if (!o.callback) return n; o.callback("", n) }, toArrays: function (e, a, t) { if (void 0 !== a && "function" == typeof a) { if (void 0 !== t) return console.error("You cannot 3 arguments with the 2nd argument being a function"); t = a, a = {} } a = void 0 !== a ? a : {}; var o = {}; o.callback = void 0 !== t && "function" == typeof t && t, o.separator = "separator" in a ? a.separator : r.csv.defaults.separator, o.delimiter = "delimiter" in a ? a.delimiter : r.csv.defaults.delimiter; var s = []; if (void 0 !== (a = { delimiter: o.delimiter, separator: o.separator, onPreParse: a.onPreParse, onParseEntry: a.onParseEntry, onParseValue: a.onParseValue, onPostParse: a.onPostParse, start: a.start, end: a.end, state: { rowNum: 1, colNum: 1 } }).onPreParse && (e = a.onPreParse(e, a.state)), s = r.csv.parsers.parse(e, a), void 0 !== a.onPostParse && (s = a.onPostParse(s, a.state)), !o.callback) return s; o.callback("", s) }, toObjects: function (e, a, t) { if (void 0 !== a && "function" == typeof a) { if (void 0 !== t) return console.error("You cannot 3 arguments with the 2nd argument being a function"); t = a, a = {} } a = void 0 !== a ? a : {}; var o = {}; o.callback = void 0 !== t && "function" == typeof t && t, o.separator = "separator" in a ? a.separator : r.csv.defaults.separator, o.delimiter = "delimiter" in a ? a.delimiter : r.csv.defaults.delimiter, o.headers = "headers" in a ? a.headers : r.csv.defaults.headers, a.start = "start" in a ? a.start : 1, o.headers && a.start++, a.end && o.headers && a.end++; var s, n = []; a = { delimiter: o.delimiter, separator: o.separator, onPreParse: a.onPreParse, onParseEntry: a.onParseEntry, onParseValue: a.onParseValue, onPostParse: a.onPostParse, start: a.start, end: a.end, state: { rowNum: 1, colNum: 1 }, match: !1, transform: a.transform }; var i = { delimiter: o.delimiter, separator: o.separator, start: 1, end: 1, state: { rowNum: 1, colNum: 1 }, headers: !0 }; void 0 !== a.onPreParse && (e = a.onPreParse(e, a.state)); var l = r.csv.parsers.splitLines(e, i), u = r.csv.toArray(l[0], i); s = r.csv.parsers.splitLines(e, a), a.state.colNum = 1, a.state.rowNum = u ? 2 : 1; for (var c = 0, f = s.length; c < f; c++) { for (var d = r.csv.toArray(s[c], a), m = {}, p = 0; p < u.length; p++)m[u[p]] = d[p]; void 0 !== a.transform ? n.push(a.transform.call(void 0, m)) : n.push(m), a.state.rowNum++ } if (void 0 !== a.onPostParse && (n = a.onPostParse(n, a.state)), !o.callback) return n; o.callback("", n) }, fromArrays: function (e, a, t) { if (void 0 !== a && "function" == typeof a) { if (void 0 !== t) return console.error("You cannot 3 arguments with the 2nd argument being a function"); t = a, a = {} } a = void 0 !== a ? a : {}; var o = {}; o.callback = void 0 !== t && "function" == typeof t && t, o.separator = "separator" in a ? a.separator : r.csv.defaults.separator, o.delimiter = "delimiter" in a ? a.delimiter : r.csv.defaults.delimiter; var s, n, i, l, u = ""; for (i = 0; i < e.length; i++) { for (s = e[i], n = [], l = 0; l < s.length; l++) { var c = void 0 === s[l] || null === s[l] ? "" : s[l].toString(); -1 < c.indexOf(o.delimiter) && (c = c.replace(new RegExp(o.delimiter, "g"), o.delimiter + o.delimiter)); var f = "\n|\r|S|D"; f = (f = f.replace("S", o.separator)).replace("D", o.delimiter), -1 < c.search(f) && (c = o.delimiter + c + o.delimiter), n.push(c) } u += n.join(o.separator) + "\n" } if (!o.callback) return u; o.callback("", u) }, fromObjects: function (e, a, t) { if (void 0 !== a && "function" == typeof a) { if (void 0 !== t) return console.error("You cannot 3 arguments with the 2nd argument being a function"); t = a, a = {} } a = void 0 !== a ? a : {}; var o = {}; if (o.callback = void 0 !== t && "function" == typeof t && t, o.separator = "separator" in a ? a.separator : r.csv.defaults.separator, o.delimiter = "delimiter" in a ? a.delimiter : r.csv.defaults.delimiter, o.headers = "headers" in a ? a.headers : r.csv.defaults.headers, o.sortOrder = "sortOrder" in a ? a.sortOrder : "declare", o.manualOrder = "manualOrder" in a ? a.manualOrder : [], o.transform = a.transform, "string" == typeof o.manualOrder && (o.manualOrder = r.csv.toArray(o.manualOrder, o)), void 0 !== o.transform) { var s, n = e; for (e = [], s = 0; s < n.length; s++)e.push(o.transform.call(void 0, n[s])) } var i, l, u = r.csv.helpers.collectPropertyNames(e); if ("alpha" === o.sortOrder && u.sort(), 0 < o.manualOrder.length) { var c, f = [].concat(o.manualOrder); for (c = 0; c < u.length; c++)f.indexOf(u[c]) < 0 && f.push(u[c]); u = f } var d, m = []; for (o.headers && m.push(u), i = 0; i < e.length; i++) { for (l = [], c = 0; c < u.length; c++)(d = u[c]) in e[i] && "function" != typeof e[i][d] ? l.push(e[i][d]) : l.push(""); m.push(l) } return r.csv.fromArrays(m, a, o.callback) } }, r.csvEntry2Array = r.csv.toArray, r.csv2Array = r.csv.toArrays, r.csv2Dictionary = r.csv.toObjects, "undefined" != typeof module && module.exports && (module.exports = r.csv) }.call(this);
