var hideNativeRepeatingSectionControlls = 0;
var serviceFilter;
var destinationFilter;
var AirRFQJSON = [];
var AFFOriginProfileJOSN = [];
var OriginLocalChargeJSON = [];
var AccessorialChargeJSON = [];
var AirWhaseTerminalJSON = [];
var FirmCodeJSON = [];
// var AirWhaseTerminalJSON = [];
var communicationMatrixJSON = [];
var QnAJSON = [];
var RFQ_ID = 100;
var id = 32;
var applicationStatus = 'PricingRound1';
var masterStatus = 'Completed';
var roundNumber = 0;
var LogisticUser = '';
var dateNtime = '';
var site_URL = '';
var prefixTabsListName = 'MGF_RFQ_AFF_';
var currencyJSON = [];
var feedbackJSON = [];
var roundNumber2 = 0;

var formStatusLists = ['Submitted','QandAReview','PricingRound1','PricingRound1Review','PricingRound2','PricingRound2Review','Completed'];


var locationFilter;
NWF$(document).ready(function () {
    //trigger if location, CapitalExp or Volume is changed - recalculate list of Approvers
    //this.AirWhaseTerminalJSON = [];

    //alert(NWF$("#" + RFQID).val());

    var firstHeader = '';
    var secondHeader = '';

    if(roundNumber2==1) {
         firstHeader = '<th></th><th colspan="5">MGF Target</th>';
         secondHeader = '<th>ATA STD</th><th>ATD Z1 STD</th><th>ATD Z2 STD</th><th>ATD Z3 STD</th><th>ATD Z4 STD</th><th>ATD Z5 STD</th>';
    }

    var header = '<tr><th colspan="4"></th><th colspan="3">Transit Solution</th><th colspan="3">Surcharge</th><th></th><th colspan="5">Last Mile Rate/kg</th><th colspan="5">ATD Rate/kg</th>'+firstHeader+'<th></th></tr>';
    header+='<tr>'+
    '<th style='+'display:none'+'>ID</th>'+
    '<th>Region</th>'+
    '<th>Origin Country</th>'+
    '<th>Origin City</th>'+
    '<th>Volume</th>'+
    '<th>ATA TT(GAC = Day 1)</th>'+
    '<th>ATD TT(GAC = Day 1)</th>'+
    '<th>Airline</th>'+
    '<th>SSC (fixed)</th>'+
    '<th>FSC (vatos)</th>'+
    '<th>Sea-Air SSC/FSC</th>'+
    '<th>ATA Rate/kg</th>'+
    '<th>Zone 1</th>'+
    '<th>Zone 2</th>'+
    '<th>Zone 3</th>'+
    '<th>Zone 4</th>'+
    '<th>Zone 5</th>'+
    '<th>Zone 1</th>'+
    '<th>Zone 2</th>'+
    '<th>Zone 3</th>'+
    '<th>Zone 4</th>'+
    '<th>Zone 5</th>'+
    secondHeader+
    '<th>NQ</th>'+
    '</tr>'

    NWF$("#AirRFQMTable thead").append(header);



    if(masterStatus == 'Completed') {
        NWF$('.closeWithoutSaveAFF input, .closeWithoutSaveLogistic input').val('Close');
    }


    NWF$.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
          if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
          } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          } else {
            this.value = "";
          }
        });
      };

      NWF$("#AirRFQMTable").on("input keydown keyup",".airline input", function(value) {

       if (value.keyCode != 8) {
            return /^[A-Z]{0,3}$/.test(value.key);
       }
    });

    NWF$(".firmCodeSecond input").inputFilter(function(value) {

        //console.log(value);
        return /^[A-Z]{0,255}$/.test(value); 
    });

    var listAllRadio = NWF$(".controlTabsList input[type='radio']");

    listAllRadio.each(function() {
           if(NWF$(this).val() !='Q&A' && (applicationStatus =='Submitted' || applicationStatus =='QandAReview')) {
                NWF$(this).prop('disabled', true);
                NWF$(this).addClass('disableRadioBTN');
            }
    });

    NWF$("#"+dstnGatewayFirmCode).on('mousedown',function() {
        var usednames = {};
        NWF$("select[id="+dstnGatewayFirmCode+"]>option").each(function(){
            if(usednames[this.text]){
                NWF$(this).remove();
            }
            else{
                usednames[this.text]=this.value;
            }
        });
    });

    NWF$("#" + dstnGatewayFirmCode).change(function () {
        var gatewayVal = NWF$("#" + dstnGatewayFirmCode).find("option:selected").text();
        if (gatewayVal != 'Please select a value...') {
            retrieveFirmCode();
        }
    });

    NWF$("#" + var_service).change(function () {
        var serVal = NWF$("#" + var_service).find("option:selected").text();
        var desVal = NWF$("#" + var_destination).find("option:selected").text();
        if (serVal != 'Please select a value...' && desVal != 'Please select a value...') {
            retrieveApprovers();
        }

    });
    NWF$("#" + var_destination).change(function () {
        var serVal = NWF$("#" + var_service).find("option:selected").text();
        var desVal = NWF$("#" + var_destination).find("option:selected").text();
        if (serVal != 'Please select a value...' && desVal != 'Please select a value...') {
            retrieveApprovers();
        }
    });

    //QnACategoryList

    NWF$(".AffQuestionTb").change(function() {
        var value = NWF$(this).val();
        var serCatVal = NWF$("#" + QnACategoryList).find("option:selected").text();
        if (serCatVal != 'Please select a value...') {   
            //console.log(NWF$(".AffQuestionTb").val());
            QnAJSON.forEach(element => {
                if(element.Category ==serCatVal) {
                    element.Q1 = value;
                }
            });
        
        }    
    })

    NWF$(".AffAnsTb").change(function() {
        var value = NWF$(this).val();
        var serCatVal = NWF$("#" + QnACategoryList).find("option:selected").text();
        if (serCatVal != 'Please select a value...') {   
            //console.log(NWF$(".AffQuestionTb").val());
            QnAJSON.forEach(element => {
                if(element.Category ==serCatVal) {
                    element.ANS1 = value;
                }
            });
        
        }    
    })


    NWF$("#" + QnACategoryList).change(function () {
        var serCatVal = NWF$("#" + QnACategoryList).find("option:selected").text();
        if (serCatVal != 'Please select a value...') {

            QnAJSON.forEach(element => {
                if(element.Category ==serCatVal) {
                    NWF$(".AffQuestionTb").val(element.Q1);
                    NWF$(".AffAnsTb").val(element.ANS1);
                }
            });
        }    
    
    });



    NWF$(".airRFQImportExcel .nf-attachmentFileInput").change(function () {
        importexcel(NWF$(this)[0].files[0], 'airRFQ');
    });

    NWF$(".affOriginProfileImport .nf-attachmentFileInput").change(function () {
        importexcel(NWF$(this)[0].files[0], 'affOriginProfile');
    });

    NWF$(".affOriginLocalChargeImport .nf-attachmentFileInput").change(function () {
        importexcel(NWF$(this)[0].files[0], 'originLocalCharge');
    });

    NWF$(".airWhaseNTerminalImport .nf-attachmentFileInput").change(function () {
        importexcel(NWF$(this)[0].files[0], 'airWhaseNTerminal');
    });

    NWF$(".firmCodeImport .nf-attachmentFileInput").change(function () {
        importexcel(NWF$(this)[0].files[0], 'firmCode');
    });

    NWF$(".communicationMatrixImport .nf-attachmentFileInput").change(function () {
        importexcel(NWF$(this)[0].files[0], 'communicationMatrix');
    });

    NWF$(".accessorialChargeImport .nf-attachmentFileInput").change(function () {
        importexcel(NWF$(this)[0].files[0], 'accessorialCharge');
    });



    NWF$(".customtab").change(function (e) {

        if (e.target.defaultValue == 'AFF Origin Profile' && AFFOriginProfileJOSN.length == 0) {
            retrieveAffOriginProfile();
        } else if (e.target.defaultValue == 'Firm Code' && FirmCodeJSON.length == 0) {
           //retrieveFirmCode();
        } else if (e.target.defaultValue == 'Accessorial Charge' && AccessorialChargeJSON.length == 0) {
            retrieveAccessorialCharge();
        } else if (e.target.defaultValue == 'Air - Whse & Terminal' && AirWhaseTerminalJSON.length == 0) {
            retrieveAirWhaseTerminal();
        } else if (e.target.defaultValue == 'Communication Matrix' && communicationMatrixJSON.length == 0) {
            retrieveCommunicationMatrix();
        } else if(e.target.defaultValue == 'Q&A' && QnAJSON.length == 0) {
            retrieveQnA();
            //retrieveFeedbackHist();
        } else if(e.target.defaultValue == 'Origin Local Charge' && currencyJSON.length == 0) {
            retrieveCurrency();
        }

        if(feedbackJSON.length ==0) {
            retrieveFeedBack();
        }
        
    });

    NWF$("#" + var_localChargeLocation).change(function () {
        var var_location = NWF$("#" + var_localChargeLocation).find("option:selected").text();
        if (var_location != 'Please select a value...') {
            retrieveLocalCharge();
        }
    });


    NWF$("#AirRFQMTable").on("change",".nqClass input", function () {


        var index = parseInt(NWF$(this).prop('className').split("_")[1]);
        var isChecked = NWF$(this).is(":checked");
        var service = NWF$("#" + var_service).find("option:selected").text();

        for (var i = 1; i <= 5; i++) {
            NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone' + i + ' input').val(0);
            NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.aTDRate_Zone' + i + ' input').val(0);
        }

        NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.ataRate input').val(0);
        NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.airline input').val('');

        
        if(service == 'DEF') {
            NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.attGac1 input').val(0);
            NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.attGac2 input').val(0);

        }

        if (isChecked) {

            for (var i = 1; i <= 5; i++) {
                NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone' + i + ' input').prop('disabled', true);
            }

            NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.ataRate input').prop('disabled', true);
            NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.airline input').prop('disabled', true);
            if(service == 'DEF') {
                NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.attGac1 input').prop('disabled', true);
                NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.attGac2 input').prop('disabled', true);
    
            }


        } else {

            for (var i = 1; i <= 5; i++) {
                NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone' + i + ' input').prop('disabled', false);
            }

            NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.ataRate input').prop('disabled', false);
            NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.airline input').prop('disabled', false);
            if(service == 'DEF') {
                NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.attGac1 input').prop('disabled', false);
                NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.attGac2 input').prop('disabled', false);
    
            }

        }

        updateRFQJsonNQ(isChecked, index);


    });


    

    NWF$(".firmCodeSecond input").change(function () {

        var value = parseInt(NWF$(this).val());
        var index = parseInt(NWF$(this).prop('className').split("-_")[1]) + 1;

        updateFirmcodeJSON(value, index);

    });    

    NWF$(".localChargeAmountMin input").change(function () {


        var currency = NWF$(".localChargeDisCurrency input").val().trim();
        // var tile = ['CNY', 'HKD', 'INR', 'IDR', 'THB', 'VND', 'PHP', 'LKR', 'BDT', 'MYR', 'KHR', 'CAD'];
        // var usd = [0.147, 0.127, 0.014, 7.122E-05, 0.032, 4.31034E-05, 0.019, 0.006, 0.012, 0.245, 0.00025, 0.753];
        var value = parseInt(NWF$(this).val());
        var index = parseInt(NWF$(this).prop('className').split("-_")[1]) + 1;

        var currencyValue = 0;
        if (currency == 'USD') {
            currencyValue = formatfloat(value, 2);
        } else {
            
            var getNumber = 0;
            currencyJSON.forEach(element => {
                if(element.Title ==currency) {
                    getNumber = element.USD;
                }
            });
            ///var getNumber = usd[tile.indexOf(currency)];
            currencyValue = formatfloat(getNumber * value, 2);

        }
        NWF$(".originLocalChargeRepeater .nf-repeater-row:eq(" + index + ")").find('.localChargeUsMin input').val(currencyValue);

        updateLocalChargeAmountMinJSON(index);
    });



    NWF$(".localChargeAmountUOM input").change(function () {

        var currency = NWF$(".localChargeDisCurrency input").val().trim();
        // var tile = ['CNY', 'HKD', 'INR', 'IDR', 'THB', 'VND', 'PHP', 'LKR', 'BDT', 'MYR', 'KHR', 'CAD'];
        // var usd = [0.147, 0.127, 0.014, 7.122E-05, 0.032, 4.31034E-05, 0.019, 0.006, 0.012, 0.245, 0.00025, 0.753];
        var value = parseInt(NWF$(this).val());
        var index = parseInt(NWF$(this).prop('className').split("-_")[1]) + 1;

        var currencyValue = 0;
        if (currency == 'USD') {
            currencyValue = formatfloat(value, 2);
        } else {
            var getNumber = 0;
            currencyJSON.forEach(element => {
                if(element.Title ==currency) {
                    getNumber = element.USD;
                }
            });
            ///var getNumber = usd[tile.indexOf(currency)];
            currencyValue = formatfloat(getNumber * value, 2);
        }
        NWF$(".originLocalChargeRepeater .nf-repeater-row:eq(" + index + ")").find('.localChargeUsPom input').val(currencyValue);
        updateLocalChargeAmountPerUOMJSON(index)
    });

    NWF$("#AirRFQMTable").on("change",".ataRate input", function () {

        var index = parseInt(NWF$(this).prop('className').split("-_")[1]);
        var value = parseInt(NWF$(this).val());
        var lastMileZone1 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone1 input').val());
        var lastMileZone2 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone2 input').val());
        var lastMileZone3 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone3 input').val());
        var lastMileZone4 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone4 input').val());
        var lastMileZone5 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone5 input').val());


        NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.aTDRate_Zone1 input').val(value + lastMileZone1);
        NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.aTDRate_Zone2 input').val(value + lastMileZone2);
        NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.aTDRate_Zone3 input').val(value + lastMileZone3);
        NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.aTDRate_Zone4 input').val(value + lastMileZone4);
        NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.aTDRate_Zone5 input').val(value + lastMileZone5);

        updateRFQJsonAtaRate(value, index);



    });

    NWF$("#AirRFQMTable").on("change",".lastMileRate_Zone1 input, .lastMileRate_Zone2 input, .lastMileRate_Zone3 input, .lastMileRate_Zone4 input, .lastMileRate_Zone5 input", function () {

        var index = parseInt(NWF$(this).prop('className').split("-_")[2]);
        var indexIterate = parseInt(NWF$(this).prop('className').split("-_")[1]);
        var value = parseInt(NWF$(this).val());

        var ataRateValue = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.ataRate input').val());

        for (var i = indexIterate; i <= 5; i++) {

            var valueOfLastmile = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone' + i + ' input').val())
            if (valueOfLastmile <= value) {
                NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone' + i + ' input').val(value);
                NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.aTDRate_Zone' + i + ' input').val(value + ataRateValue);
            }

        }

        for (var i = indexIterate; i >= 1; i--) {
            var valueOfLastmile = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone' + i + ' input').val())
            if (valueOfLastmile >= value) {
                NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone' + i + ' input').val(value);
                NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.aTDRate_Zone' + i + ' input').val(value + ataRateValue);
            }
        }

        updateRFQJsonLastMile(index);


    })



    // NWF$(".airWhaseNTerminalRepeater .ms-addnew").click(function () {
    //     AirWhaseTerminalJSON.push({
    //         "Country": "",
    //         "Location": "",
    //         "Warehouse Address (Eng)": "",
    //         "Warehouse Address (Chi - for HK & CN only)": "",
    //         "Booking Receiving Hour// Office Mon-Fri": "",
    //         "Booking Receiving Hour// Office Sat": "",
    //         "Booking Receiving Hour// Office Sun / PH": "",
    //         "Warehouse Operating Hour Mon-Fri": "",
    //         "Warehouse Operating Hour Cargo Reception Cutoff": "",
    //         "Warehouse Operating Hour Sat": "",
    //         "Warehouse Operating Hour Cargo Reception Cutoff": "",
    //         "Warehouse Operating Hour Sun / PH": "",
    //         "Warehouse Operating Hour Cargo Reception Cutoff": "",
    //         "Bonded/Non-Bonded Warehouse": "",
    //         "Remarks / Comment": ""
    //     })

    //     var lastIndex;
    //     NWF$(".airWhaseNTerminalRepeater .nf-repeater-row").each(function (index) {
    //         if (index != 0) {
    //             lastIndex = index;
    //         }
    //     });

    //     NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.nf-repeater-deleterow-image').addClass('inc-_' + lastIndex);

    // });

    NWF$(".airWhaseNTerminalRepeater .nf-repeater-row input").change(function () {

        console.log(NWF$(this).attr('class'));

        console.log(NWF$(this).closest('.nf-repeater-row').find('.nf-repeater-deleterow-image').attr('class'));

    });

    NWF$(".Tab-_2, .Tab-_3, .Tab-_4, .Tab-_5, .Tab-_6, .Tab-_7, .Tab-_8").change(function () {

        var value = NWF$(this).val();
        var tabClassList = NWF$(this).prop('className').split(" ");
        var tabNum;

        tabClassList.forEach(element => {
            if(element.indexOf('Tab-_') !=-1) {
                tabNum = element.split('-_')[1];
                return false; 
            }

        });

        var tabClass = 'Tab'+tabNum;
        for (let index = 0; index < feedbackJSON.length; index++) {
            const element = feedbackJSON[index];

            if(element['TabName'] ==tabClass) {
                feedbackJSON[index]['FeedbackDraft'] = value;
            }
            
        }

    });

    NWF$("#AirRFQMTable").on("change",".attGac2 input, .attGac1 input", function () {

        var index = parseInt(NWF$(this).prop('className').split("-_")[1]);
        //var value = parseInt(NWF$(this).val());

        updateRFQJsonATAAndATD(index);

    });  
    
    NWF$("#AirRFQMTable").on("change",".airline input", function () {
        
        var index = parseInt(NWF$(this).prop('className').split("-_")[1]);
        updateRFQJsonAirline(index);

    });  
    

    NWF$(".linktoInstrunction").click(function() {
        NWF$(".closeWithoutSaveAFF input").click();
    })
    



});

function retrieveApprovers() {


    //var siteurl = _spPageContextInfo.webAbsoluteUrl;
    var hideNativeRepeatingSectionControlls = 0;
    var service = NWF$("#" + var_service).find("option:selected").text();
    this.serviceFilter = service;
    var destination = NWF$("#" + var_destination).find("option:selected").text();
    this.destinationFilter = destination;

    if (this.AirRFQJSON.length > 0) {
        generateAirRFQGrid();
    } else {
        var clientContext = new SP.ClientContext.get_current();
        var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'AirRFQ');
        var camlQuery = new SP.CamlQuery();
        var mode = 'A';
        var locationCaml = '<Eq><FieldRef Name="Mode"/><Value Type="Single line of text">"A"</Value></Eq>';

        console.log(roundNumber);
        roundNumber+=1;
        camlQuery.set_viewXml("<View>" +
            "<Query>" +
            "<Where>" +
            "<And>"+
            "<Eq>" +
            "<FieldRef Name='RFQ_AFFID' />" +
            "<Value Type='Lookup'>"+RFQ_ID+"</Value>" +
            "</Eq>" +
            "<Eq>" +
            "<FieldRef Name='RoundNum' />" +
            "<Value Type='Number'>"+roundNumber+"</Value>" +
            "</Eq>" +
            "</And>"+
            "</Where>" +
            "</Query>" +
            "</View>");

        this.collListItem = oList.getItems(camlQuery);
        clientContext.load(collListItem);
        clientContext.executeQueryAsync(
            Function.createDelegate(this, this.onQuerySucceeded),
            Function.createDelegate(this, this.onQueryFailed));
    }
}

function onQuerySucceeded(sender, args) {

    // Redraw the existing table, remove everything what exists, leave fresh instance
    // redrawRepeatingTable();
    this.AirRFQJSON = [];
    var listItemEnumerator = collListItem.getEnumerator();
    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();
        this.AirRFQJSON.push({
            "ID": oListItem.get_item('ID'),
            "Region": oListItem.get_item('Region'),
            "Origin Country": oListItem.get_item('OriginCountry'),
            "Origin City": oListItem.get_item('OriginCity'),
            "Destination Gateway": oListItem.get_item('Dstn'),
            "Volume": oListItem.get_item('Volume'),
            "Service level": oListItem.get_item('ServiceLevel'),
            "Transit Solution ATA TT (GAC = Day 1)": oListItem.get_item('Transit_ATATT'),
            "Transit Solution ATD TT (GAC = Day 1)": oListItem.get_item('Transit_ATDTT'),
            "Transit Solution Airline": oListItem.get_item('Transit_Airline')==null?'': oListItem.get_item('Transit_Airline'),
            "Surcharge SSC (fixed)": oListItem.get_item('Surcharge_SSC') == 'incl' ? 'incl.' : oListItem.get_item('Surcharge_SSC'),
            "Surcharge FSC (Vatos)": oListItem.get_item('Surcharge_FSC') == 'incl' ? 'incl.' : oListItem.get_item('Surcharge_FSC'),
            "Surcharge Sea-Air SSC/FSC": oListItem.get_item('Surcharge_SSC_FSC') == 'incl' ? 'incl.' : oListItem.get_item('Surcharge_SSC_FSC'),
            "ATA Rate/kg": oListItem.get_item('ATARate'),
            "Last Mile Rate/kg Zone 1": oListItem.get_item('LastMileRate_Zone1'),
            "Last Mile Rate/kg Zone 2": oListItem.get_item('LastMileRate_Zone2'),
            "Last Mile Rate/kg Zone 3": oListItem.get_item('LastMileRate_Zone3'),
            "Last Mile Rate/kg Zone 4": oListItem.get_item('LastMileRate_Zone4'),
            "Last Mile Rate/kg Zone 5": oListItem.get_item('LastMileRate_Zone5'),
            "ATD Rate/kg Zone 1": oListItem.get_item('ATDRate_Zone1'),
            "ATD Rate/kg Zone 2": oListItem.get_item('ATDRate_Zone2'),
            "ATD Rate/kg Zone 3": oListItem.get_item('ATDRate_Zone3'),
            "ATD Rate/kg Zone 4": oListItem.get_item('ATDRate_Zone4'),
            "ATD Rate/kg Zone 5": oListItem.get_item('ATDRate_Zone5'),
            "NQ": oListItem.get_item('IsNQ'),
        })


    }

    generateAirRFQGrid();

}


function generateAirRFQGrid() {

   redrawRepeatingTable();

   var mgfTargetSecondRound = '';

   if(roundNumber2==1) {
    mgfTargetSecondRound = '<td class="ataSTD"><input disabled = "true" type="text"/></td>'+
    '<td class="atdZ1STD"><input disabled = "true" type="text"/></td>'+
    '<td class="atdZ2STD"><input disabled = "true" type="text"/></td>'+
    '<td class="atdZ3STD"><input disabled = "true" type="text"/></td>'+
    '<td class="atdZ4STD"><input disabled = "true" type="text"/></td>'+
    '<td class="atdZ5STD"><input disabled = "true" type="text"/></td>';

   }

    var tBody = '';

    tBody+='<tr>'+
    '<td class="ID" style='+'display:none'+'><input disabled = "true" type="text"></input></td>'+
    '<td class="regionGrid"><input  disabled = "true" type="text"></input></td>'+
    '<td class="originCountryGrid"><input  disabled = "true" type="text"></input></td>'+
    '<td class="originCityGrid"><input  disabled = "true" type="text"></input></td>'+
    '<td class="volumeGrid"><input  disabled = "true" type="text"></input></td>'+
    '<td class="attGac1"><input class="editable_cell" type="text"></input></td>'+
    '<td class="attGac2"><input class="editable_cell" type="text"></input></td>'+
    '<td class="airline"><input class="editable_cell" type="text" maxlength="3"></input></td>'+
    '<td class="surchargeSSC"><input type="text"></input></td>'+
    '<td class="surchargeFSC"><input type="text"></input></td>'+
    '<td class="surchargeSSC_FSC"><input type="text"></input></td>'+
    '<td class="ataRate"><input class="editable_cell" type="text"></input></td>'+
    '<td class="lastMileRate_Zone1"><input class="editable_cell" type="text"></input></td>'+
    '<td class="lastMileRate_Zone2"><input class="editable_cell" type="text"></input></td>'+
    '<td class="lastMileRate_Zone3"><input class="editable_cell" type="text"></input></td>'+
    '<td class="lastMileRate_Zone4"><input class="editable_cell" type="text"></input></td>'+
    '<td class="lastMileRate_Zone5"><input class="editable_cell" type="text"></input></td>'+
    '<td class="aTDRate_Zone1"><input disabled = "true" type="text"></input></td>'+
    '<td class="aTDRate_Zone2"><input disabled = "true" type="text"></input></td>'+
    '<td class="aTDRate_Zone3"><input disabled = "true" type="text"></input></td>'+
    '<td class="aTDRate_Zone4"><input disabled = "true" type="text"></input></td>'+
    '<td class="aTDRate_Zone5"><input disabled = "true" type="text"></input></td>'+
    mgfTargetSecondRound+
    '<td class="nqClass"><input type="checkbox"></input></td>'
    '</tr>';

   NWF$("#AirRFQMTable tbody").append(tBody);

    var incVal = 0;

    for (var i = 0; i < this.AirRFQJSON.length; i++) {

        if ((this.AirRFQJSON[i]["Service level"].trim() == this.serviceFilter) && (this.AirRFQJSON[i]["Destination Gateway"].trim() == this.destinationFilter)) {

            NWF$("#AirRFQMTable tbody tr:last").find('.ID input').val(this.AirRFQJSON[i]["ID"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.regionGrid input').val(this.AirRFQJSON[i]["Region"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.originCountryGrid input').val(this.AirRFQJSON[i]["Origin Country"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.originCityGrid input').val(this.AirRFQJSON[i]["Origin City"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.volumeGrid input').val(this.AirRFQJSON[i]["Volume"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.attGac1 input').val(this.AirRFQJSON[i]["Transit Solution ATA TT (GAC = Day 1)"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.attGac2 input').val(this.AirRFQJSON[i]["Transit Solution ATD TT (GAC = Day 1)"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.airline input').val(this.AirRFQJSON[i]["Transit Solution Airline"]);

            var isDisabledAtaRate = NWF$("#AirRFQMTable tbody tr:last").find('.ataRate input').prop("disabled");
            if(this.serviceFilter == 'DEF' && isDisabledAtaRate == false) {

                NWF$("#AirRFQMTable tbody tr:last").find('.attGac1 input').prop('disabled', false);
                NWF$("#AirRFQMTable tbody tr:last").find('.attGac2 input').prop('disabled', false);
            } else {
                NWF$("#AirRFQMTable tbody tr:last").find('.attGac1 input').prop('disabled', true);
                NWF$("#AirRFQMTable tbody tr:last").find('.attGac2 input').prop('disabled', true);
            }
            
            if (this.AirRFQJSON[i]["Surcharge SSC (fixed)"] == 'incl.') {
                NWF$("#AirRFQMTable tbody tr:last").find('.surchargeSSC input').prop('disabled', true);
                NWF$("#AirRFQMTable tbody tr:last").find('.surchargeSSC input').addClass('non_editable_cell');
            } else {
                NWF$("#AirRFQMTable tbody tr:last").find('.surchargeSSC input').addClass('editable_cell');
            }

            if (this.AirRFQJSON[i]["Surcharge FSC (Vatos)"] == 'incl.') {
                NWF$("#AirRFQMTable tbody tr:last").find('.surchargeFSC input').prop('disabled', true);
                NWF$("#AirRFQMTable tbody tr:last").find('.surchargeFSC input').addClass('non_editable_cell');
            } else {
                NWF$("#AirRFQMTable tbody tr:last").find('.surchargeFSC input').addClass('editable_cell');
            }

            if (this.AirRFQJSON[i]["Surcharge Sea-Air SSC/FSC"] == 'incl.') {
                NWF$("#AirRFQMTable tbody tr:last").find('.surchargeSSC_FSC input').prop('disabled', true);
                NWF$("#AirRFQMTable tbody tr:last").find('.surchargeSSC input').addClass('non_editable_cell');
            } else {
                NWF$("#AirRFQMTable tbody tr:last").find('.surchargeSSC_FSC input').addClass('editable_cell');
            }

            NWF$("#AirRFQMTable tbody tr:last").find('.surchargeSSC input').val(this.AirRFQJSON[i]["Surcharge SSC (fixed)"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.surchargeFSC input').val(this.AirRFQJSON[i]["Surcharge FSC (Vatos)"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.surchargeSSC_FSC input').val(this.AirRFQJSON[i]["Surcharge Sea-Air SSC/FSC"]);

            NWF$("#AirRFQMTable tbody tr:last").find('.ataRate input').val(this.AirRFQJSON[i]["ATA Rate/kg"]);

            NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone1 input').val(this.AirRFQJSON[i]["Last Mile Rate/kg Zone 1"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone2 input').val(this.AirRFQJSON[i]["Last Mile Rate/kg Zone 2"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone3 input').val(this.AirRFQJSON[i]["Last Mile Rate/kg Zone 3"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone4 input').val(this.AirRFQJSON[i]["Last Mile Rate/kg Zone 4"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone5 input').val(this.AirRFQJSON[i]["Last Mile Rate/kg Zone 5"]);

            NWF$("#AirRFQMTable tbody tr:last").find('.aTDRate_Zone1 input').val(this.AirRFQJSON[i]["ATD Rate/kg Zone 1"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.aTDRate_Zone2 input').val(this.AirRFQJSON[i]["ATD Rate/kg Zone 2"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.aTDRate_Zone3 input').val(this.AirRFQJSON[i]["ATD Rate/kg Zone 3"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.aTDRate_Zone4 input').val(this.AirRFQJSON[i]["ATD Rate/kg Zone 4"]);
            NWF$("#AirRFQMTable tbody tr:last").find('.aTDRate_Zone5 input').val(this.AirRFQJSON[i]["ATD Rate/kg Zone 5"]);

            /**increment Class*/

            var alreadyAirline = NWF$("#AirRFQMTable tbody tr:last").find('.airline input').prop('className').split("-_")[1];
            if(!!alreadyAirline) {
                NWF$("#AirRFQMTable tbody tr:last").find('.airline input').removeClass('inc-_' + alreadyAirline);
            }
            NWF$("#AirRFQMTable tbody tr:last").find('.airline input').addClass('inc-_' + incVal);

            var alreadyAttGac1 = NWF$("#AirRFQMTable tbody tr:last").find('.attGac1 input').prop('className').split("-_")[1];
            if(!!alreadyAttGac1) {
                NWF$("#AirRFQMTable tbody tr:last").find('.attGac1 input').removeClass('inc-_' + alreadyAttGac1);
            }

            var alreadyAttGac2 = NWF$("#AirRFQMTable tbody tr:last").find('.attGac2 input').prop('className').split("-_")[1];
            if(!!alreadyAttGac2) {
                NWF$("#AirRFQMTable tbody tr:last").find('.attGac2 input').removeClass('inc-_' + alreadyAttGac2);
            }

            NWF$("#AirRFQMTable tbody tr:last").find('.attGac1 input').addClass('inc-_' + incVal);
            NWF$("#AirRFQMTable tbody tr:last").find('.attGac2 input').addClass('inc-_' + incVal);

            var alreadyNQ = NWF$("#AirRFQMTable tbody tr:last").find('.nqClass input').prop('className').split("_")[1];
            if(!!alreadyNQ) {
                NWF$("#AirRFQMTable tbody tr:last").find('.nqClass input').removeClass('inc_' + alreadyNQ);
            }

            NWF$("#AirRFQMTable tbody tr:last").find('.nqClass input').addClass('inc_' + incVal);

            var alreadyincValataRate = NWF$("#AirRFQMTable tbody tr:last").find('.ataRate input').prop('className').split("-_")[1];
            if(!!alreadyincValataRate) {
                NWF$("#AirRFQMTable tbody tr:last").find('.ataRate input').removeClass('inc-_' + alreadyincValataRate);
            }
            
            NWF$("#AirRFQMTable tbody tr:last").find('.ataRate input').addClass('inc-_' + incVal);

            // for(var i=1; i<6; i++) {
            //     NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone'+i+' input').addClass('inc-_'+incVal);
            // }

           var alreadyincZone =  NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone1 input').prop('className').split("-_")[2];
           if(!!alreadyincZone) {
            for (let index = 1; index <=5; index++) {
                NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone'+index+ ' input').removeClass('inc-_'+index+'-_'+alreadyincZone);
           }
           }
          

            NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone1 input').addClass('inc-_1-_' + incVal);
            NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone2 input').addClass('inc-_2-_' + incVal);
            NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone3 input').addClass('inc-_3-_' + incVal);
            NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone4 input').addClass('inc-_4-_' + incVal);
            NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone5 input').addClass('inc-_5-_' + incVal);

            if (parseInt(this.AirRFQJSON[i]["NQ"]) == 1) {
                NWF$("#AirRFQMTable tbody tr:last").find('.nqClass input').prop('checked', true);

                NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone1 input').prop('disabled', true);
                NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone2 input').prop('disabled', true);
                NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone3 input').prop('disabled', true);
                NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone4 input').prop('disabled', true);
                NWF$("#AirRFQMTable tbody tr:last").find('.lastMileRate_Zone5 input').prop('disabled', true);
                NWF$("#AirRFQMTable tbody tr:last").find('.ataRate input').prop('disabled', true);

                NWF$("#AirRFQMTable tbody tr:last").find('.attGac1 input').prop('disabled', true);
                NWF$("#AirRFQMTable tbody tr:last").find('.attGac2 input').prop('disabled', true);
                NWF$("#AirRFQMTable tbody tr:last").find('.airline input').prop('disabled', true);
    

            } else {
                NWF$("#AirRFQMTable tbody tr:last").find('.nqClass input').prop('checked', false);
            }

            NWF$("#AirRFQMTable tbody tr:last").find('.nf-repeater-deleterow-image').css("visibility", "hidden");

            incVal = incVal + 1;
            //add next row
           // NWF$(".approvers").find('a').click();
           tBody = '';

           tBody+='<tr>'+
           '<td class="ID" style='+'display:none'+'><input disabled = "true" type="text"></input></td>'+
           '<td class="regionGrid"><input  disabled = "true" type="text"></input></td>'+
           '<td class="originCountryGrid"><input  disabled = "true" type="text"></input></td>'+
           '<td class="originCityGrid"><input  disabled = "true" type="text"></input></td>'+
           '<td class="volumeGrid"><input  disabled = "true" type="text"></input></td>'+
           '<td class="attGac1"><input class="editable_cell" type="text"></input></td>'+
           '<td class="attGac2"><input class="editable_cell" type="text"></input></td>'+
           '<td class="airline"><input class="editable_cell" type="text" maxlength="3"></input></td>'+
           '<td class="surchargeSSC"><input type="text"></input></td>'+
           '<td class="surchargeFSC"><input type="text"></input></td>'+
           '<td class="surchargeSSC_FSC"><input type="text"></input></td>'+
           '<td class="ataRate"><input class="editable_cell" type="text"></input></td>'+
           '<td class="lastMileRate_Zone1"><input class="editable_cell" type="text"></input></td>'+
           '<td class="lastMileRate_Zone2"><input class="editable_cell" type="text"></input></td>'+
           '<td class="lastMileRate_Zone3"><input class="editable_cell" type="text"></input></td>'+
           '<td class="lastMileRate_Zone4"><input class="editable_cell" type="text"></input></td>'+
           '<td class="lastMileRate_Zone5"><input class="editable_cell" type="text"></input></td>'+
           '<td class="aTDRate_Zone1"><input disabled = "true" type="text"></input></td>'+
           '<td class="aTDRate_Zone2"><input disabled = "true" type="text"></input></td>'+
           '<td class="aTDRate_Zone3"><input disabled = "true" type="text"></input></td>'+
           '<td class="aTDRate_Zone4"><input disabled = "true" type="text"></input></td>'+
           '<td class="aTDRate_Zone5"><input disabled = "true" type="text"></input></td>'+
           mgfTargetSecondRound+
           '<td class="nqClass"><input type="checkbox"></input></td>'
           '</tr>';
       
           NWF$("#AirRFQMTable tbody").append(tBody);
       
        }
    }

   NWF$("#AirRFQMTable tbody tr:last").remove();

   var checkFormEdibleStatus = NWF$(".disableInputControl2 label").text();

//    if(checkFormEdibleStatus.toUpperCase() =='TRUE') {
//     NWF$("#AirRFQMTable tbody tr td input").attr('disabled', true);
//    }

}

function onQueryFailed() {
    alert("Failed");
}

function redrawRepeatingTable() {
    //delete all existing repeating table rows, then build them again
    // NWF$("#AirRFQMTable tbody tr").each(function (index) {
    //     if (index != 0) {
    //         NWF$(this).find('.nf-repeater-deleterow-image').click();
    //     }
    // });
    // NWF$("#AirRFQMTable tbody tr:last").find('.regionGrid input').val("");
    // NWF$("#AirRFQMTable tbody tr:last").find('.originCountryGrid input').val("");
    // NWF$("#AirRFQMTable tbody tr:last").find('.originCityGrid input').val("");
    NWF$("#AirRFQMTable tbody").empty();
}

function exportExcel(flag) {


    const fileName = flag;
    if (flag == 'AIR-RFQ') {
        data = this.AirRFQJSON;
    } else if (flag == 'AFF Origin Profile') {
        data = this.AFFOriginProfileJOSN;
    } else if (flag == 'Origin Local Charge') {
        data = this.OriginLocalChargeJSON;
    } else if (flag == 'Air-Whse Terminal') {
        if(this.AirWhaseTerminalJSON.length ==0) {
            data = [{
                "ID": '',
                "Country": '',
                "Location": '',
                "Warehouse Address (Eng)": '',
                "Warehouse Address (Chi - for HK & CN only)":'',
                "Booking Receiving Hour// Office Mon-Fri": '',
                "Booking Receiving Hour// Office Sat": '',
                "Booking Receiving Hour// Office Sun/PH": '',
                "Warehouse Operating Hour Mon-Fri": '',
                "Warehouse Operating Hour Cargo Reception Cutoff": '',
                "Warehouse Operating Hour Sat": '',
                "Warehouse Operating Hour Cargo Reception Cutoff2": '',
                "Warehouse Operating Hour Sun / PH": '',
                "Warehouse Operating Hour Cargo Reception Cutoff3": '',
                "Bonded/Non-Bonded Warehouse": '',
                "Remarks / Comment": '',
            }]
        } else {
            data = this.AirWhaseTerminalJSON;
        }
        
    } else if (flag == 'Firm Code') {
        data = this.FirmCodeJSON;
    } else if (flag == 'Communication Matrix') {
        if(this.communicationMatrixJSON.length ==0) {
            data = [{
                "ID": '',
                "Country": '',
                "Location": '',
                "Name": '',
                "Level of Communication": '',
                "Function": '',
                "Title": '',
                "Email":'',
                "Office Contact": '',
                "Mobile Contact": '',
            }]

        } else {
            data = this.communicationMatrixJSON;
        }
        
    } else if (flag == 'Accessorial Charge') {
        data = this.AccessorialChargeJSON;
    }

    const exportType = 'csv';
    window.exportFromJSON({
        data,
        fileName,
        exportType
    });
    // });

}



function onQueryFailed() {
    alert("Failed");
}

function retrieveAffOriginProfile() {
   
    var clientContext = new SP.ClientContext.get_current();
    //var siteurl = _spPageContextInfo.webAbsoluteUrl;
    var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'AFFOriginProfile');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View>" +
        "<Query>" +
        "<Where>" +
        "<Eq>" +
        "<FieldRef Name='RFQ_AFFID' />" +
        "<Value Type='Lookup'>"+RFQ_ID+"</Value>" +
        "</Eq>" +
        "</Where>" +
        "</Query>" +
        "</View>");

    //camlQuery.set_viewXml();


    this.collListItemAffOriginProfile = oList.getItems(camlQuery);
    clientContext.load(collListItemAffOriginProfile);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQuerySucceededAffOrgnProfile),
        Function.createDelegate(this, this.onQueryFailed));
}

function onQuerySucceededAffOrgnProfile() {

    var listItemEnumerator = collListItemAffOriginProfile.getEnumerator();
    this.AFFOriginProfileJOSN = [];
    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();

        this.AFFOriginProfileJOSN.push({

            "ID": oListItem.get_item('ID'),
            "Region": oListItem.get_item('Region'),
            "Country": oListItem.get_item('Country'),
            "Location": oListItem.get_item('Location'),
            "Business Entity Type": oListItem.get_item('BusinessEntityType')==null?'': oListItem.get_item('BusinessEntityType'),
            "% of Share (please indicate if local office is JV)": oListItem.get_item('PercentageOfShare')==null?'':oListItem.get_item('PercentageOfShare'),
            "CFS Facility Type": oListItem.get_item('CFSFacilityType')==null?'': oListItem.get_item('CFSFacilityType'),
            "CFS Facility Segregation Mode": oListItem.get_item('CFSFacilitySegregationArea')==null?'': oListItem.get_item('CFSFacilitySegregationArea'),
            "Receiving Agent": oListItem.get_item('ReceivingAgent')==null?'':oListItem.get_item('ReceivingAgent'),

        });
    }


    generateAffOriginProfileGrid();
}

function generateAffOriginProfileGrid() {

    redrawRepeatingTableAffProfile();

    for (let index = 0; index < this.AFFOriginProfileJOSN.length; index++) {

        NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.ID input').val(this.AFFOriginProfileJOSN[index]['ID']);
        NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.regionOriginProfile input').val(this.AFFOriginProfileJOSN[index]['Region']);
        NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.countryOriginProfile input').val(this.AFFOriginProfileJOSN[index]['Country']);
        NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.locationOriginProfile input').val(this.AFFOriginProfileJOSN[index]['Location']);
        NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.bussinessEntityOriginProfile select').val(this.AFFOriginProfileJOSN[index]['Business Entity Type']).trigger('change');
        NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.perShareOriginProfile input').val(this.AFFOriginProfileJOSN[index]['% of Share (please indicate if local office is JV)']);
        NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.cfsFacilityTypeOriginProfile select').val(this.AFFOriginProfileJOSN[index]['CFS Facility Type']).trigger('change');
        NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.cfsFacilitySegOriginProfile select').val(this.AFFOriginProfileJOSN[index]['CFS Facility Segregation Mode']).trigger('change');
        NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.recievingAgentOriginProfile select').val(this.AFFOriginProfileJOSN[index]['Receiving Agent']).trigger('change');

        NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.nf-repeater-deleterow-image').css("visibility", "hidden");
        //add next row
        NWF$(".OriginProfileRepeater").find('a').click();
    }

    NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.nf-repeater-deleterow-image').click();
}

function redrawRepeatingTableAffProfile() {
    //delete all existing repeating table rows, then build them again
    NWF$(".OriginProfileRepeater .nf-repeater-row").each(function (index) {
        if (index != 0) {
            NWF$(this).find('.nf-repeater-deleterow-image').click();
        }
    });
    NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.regionOriginProfile input').val("");
    NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.countryOriginProfile input').val("");
    NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.locationOriginProfile input').val("");
    NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.bussinessEntityOriginProfile input').val("");
    NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.perShareOriginProfile input').val("");
    NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.cfsFacilityTypeOriginProfile input').val("");
    NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.cfsFacilitySegOriginProfile input').val("");
    NWF$(".OriginProfileRepeater .nf-repeater-row:last").find('.recievingAgentOriginProfile input').val("");
}




function retrieveLocalCharge() {

    var Location = NWF$("#" + var_localChargeLocation).find("option:selected").text();
    this.Location = Location;

    if (this.OriginLocalChargeJSON.length > 0) {
        generateOriginLocalChargeGrid();
    } else {

        var clientContext = new SP.ClientContext.get_current();
        var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'OriginLocalCharge');
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<View>" +
            "<Query>" +
            "<Where>" +
            "<Eq>" +
            "<FieldRef Name='RFQ_AFFID' />" +
            "<Value Type='Lookup'>"+RFQ_ID+"</Value>" +
            "</Eq>" +
            "</Where>" +
            "</Query>" +
            "</View>");

        this.collListItemLocalCharge = oList.getItems(camlQuery);
        clientContext.load(collListItemLocalCharge);
        clientContext.executeQueryAsync(
            Function.createDelegate(this, this.onQuerySucceededLocalCharge),
            Function.createDelegate(this, this.onQueryFailed));

    }

}

function onQuerySucceededLocalCharge() {

    var listItemEnumerator = collListItemLocalCharge.getEnumerator();
    this.OriginLocalChargeJSON = [];

    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();


        this.OriginLocalChargeJSON.push({

            "ID": oListItem.get_item('ID'),
            "Country": oListItem.get_item('Country'),
            "Location": oListItem.get_item('Location'),
            "Currency": oListItem.get_item('Currency'),
            "Mode": oListItem.get_item('Mode'),
            "Charge Item Description": oListItem.get_item('ChargeItemDescription'),
            "Unit of Measurement": oListItem.get_item('UnitofMeasurement'),
            "Charge Amount Min": oListItem.get_item('RawChargeAmount_Min')==null?'':oListItem.get_item('RawChargeAmount_Min'),
            "Charge Amount per UOM": oListItem.get_item('RawChargeAmount_PerUOM')==null?'':oListItem.get_item('RawChargeAmount_PerUOM'),
            "(US) Min": oListItem.get_item('ChargeAmount_Min')==null?'':oListItem.get_item('ChargeAmount_Min'),
            "(US) per UOM": oListItem.get_item('ChargeAmount_PerUOM')==null?'':oListItem.get_item('ChargeAmount_PerUOM'),

        });

    }
    generateOriginLocalChargeGrid();
}

function generateOriginLocalChargeGrid() {

    redrawRepeatingTableLocalCharge();
    var inc = 0;

    for (let index = 0; index < this.OriginLocalChargeJSON.length; index++) {

        if (this.OriginLocalChargeJSON[index]['Location'].trim() == this.Location) {

            NWF$(".localChargeDisCurrency input").val(this.OriginLocalChargeJSON[index]['Currency']);
            NWF$(".localChargeDisCountry input").val(this.OriginLocalChargeJSON[index]['Country']);

            NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.ID input').val(this.OriginLocalChargeJSON[index]['ID']);
            NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.localChargeMode input').val(this.OriginLocalChargeJSON[index]['Mode']);
            NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.localChargeitemDesc input').val(this.OriginLocalChargeJSON[index]['Charge Item Description']);
            NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.localChargeMes input').val(this.OriginLocalChargeJSON[index]['Unit of Measurement']);
            NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.localChargeAmountMin input').val(this.OriginLocalChargeJSON[index]['Charge Amount Min']);
            NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.localChargeAmountUOM input').val(this.OriginLocalChargeJSON[index]['Charge Amount per UOM']);
            NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.localChargeUsMin input').val(this.OriginLocalChargeJSON[index]['(US) Min']);
            NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.localChargeUsPom input').val(this.OriginLocalChargeJSON[index]['(US) per UOM']);
            //NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.recievingAgentOriginProfile input').val(ReceivingAgent);

            var alreadylocalChargeAmountMin = NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.localChargeAmountMin input').prop('className').split("-_")[1];
            if(!!alreadylocalChargeAmountMin) {
                NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.localChargeAmountMin input').removeClass('inc-_' + alreadylocalChargeAmountMin);
            }

            var alreadylocalChargeAmountUOM = NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.localChargeAmountUOM input').prop('className').split("-_")[1];
            if(!!alreadylocalChargeAmountUOM) {
                NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.localChargeAmountUOM input').removeClass('inc-_' + alreadylocalChargeAmountUOM);
            }


            /**add class */
            NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.localChargeAmountMin input').addClass("inc-_" + inc)
            NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.localChargeAmountUOM input').addClass("inc-_" + inc)

            NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.nf-repeater-deleterow-image').css("visibility", "hidden");
            //add next row
            NWF$(".originLocalChargeRepeater").find('a').click();
            inc = inc + 1;

        }


    }

    NWF$(".originLocalChargeRepeater .nf-repeater-row:last").find('.nf-repeater-deleterow-image').click();

}

function redrawRepeatingTableLocalCharge() {
    //delete all existing repeating table rows, then build them again
    NWF$(".originLocalChargeRepeater .nf-repeater-row").each(function (index) {
        if (index != 0) {
            NWF$(this).find('.nf-repeater-deleterow-image').click();
        }
    });

}

function retrieveFirmCode() {

    var destnGateway = NWF$("#" + dstnGatewayFirmCode).find("option:selected").text();
    this.firmCodeDestGateway = destnGateway;

    if(this.FirmCodeJSON.length>0) {

        generateFirmCodeGrid();

    } else {

        var clientContext = new SP.ClientContext.get_current();
        //var siteurl = _spPageContextInfo.webAbsoluteUrl;
        var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'FirmCode');
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<View>" +
            "<Query>" +
            "<Where>" +
            "<Eq>" +
            "<FieldRef Name='RFQ_AFFID' />" +
            "<Value Type='Lookup'>"+RFQ_ID+"</Value>" +
            "</Eq>" +
            "</Where>" +
            "</Query>" +
            "</View>");

        //camlQuery.set_viewXml();
        this.collListItemFirmCode = oList.getItems(camlQuery);
        clientContext.load(collListItemFirmCode);
        clientContext.executeQueryAsync(
            Function.createDelegate(this, this.onQuerySucceededFirmCode),
            Function.createDelegate(this, this.onQueryFailed));

    }

    
}

function onQuerySucceededFirmCode() {

    var listItemEnumerator = collListItemFirmCode.getEnumerator();
    this.FirmCodeJSON = [];

    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();
        this.FirmCodeJSON.push({
            "ID": oListItem.get_item('ID'),
            "Forwarder Name": oListItem.get_item('ForwarderName'),
            "Destination Gateway": oListItem.get_item('DestinationGateway'),
            "Airlines": oListItem.get_item('Airlines'),
            "Firm Code": oListItem.get_item('FirmCode')==null?'':oListItem.get_item('FirmCode'),
        })

    }

    generateFirmCodeGrid();
}

function generateFirmCodeGrid() {

    redrawRepeatingTableFirmCode();
    var inc = 0;

    for (let index = 0; index < this.FirmCodeJSON.length; index++) {
        //const element = array[index];

        if (this.FirmCodeJSON[index]['Destination Gateway'].trim() == this.firmCodeDestGateway) { 

            NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.ID input').val(this.FirmCodeJSON[index]['ID']);
            NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.firmCodeForwarder input').val(this.FirmCodeJSON[index]['Forwarder Name']);
            NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.firmCodeDestinationGateway input').val(this.FirmCodeJSON[index]['Destination Gateway']);
            NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.firmCodeAirline input').val(this.FirmCodeJSON[index]['Airlines']);
            NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.firmCodeSecond input').val(this.FirmCodeJSON[index]['Firm Code']);
            
            var alreadyFirmCode = NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.firmCodeSecond input').prop('className').split("-_")[1];
            if(!!alreadyFirmCode) {
                NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.firmCodeSecond input').removeClass('inc-_' + alreadyFirmCode);
            }


            /**add class */
            NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.firmCodeSecond input').addClass("inc-_" + inc)
            
            
            NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.nf-repeater-deleterow-image').css("visibility", "hidden");
            //add next row
            NWF$(".firmCodeRepeater").find('a').click();

            inc = inc + 1;

        }

    }

    NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.nf-repeater-deleterow-image').click();

}

function redrawRepeatingTableFirmCode() {
    //delete all existing repeating table rows, then build them again
    NWF$(".firmCodeRepeater .nf-repeater-row").each(function (index) {
        if (index != 0) {
            NWF$(this).find('.nf-repeater-deleterow-image').click();
        }
    });
    NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.firmCodeForwarder input').val("");
    NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.firmCodeDestinationGateway input').val("");
    NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.firmCodeAirline input').val("");
    NWF$(".firmCodeRepeater .nf-repeater-row:last").find('.firmCodeSecond input').val("");
}

function retrieveAirWhaseTerminal() {

    var clientContext = new SP.ClientContext.get_current();
    //var siteurl = _spPageContextInfo.webAbsoluteUrl;
    var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'WareHouse');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View>" +
        "<Query>" +
        "<Where>" +
        "<Eq>" +
        "<FieldRef Name='RFQ_AFFID' />" +
        "<Value Type='Lookup'>"+RFQ_ID+"</Value>" +
        "</Eq>" +
        "</Where>" +
        "</Query>" +
        "</View>");

    //camlQuery.set_viewXml();
    this.collListItemAirwWhaseTerminal = oList.getItems(camlQuery);
    clientContext.load(collListItemAirwWhaseTerminal);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQuerySucceededAirWhaseTerminal),
        Function.createDelegate(this, this.onQueryFailed));
}

function onQuerySucceededAirWhaseTerminal() {

    var listItemEnumerator = collListItemAirwWhaseTerminal.getEnumerator();
    this.AirWhaseTerminalJSON = [];

    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();

        this.AirWhaseTerminalJSON.push(
            {
                "ID": oListItem.get_item('ID'),
                "Country": oListItem.get_item('Country'),
                "Location": oListItem.get_item('Location'),
                "Warehouse Address (Eng)": oListItem.get_item('WarehouseAddress_Eng'),
                "Warehouse Address (Chi - for HK & CN only)": oListItem.get_item('WarehouseAddress_Chi'),
                "Booking Receiving Hour// Office Mon-Fri": oListItem.get_item('BookingReceivingHour_MonToFri'),
                "Booking Receiving Hour// Office Sat": oListItem.get_item('BookingReceivingHour_Sat'),
                "Booking Receiving Hour// Office Sun/PH": oListItem.get_item('BookingReceivingHour_SunOrPH'),
                "Warehouse Operating Hour Mon-Fri": oListItem.get_item('WarehouseOperatingHour_MonToFri'),
                "Warehouse Operating Hour Cargo Reception Cutoff": oListItem.get_item('WarehouseOperatingHour_MonToFri_'),
                "Warehouse Operating Hour Sat": oListItem.get_item('WarehouseOperatingHour_Sat'),
                "Warehouse Operating Hour Cargo Reception Cutoff2": oListItem.get_item('WarehouseOperatingHour_Sat_Cargo'),
                "Warehouse Operating Hour Sun / PH": oListItem.get_item('WarehouseOperatingHour_SunOrPH'),
                "Warehouse Operating Hour Cargo Reception Cutoff3": oListItem.get_item('WarehouseOperatingHour_SunOrPH_C'),
                "Bonded/Non-Bonded Warehouse": oListItem.get_item('WarehouseBondedStatus'),
                "Remarks / Comment": oListItem.get_item('RemarksComment'),
            }
        )

    }
    generateAirWhaseTerminalGrid();

}

function generateAirWhaseTerminalGrid() {

    redrawRepeatingTableAirWhaseNTerminal();

    for (let index = 0; index < this.AirWhaseTerminalJSON.length; index++) {

        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.ID input').val(this.AirWhaseTerminalJSON[index]['ID']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.country input').val(this.AirWhaseTerminalJSON[index]['Country']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.location input').val(this.AirWhaseTerminalJSON[index]['Location']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.warehouseAddressEng input').val(this.AirWhaseTerminalJSON[index]['Warehouse Address (Eng)']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.warehouseAddressCH input').val(this.AirWhaseTerminalJSON[index]['Warehouse Address (Chi - for HK & CN only)'])
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.bookingRCVMON_FRI input').val(this.AirWhaseTerminalJSON[index]['Booking Receiving Hour// Office Mon-Fri']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.bookingRCVSAT input').val(this.AirWhaseTerminalJSON[index]['Booking Receiving Hour// Office Sat']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.bookingRCVSUN input').val(this.AirWhaseTerminalJSON[index]['Booking Receiving Hour// Office Sun/PH']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.wareHouseAddMON_FRI input').val(this.AirWhaseTerminalJSON[index]['Warehouse Operating Hour Mon-Fri']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.wareHouseCargoReception input').val(this.AirWhaseTerminalJSON[index]['Warehouse Operating Hour Cargo Reception Cutoff']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.wareHouseCargoSAT input').val(this.AirWhaseTerminalJSON[index]['Warehouse Operating Hour Sat']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.wareHouseCargoReception2 input').val(this.AirWhaseTerminalJSON[index]['Warehouse Operating Hour Cargo Reception Cutoff2']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.wareHouseSUNPH input').val(this.AirWhaseTerminalJSON[index]['Warehouse Operating Hour Sun / PH']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.wareHouseCargoReception3 input').val(this.AirWhaseTerminalJSON[index]['Warehouse Operating Hour Cargo Reception Cutoff3']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.bondedNonbondedWH input').val(this.AirWhaseTerminalJSON[index]['Bonded/Non-Bonded Warehouse']);
        NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.remarksComment input').val(this.AirWhaseTerminalJSON[index]['Remarks / Comment']);

        NWF$(".airWhaseNTerminalRepeater").find('a').click();

    }

    NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:last").find('.nf-repeater-deleterow-image').click();

}

function redrawRepeatingTableAirWhaseNTerminal() {
    //delete all existing repeating table rows, then build them again
    NWF$(".airWhaseNTerminalRepeater .nf-repeater-row").each(function (index) {
        if (index != 0) {
            NWF$(this).find('.nf-repeater-deleterow-image').click();
        }
    });

    // NWF$(".accessorialChargeRepeater .nf-repeater-row:last").find('.accChargeItemDesc input').val("");
    // NWF$(".accessorialChargeRepeater .nf-repeater-row:last").find('.accUnitOfMesu input').val("");
    // NWF$(".accessorialChargeRepeater .nf-repeater-row:last").find('.accPerUom input').val("");
}

function retrieveCommunicationMatrix() {

    var clientContext = new SP.ClientContext.get_current();
    //var siteurl = _spPageContextInfo.webAbsoluteUrl;
    var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'CommunicationMatrix');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View>" +
        "<Query>" +
        "<Where>" +
        "<Eq>" +
        "<FieldRef Name='RFQ_AFFID' />" +
        "<Value Type='Lookup'>"+RFQ_ID+"</Value>" +
        "</Eq>" +
        "</Where>" +
        "</Query>" +
        "</View>");

    //camlQuery.set_viewXml();

    this.collListItemCommunicationMatrix = oList.getItems(camlQuery);
    clientContext.load(collListItemCommunicationMatrix);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQuerySucceededCommunicationMatrix),
        Function.createDelegate(this, this.onQueryFailed));


}

function onQuerySucceededCommunicationMatrix() {

    var listItemEnumerator = collListItemCommunicationMatrix.getEnumerator();
    this.communicationMatrixJSON = [];
    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();

        this.communicationMatrixJSON.push({
            "ID": oListItem.get_item('ID'),
            "Country": oListItem.get_item('Country'),
            "Location": oListItem.get_item('Location'),
            "Name": oListItem.get_item('Name'),
            "Level of Communication": oListItem.get_item('LevelofCommunication'),
            "Function": oListItem.get_item('Function'),
            "Title": oListItem.get_item('Titles'),
            "Email": oListItem.get_item('Email'),
            "Office Contact": oListItem.get_item('OfficeContact'),
            "Mobile Contact": oListItem.get_item('MobileContact'),
        });

    }

    generateCommunicationMatrixGrid();
}

function generateCommunicationMatrixGrid() {

    redrawRepeatingTableCommunicationMatrix();

    for (let index = 0; index < this.communicationMatrixJSON.length; index++) {

        var record = this.communicationMatrixJSON[index];

        NWF$(".communicationMatrixRepeater .nf-repeater-row:last").find('.ID input').val(record['ID']);
        NWF$(".communicationMatrixRepeater .nf-repeater-row:last").find('.country input').val(record['Country']);
        NWF$(".communicationMatrixRepeater .nf-repeater-row:last").find('.location input').val(record['Location']);
        NWF$(".communicationMatrixRepeater .nf-repeater-row:last").find('.name input').val(record['Name']);
        NWF$(".communicationMatrixRepeater .nf-repeater-row:last").find('.levelOfCommunication input').val(record['Level of Communication']);
        NWF$(".communicationMatrixRepeater .nf-repeater-row:last").find('.function input').val(record['Function']);
        NWF$(".communicationMatrixRepeater .nf-repeater-row:last").find('.title input').val(record['Title']);
        NWF$(".communicationMatrixRepeater .nf-repeater-row:last").find('.email input').val(record['Email']);
        NWF$(".communicationMatrixRepeater .nf-repeater-row:last").find('.officeContact input').val(record['Office Contact']);
        NWF$(".communicationMatrixRepeater .nf-repeater-row:last").find('.mobileContact input').val(record['Mobile Contact']);

        NWF$(".communicationMatrixRepeater").find('a').click();

    }

    NWF$(".communicationMatrixRepeater .nf-repeater-row:last").find('.nf-repeater-deleterow-image').click();
}

function redrawRepeatingTableCommunicationMatrix() {

    NWF$(".communicationMatrixRepeater .nf-repeater-row").each(function (index) {
        if (index != 0) {
            NWF$(this).find('.nf-repeater-deleterow-image').click();
        }
    });

}





function retrieveAccessorialCharge() {
    var clientContext = new SP.ClientContext.get_current();
    //var siteurl = _spPageContextInfo.webAbsoluteUrl;
    var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'AccessorialCharge');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View>" +
        "<Query>" +
        "<Where>" +
        "<Eq>" +
        "<FieldRef Name='RFQ_AFFID' />" +
        "<Value Type='Lookup'>"+RFQ_ID+"</Value>" +
        "</Eq>" +
        "</Where>" +
        "</Query>" +
        "</View>");

    //camlQuery.set_viewXml();
    this.collListItemAccessorialCharge = oList.getItems(camlQuery);
    clientContext.load(collListItemAccessorialCharge);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQuerySucceededAccessorialCharge),
        Function.createDelegate(this, this.onQueryFailed));
}


function onQuerySucceededAccessorialCharge() {

    var listItemEnumerator = collListItemAccessorialCharge.getEnumerator();
    this.AccessorialChargeJSON = [];

    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();

        this.AccessorialChargeJSON.push(
            {
                "ID": oListItem.get_item('ID'),
                "Currency": oListItem.get_item('Currency'),
                "Charge Item Description": oListItem.get_item('ChargeItemDescription'),
                "Unit of Measurement": oListItem.get_item('UnitofMeasurement'),
                "per UOM": oListItem.get_item('RawChargeAmount_PerUOM')==null?'':oListItem.get_item('RawChargeAmount_PerUOM'),
            }
        )

    }
    console.log(this.AccessorialChargeJSON);
    generateAccessorialChargeGrid();
}

function generateAccessorialChargeGrid() {

    redrawRepeatingTableAccessorialCharge();
    var incVal = 0;

    for (let index = 0; index < this.AccessorialChargeJSON.length; index++) {

        NWF$(".accessorialChargeRepeater .nf-repeater-row:last").find('.ID input').val(this.AccessorialChargeJSON[index]['ID']);
        NWF$(".accessorialChargeRepeater .nf-repeater-row:last").find('.accChargeItemDesc input').val(this.AccessorialChargeJSON[index]['Charge Item Description']);
        NWF$(".accessorialChargeRepeater .nf-repeater-row:last").find('.accUnitOfMesu input').val(this.AccessorialChargeJSON[index]['Unit of Measurement']);
        NWF$(".accessorialChargeRepeater .nf-repeater-row:last").find('.accPerUom input').val(this.AccessorialChargeJSON[index]['per UOM']);
        NWF$(".currencAccCharge input").val('USD');

        //add class 
        NWF$(".accessorialChargeRepeater .nf-repeater-row:last").find('.accPerUom input').addClass('inc-_' + incVal);

        NWF$(".accessorialChargeRepeater .nf-repeater-row:last").find('.nf-repeater-deleterow-image').css("visibility", "hidden");
        //add next row
        NWF$(".accessorialChargeRepeater").find('a').click();
        incVal = incVal + 1;

    }

    NWF$(".accessorialChargeRepeater .nf-repeater-row:last").find('.nf-repeater-deleterow-image').click();
}

function redrawRepeatingTableAccessorialCharge() {
    //delete all existing repeating table rows, then build them again
    NWF$(".accessorialChargeRepeater .nf-repeater-row").each(function (index) {
        if (index != 0) {
            NWF$(this).find('.nf-repeater-deleterow-image').click();
        }
    });
    NWF$(".accessorialChargeRepeater .nf-repeater-row:last").find('.accChargeItemDesc input').val("");
    NWF$(".accessorialChargeRepeater .nf-repeater-row:last").find('.accUnitOfMesu input').val("");
    NWF$(".accessorialChargeRepeater .nf-repeater-row:last").find('.accPerUom input').val("");
}


function retrieveQnA() {
    //MGF_RFQ_AFF_QnA
    var clientContext = new SP.ClientContext.get_current();
    //var siteurl = _spPageContextInfo.webAbsoluteUrl;
    var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'QnA');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View>" +
        "<Query>" +
        "<Where>" +
        "<Eq>" +
        "<FieldRef Name='RFQ_AFFID' />" +
        "<Value Type='Lookup'>"+RFQ_ID+"</Value>" +
        "</Eq>" +
        "</Where>" +
        "</Query>" +
        "</View>");

    //camlQuery.set_viewXml();
    this.collListItemQnA = oList.getItems(camlQuery);
    clientContext.load(collListItemQnA);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQuerySucceededQnA),
        Function.createDelegate(this, this.onQueryFailed));

    // alert("here too");    
}

function onQuerySucceededQnA() {

    var listItemEnumerator = collListItemQnA.getEnumerator();
    this.QnAJSON = [];

    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();

        //console.log(oListItem.get_item('Editor').get_lookupValue());

        this.QnAJSON.push(
            {
                "ID": oListItem.get_item('ID'),
                "RFQ_AFFID": oListItem.get_item('RFQ_AFFID').get_lookupValue(),
                "Category": oListItem.get_item('Category'),
                "Q1": oListItem.get_item('_x0051_1'),
                "Q2": oListItem.get_item('_x0051_2'),
                "Q3": oListItem.get_item('_x0051_3'),
                "Q4": oListItem.get_item('_x0051_4'),
                "Q5": oListItem.get_item('_x0051_5'),
                "ANS1":oListItem.get_item('_x0041_NS1'),
                "ANS2": oListItem.get_item('_x0041_NS2'),
                "ANS3": oListItem.get_item('_x0041_NS3'),
                "ANS4": oListItem.get_item('_x0041_NS4'),
                "ANS5": oListItem.get_item('_x0041_NS5'),
                "ModifiedBy":oListItem.get_item('Editor').get_lookupValue(),
                "Time":oListItem.get_item('Modified').toLocaleDateString(),
               
            }
        )
    }

    // console.log(this.QnAJSON);
    
    generatfeedBackGrid(QnAJSON);
}

function generatfeedBackGrid(QALists) {

    //redrawRepeatingTableAffProfile();

    for (let index = 0; index < QALists.length; index++) {

       //console.log(QALists[index]['ModifiedBy']);

       if(!!QALists[index]['Q1']) {

        var ans = applicationStatus == 'QandAReview'?'':QALists[index]['ANS1'];
        NWF$(".feedBackRepeater .nf-repeater-row:last").find('.QnACategory label').html(QALists[index]['Category']);
        NWF$(".feedBackRepeater .nf-repeater-row:last").find('.QnAQuestion label').html(QALists[index]['Q1']);
        NWF$(".feedBackRepeater .nf-repeater-row:last").find('.QnAAnswer label').html(ans);
        NWF$(".feedBackRepeater .nf-repeater-row:last").find('.QnAReplied label').html(QALists[index]['ModifiedBy']);
        NWF$(".feedBackRepeater .nf-repeater-row:last").find('.QnAWhen label').html(QALists[index]['Time']);
        NWF$(".feedBackRepeater .nf-repeater-row:last").find('.nf-repeater-deleterow-image').css("visibility", "hidden");
        //add next row
        NWF$(".feedBackRepeater").find('a').click();

       }

       
    }

    NWF$(".feedBackRepeater .nf-repeater-row:last").find('.nf-repeater-deleterow-image').click();
}

function retrieveCurrency() {

     var clientContext = new SP.ClientContext.get_current();
     var oList = clientContext.get_web().get_lists().getByTitle('LGS_ORFQ_Currency');
     var camlQuery = new SP.CamlQuery();
     camlQuery.set_viewXml();
     this.collListItemCurrency = oList.getItems(camlQuery);
     clientContext.load(collListItemCurrency);
     clientContext.executeQueryAsync(
         Function.createDelegate(this, this.onQuerySucceededCurrencyList),
         Function.createDelegate(this, this.onQueryFailed));

}

function onQuerySucceededCurrencyList() {

    var listItemEnumerator = collListItemCurrency.getEnumerator();
    this.currencyJSON = [];

    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();

        this.currencyJSON.push(
            {
                "Title": oListItem.get_item('Title'),
                "USD": oListItem.get_item('USD'),
               
            }
        )
    }

     //console.log(this.currencyJSON);
}

function retrieveFeedBack() {
    
     var clientContext = new SP.ClientContext.get_current();
     var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'MGFFeedback');
     var camlQuery = new SP.CamlQuery();
     camlQuery.set_viewXml("<View>" +
         "<Query>" +
         "<Where>" +
         "<Eq>" +
         "<FieldRef Name='RFQ_AFFID' />" +
         "<Value Type='Lookup'>"+RFQ_ID+"</Value>" +
         "</Eq>" +
         "</Where>" +
         "</Query>" +
         "</View>");
 
     //camlQuery.set_viewXml();
     this.collListItemFeedback = oList.getItems(camlQuery);
     clientContext.load(collListItemFeedback);
     clientContext.executeQueryAsync(
         Function.createDelegate(this, this.onQuerySucceededFeedBack),
         Function.createDelegate(this, this.onQueryFailed));

}

function onQuerySucceededFeedBack() {

    var listItemEnumerator = collListItemFeedback.getEnumerator();
    this.feedbackJSON = [];

    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();

        this.feedbackJSON.push(
            {
                "ID": oListItem.get_item('ID'),
                "TabName": oListItem.get_item('TabName'),
                "FeedbackDraft":oListItem.get_item('FeedbackDraft'),
                "FeedbackLog":oListItem.get_item('FeedbackLog')
               
            }
        )
    }

     populateFeedback(this.feedbackJSON);
}

function populateFeedback(result) {

    for (let index = 0; index < result.length; index++) {
        const element = result[index];

        if(element['FeedbackLog'] == null || element['FeedbackLog'] =='') {
            element['FeedbackLog'] = 'No Feedback History'
        }

        if(element['TabName'] == 'Tab2') {
            NWF$(".airRFQFeedbackInput").val(element['FeedbackDraft']);
            NWF$(".airRFQFeedback span").text(element['FeedbackLog']);
        }

        if(element['TabName'] == 'Tab3') {
            NWF$(".AffOriginProfileFeedbackInput").val(element['FeedbackDraft']);
            NWF$(".AffOriginProfileFeedback span").text(element['FeedbackLog']);
        }

        if(element['TabName'] == 'Tab4') {
            NWF$(".OriginLocalChargeFeedbackInput").val(element['FeedbackDraft']);
            NWF$(".OriginLocalChargeFeedback span").text(element['FeedbackLog']);
        }

        if(element['TabName'] == 'Tab5') {
            NWF$(".AirWhaseNterminalFeedbackInput").val(element['FeedbackDraft']);
            NWF$(".AirWhaseNterminalFeedback span").text(element['FeedbackLog']);
        }

        if(element['TabName'] == 'Tab6') {
            NWF$(".FirmCodeFeedbackInput").val(element['FeedbackDraft']);
            NWF$(".FirmCodeFeedback span").text(element['FeedbackLog']);
        }

        if(element['TabName'] == 'Tab7') {
            NWF$(".CommunicationMatrixFeedbackInput").val(element['FeedbackDraft']);
            NWF$(".CommunicationMatrixFeedback span").text(element['FeedbackLog']);
        }

        if(element['TabName'] == 'Tab8') {
            NWF$(".AccessorialChargeFeedbackInput").val(element['FeedbackDraft']);
            NWF$(".AccessorialChargeFeedback span").text(element['FeedbackLog']);
        }

        
    }

}


function importexcel(selectedFile, flag) {
    // var reader = new FileReader();
    //reader.onload = function(e) {
    //var data = e.target.result;
    //const myObj = NWF$.csv.toObjects(data)
    //console.log(myObj);
    //};
    //reader.onerror = function(ex) {
    //console.log(ex);
    //};
    //reader.readAsText(file);

    //console.log(selectedFile);
    const promise = new Promise(resolve => {
        const fileContent = ReadFile(selectedFile)
        resolve(fileContent)
    })

    promise.then(fileContent => {
        // Use promise to wait for the file reading to finish.
        //console.log(fileContent);
        fileContent = removeExtraSpace(fileContent);
        const jsonData = NWF$.csv.toObjects(fileContent);
        //console.log(jsonData);
        if (flag == 'airRFQ') {
            importAirRFQ(jsonData);
        } else if (flag == 'affOriginProfile') {
            importAffOriginProfile(jsonData);
        } else if (flag == 'originLocalCharge') {
            importOriginLocalCharge(jsonData);
        } else if (flag == 'airWhaseNTerminal') {
            importAirWhaseNTerminal(jsonData);
        } else if (flag == 'firmCode') {
            importFirmCode(jsonData);
        } else if (flag == 'communicationMatrix') {
            importCommunicationMatrix(jsonData);
        } else if (flag == 'accessorialCharge') {
            importAccessorialCharge(jsonData);
        }

    })
}

async function ReadFile(file) {
    return await file.text()
}

function removeExtraSpace(stringData) {
    stringData = stringData.replace(/,( *)/gm, ",") // remove extra space
    stringData = stringData.replace(/^ *| *$/gm, "") // remove space on the beginning and end.
    return stringData
}

function importAirRFQ(jsonData) {

    // var itemArray = [];
    // var clientContext = new SP.ClientContext.get_current();
    // var oList = clientContext.get_web().get_lists().getByTitle('RFQ_AFF_AirRFQ');

    // for (var i = 0; i < jsonData.length; i++) {
    // //console.log();
    // var oListItem = oList.getItemById(jsonData[i].ID);
    // oListItem.set_item('Transit_ATATT', jsonData[i]["Transit Solution ATA TT (GAC = Day 1)"]);
    // oListItem.set_item('Transit_ATDTT', jsonData[i]["Transit Solution ATD TT (GAC = Day 1)"]);
    // oListItem.set_item('Transit_Airline', jsonData[i]["Transit Solution Airline"]);
    // oListItem.set_item('ATARate', jsonData[i]["ATA Rate/kg"]);
    // oListItem.set_item('LastMileRate_Zone1', jsonData[i]["Last Mile Rate/kg Zone 1"]);
    // oListItem.set_item('LastMileRate_Zone2', jsonData[i]["Last Mile Rate/kg Zone 2"]);
    // oListItem.set_item('LastMileRate_Zone3', jsonData[i]["Last Mile Rate/kg Zone 3"]);
    // oListItem.set_item('LastMileRate_Zone4', jsonData[i]["Last Mile Rate/kg Zone 4"]);
    // oListItem.set_item('LastMileRate_Zone5', jsonData[i]["Last Mile Rate/kg Zone 5"]);
    // oListItem.update();
    // itemArray.push(oListItem);
    // clientContext.load(itemArray[itemArray.length - 1]);
    // }
    // clientContext.executeQueryAsync(updateMultipleListItemsSuccess, updateMultipleListItemsFailed);

    for (var i = 0; i < jsonData.length; i++) {

        for (var j = 0; j < this.AirRFQJSON.length; j++) {

            if (jsonData[i]["ID"] == this.AirRFQJSON[j]["ID"] && jsonData[i]["Origin Country"] == this.AirRFQJSON[j]["Origin Country"] && jsonData[i]["Service level"] == this.AirRFQJSON[j]["Service level"] && jsonData[i]["Destination Gateway"] == this.AirRFQJSON[j]["Destination Gateway"]) {

                this.AirRFQJSON[i]["Surcharge SSC (fixed)"] = jsonData[i]["Surcharge SSC (fixed)"];
                this.AirRFQJSON[i]["Surcharge FSC (Vatos)"] = jsonData[i]["Surcharge FSC (Vatos)"];
                this.AirRFQJSON[i]["Surcharge Sea-Air SSC/FSC"] = jsonData[i]["Surcharge Sea-Air SSC/FSC"];
                this.AirRFQJSON[i]["ATA Rate/kg"] = jsonData[i]["ATA Rate/kg"];
                this.AirRFQJSON[i]["Last Mile Rate/kg Zone 1"] = jsonData[i]["Last Mile Rate/kg Zone 1"];
                this.AirRFQJSON[i]["Last Mile Rate/kg Zone 2"] = jsonData[i]["Last Mile Rate/kg Zone 2"];
                this.AirRFQJSON[i]["Last Mile Rate/kg Zone 3"] = jsonData[i]["Last Mile Rate/kg Zone 3"];
                this.AirRFQJSON[i]["Last Mile Rate/kg Zone 4"] = jsonData[i]["Last Mile Rate/kg Zone 4"];
                this.AirRFQJSON[i]["Last Mile Rate/kg Zone 5"] = jsonData[i]["Last Mile Rate/kg Zone 5"];

            }
        }
    }


    generateAirRFQGrid();


    // this.AirRFQJSON.push({
    // "ID": oListItem.get_item('ID'),
    // "Region": oListItem.get_item('Region'),
    // "Origin Country": oListItem.get_item('OriginCountry'),
    // "Origin City": oListItem.get_item('OriginCity'),
    // "Destination Gateway": oListItem.get_item('Dstn'),
    // "Volume": oListItem.get_item('Volume'),
    // "Service level": oListItem.get_item('ServiceLevel'),
    // "Transit Solution ATA TT (GAC = Day 1)": oListItem.get_item('Transit_ATATT'),
    // "Transit Solution ATD TT (GAC = Day 1)": oListItem.get_item('Transit_ATDTT'),
    // "Transit Solution Airline": oListItem.get_item('Transit_Airline'),
    // "Surcharge SSC (fixed)": oListItem.get_item('Surcharge_SSC'),
    // "Surcharge FSC (Vatos)": oListItem.get_item('Surcharge_FSC'),
    // "Surcharge Sea-Air SSC/FSC": oListItem.get_item('Surcharge_SSC_FSC'),
    // "ATA Rate/kg": oListItem.get_item('ATARate'),
    // "Last Mile Rate/kg Zone 1": oListItem.get_item('LastMileRate_Zone1'),
    // "Last Mile Rate/kg Zone 2": oListItem.get_item('LastMileRate_Zone2'),
    // "Last Mile Rate/kg Zone 3": oListItem.get_item('LastMileRate_Zone3'),
    // "Last Mile Rate/kg Zone 4": oListItem.get_item('LastMileRate_Zone4'),
    // "Last Mile Rate/kg Zone 5": oListItem.get_item('LastMileRate_Zone5'),
    // "ATD Rate/kg Zone 1": oListItem.get_item('ATDRate_Zone1'),
    // "ATD Rate/kg Zone 2": oListItem.get_item('ATDRate_Zone2'),
    // "ATD Rate/kg Zone 3": oListItem.get_item('ATDRate_Zone3'),
    // "ATD Rate/kg Zone 4": oListItem.get_item('ATDRate_Zone4'),
    // "ATD Rate/kg Zone 5": oListItem.get_item('ATDRate_Zone5'),
    // "NQ": oListItem.get_item('IsNQ'),
    // }) 
}

function importAffOriginProfile(jsonData) {

    // var itemArray = [];
    // var clientContext = new SP.ClientContext.get_current();
    // var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'Profile');

    // for (var i = 0; i < jsonData.length; i++) {
    //     //console.log();
    //     var oListItem = oList.getItemById(jsonData[i].ID);
    //     oListItem.set_item('BusinessEntityType', jsonData[i]["Business Entity Type"]);
    //     oListItem.set_item('PercentageOfShare', jsonData[i]["% of Share (please indicate if local office is JV)"]);
    //     oListItem.set_item('CFSFacilityType', jsonData[i]["CFS Facility Type"]);
    //     oListItem.set_item('CFSFacilitySegregationArea', jsonData[i]["CFS Facility Segregation Mode"]);
    //     oListItem.set_item('ReceivingAgent', jsonData[i]["Receiving Agent"]);
    //     oListItem.update();
    //     itemArray.push(oListItem);
    //     clientContext.load(itemArray[itemArray.length - 1]);
    // }
    // clientContext.executeQueryAsync(updateMultipleListItemsSuccess, updateMultipleListItemsFailed);

    for (var i = 0; i < jsonData.length; i++) {

        for (var j = 0; j < AFFOriginProfileJOSN.length; j++) {

            if (jsonData[i]["ID"] == AFFOriginProfileJOSN[j]["ID"]) {

                AFFOriginProfileJOSN[j]["Business Entity Type"] = jsonData[i]["Business Entity Type"];
                AFFOriginProfileJOSN[j]["% of Share (please indicate if local office is JV)"] = jsonData[i]["% of Share (please indicate if local office is JV)"];
                AFFOriginProfileJOSN[j]["CFS Facility Type"] = jsonData[i]["CFS Facility Type"];
                AFFOriginProfileJOSN[j]["CFS Facility Segregation Mode"] = jsonData[i]["CFS Facility Segregation Mode"];
                AFFOriginProfileJOSN[j]["Receiving Agent"] = jsonData[i]["Receiving Agent"];
            }
        }
    }

    generateAffOriginProfileGrid();


}

function importOriginLocalCharge(jsonData) {

    // var itemArray = [];
    // var clientContext = new SP.ClientContext.get_current();
    // var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'LocalCharge');

    // for (var i = 0; i < jsonData.length; i++) {
    //     //console.log();
    //     var oListItem = oList.getItemById(jsonData[i].ID);
    //     oListItem.set_item('RawChargeAmount_Min', jsonData[i]["Charge Amount Min"]);
    //     oListItem.set_item('RawChargeAmount_PerUOM', jsonData[i]["Charge Amount per UOM"]);
    //     oListItem.set_item('ChargeAmount_Min', jsonData[i]["Charge Amount Min"]);
    //     oListItem.set_item('ChargeAmount_PerUOM', jsonData[i]["Charge Amount per UOM"]);
    //     oListItem.update();
    //     itemArray.push(oListItem);
    //     clientContext.load(itemArray[itemArray.length - 1]);
    // }
    // clientContext.executeQueryAsync(updateMultipleListItemsSuccess, updateMultipleListItemsFailed);

    for (var i = 0; i < jsonData.length; i++) {

        for (var j = 0; j < OriginLocalChargeJSON.length; j++) {

            if (jsonData[i]["ID"] == OriginLocalChargeJSON[j]["ID"]) {

                OriginLocalChargeJSON[j]["Charge Amount Min"] = jsonData[i]["Charge Amount Min"];
                OriginLocalChargeJSON[j]["Charge Amount per UOM"] = jsonData[i]["Charge Amount per UOM"];
                OriginLocalChargeJSON[j]["(US) Min"] = jsonData[i]["(US) Min"];
                OriginLocalChargeJSON[j]["(US) per UOM"] = jsonData[i]["(US) per UOM"];
            }
        }
    }

    generateOriginLocalChargeGrid();


}

function importAirWhaseNTerminal(jsonData) {

    var itemArray = [];
    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'WareHouse');

    for (var i = 0; i < jsonData.length; i++) {
        //console.log();
        var oListItem = oList.getItemById(jsonData[i].ID);
        oListItem.set_item('Country', jsonData[i]["Country"]);
        oListItem.set_item('Location', jsonData[i]["Location"]);
        oListItem.set_item('WarehouseAddress_Eng', jsonData[i]["Warehouse Address (Eng)"]);
        oListItem.set_item('WarehouseAddress_Chi', jsonData[i]["Warehouse Address (Chi - for HK & CN only)"]);
        oListItem.set_item('BookingReceivingHour_MonToFri', jsonData[i]["Booking Receiving Hour// Office Mon-Fri"]);
        oListItem.set_item('BookingReceivingHour_Sat', jsonData[i]["Booking Receiving Hour// Office Sat"]);
        oListItem.set_item('BookingReceivingHour_SunOrPH', jsonData[i]["Booking Receiving Hour// Office Sun/PH"]);
        oListItem.set_item('WarehouseOperatingHour_MonToFri', jsonData[i]["Warehouse Operating Hour Mon-Fri"]);
        oListItem.set_item('WarehouseOperatingHour_MonToFri_', jsonData[i]["Warehouse Operating Hour Cargo Reception Cutoff"]);
        oListItem.set_item('WarehouseOperatingHour_Sat', jsonData[i]["Warehouse Operating Hour Sat"]);
        oListItem.set_item('WarehouseOperatingHour_Sat_Cargo', jsonData[i]["Warehouse Operating Hour Cargo Reception Cutoff2"]);
        oListItem.set_item('WarehouseOperatingHour_SunOrPH', jsonData[i]["Warehouse Operating Hour Sun / PH"]);
        oListItem.set_item('WarehouseOperatingHour_SunOrPH_C', jsonData[i]["Warehouse Operating Hour Cargo Reception Cutoff3"]);
        oListItem.set_item('WarehouseBondedStatus', jsonData[i]["Bonded/Non-Bonded Warehouse"]);
        oListItem.set_item('RemarksComment', jsonData[i]["Remarks / Comment"]);

        oListItem.update();
        itemArray.push(oListItem);
        clientContext.load(itemArray[itemArray.length - 1]);
    }
    clientContext.executeQueryAsync(updateMultipleListItemsSuccess, updateMultipleListItemsFailed);
}

function importFirmCode(jsonData) {

    for (var i = 0; i < jsonData.length; i++) {

        for (var j = 0; j < FirmCodeJSON.length; j++) {
            if (jsonData[i]["ID"] == FirmCodeJSON[j]["ID"]) {
                FirmCodeJSON[j]["Firm Code"] = jsonData[i]["Firm Code"];
            }
        }
    }

    generateFirmCodeGrid();


    // //this.FirmCodeJSON
    // var itemArray = [];
    // var clientContext = new SP.ClientContext.get_current();
    // var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'FirmCode');

    // for (var i = 0; i < jsonData.length; i++) {
    //     //console.log();
    //     var oListItem = oList.getItemById(jsonData[i].ID);
    //     oListItem.set_item('FirmCode', jsonData[i]["Firm Code"]);


    //     oListItem.update();
    //     itemArray.push(oListItem);
    //     clientContext.load(itemArray[itemArray.length - 1]);
    // }
    // clientContext.executeQueryAsync(updateMultipleListItemsSuccess, updateMultipleListItemsFailed);


}

function importCommunicationMatrix(jsonData) {

    var itemArray = [];
    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'Communication');

    for (var i = 0; i < jsonData.length; i++) {
        //console.log();
        var oListItem = oList.getItemById(jsonData[i].ID);
        oListItem.set_item('Country', jsonData[i]["Country"]);
        oListItem.set_item('Location', jsonData[i]["Location"]);
        oListItem.set_item('Name', jsonData[i]["Name"]);
        oListItem.set_item('Level_Communication', jsonData[i]["Level of Communication"]);
        oListItem.set_item('Function', jsonData[i]["Function"]);
        oListItem.set_item('Title', jsonData[i]["Title"]);
        oListItem.set_item('Email', jsonData[i]["Email"]);
        oListItem.set_item('OfficeContact', jsonData[i]["Office Contact"]);
        oListItem.set_item('MobileContact', jsonData[i]["Mobile Contact"]);


        oListItem.update();
        itemArray.push(oListItem);
        clientContext.load(itemArray[itemArray.length - 1]);
    }
    clientContext.executeQueryAsync(updateMultipleListItemsSuccess, updateMultipleListItemsFailed);
}

function importAccessorialCharge(jsonData) {

    var itemArray = [];
    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'AccessorialCharge');

    for (var i = 0; i < jsonData.length; i++) {
        //console.log();
        var oListItem = oList.getItemById(jsonData[i].ID);
        oListItem.set_item('RawChargeAmount_PerUOM', jsonData[i]["per UOM"]);


        oListItem.update();
        itemArray.push(oListItem);
        clientContext.load(itemArray[itemArray.length - 1]);
    }
    clientContext.executeQueryAsync(updateMultipleListItemsSuccess, updateMultipleListItemsFailed);
}

function updateQnA(btnStatus) {

    var itemArray = [];
    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'QnA');

    for (var i = 0; i < QnAJSON.length; i++) {
        //console.log();
        var oListItem = oList.getItemById(QnAJSON[i].ID);
        oListItem.set_item('_x0051_1', QnAJSON[i]["Q1"]);
        oListItem.set_item('_x0041_NS1', QnAJSON[i]["ANS1"]);
        
        oListItem.update();
        itemArray.push(oListItem);
        clientContext.load(itemArray[itemArray.length - 1]);
    }
    clientContext.executeQueryAsync(btnStatus=='saveOrConfirmedByMGF'?updateMasterListSuccess(btnStatus):updateMultipleListItemsSuccess, updateMultipleListItemsFailed);
}

function updateFeedbackDraft() {

    var updatedRecords = [];
    feedbackJSON.forEach(element => {

        if(element.ID) {

            updatedRecords.push({
                "ID":element['ID'],
                "FeedbackDraft":element['FeedbackDraft']

            })
        }
        
    });

    updateListItems(prefixTabsListName+'MGFFeedback', updatedRecords,
    function (items) {
        console.log("List Updated Successfully FirmCode");
    },
    function (sender, args) {
        alert('Error occured while creating tasks: FirmCode' + args.get_message());
    }
    );

}

function updateFeedback() {

    var updatedRecords = [];
    feedbackJSON.forEach(element => {

        if(element.ID) {

            var feedbackLog;
            element['FeedbackLog'] = element['FeedbackLog'] == 'No Feedback History'?'':element['FeedbackLog'];
            if(!!element['FeedbackDraft']) {
                feedbackLog = element['FeedbackLog']+'</br>'+ '-'+dateNtime+LogisticUser+ ':' +element['FeedbackDraft'];
            } else {
                feedbackLog = element['FeedbackLog']
            }
            
            updatedRecords.push({
                "ID":element['ID'],
                "FeedbackDraft": '',
                "FeedbackLog": feedbackLog
            })
        }
        
    });

    updateListItems(prefixTabsListName+'MGFFeedback', updatedRecords,
    function (items) {
        console.log("List Updated Successfully FirmCode");
    },
    function (sender, args) {
        alert('Error occured while creating tasks: FirmCode' + args.get_message());
    }
    );

}


function updateMasterListSuccess(btnStatus) {
    //alert("Success");
    var index = formStatusLists.indexOf(applicationStatus);
    //NWF$(".applicationStatusInput").val(formStatusLists[index+1]);
    var updatedRecords = [];

    if(btnStatus =='ruleOut') {

        updatedRecords.push({
            "ID":id,
            "MasterStatus":'Completed'
        })


    } else if(btnStatus =='return') {

        updatedRecords.push({
            "ID":id,
            "ApplicationStatus":formStatusLists[index-1]
        })

    } else if(btnStatus =='saveOrConfirmedByMGF' && applicationStatus=='PricingRound1Review') {

        var checkedStatus = NWF$('#'+varConfirmRound2 + ' input:checked').val();
        if(!!checkedStatus) {
            var yesNoBool = checkedStatus=='Yes'?1:0;
        }

        updatedRecords.push({
            "ID":id,
            "ApplicationStatus":formStatusLists[index+1],
            "IsRound2Listed":yesNoBool
        })
        
    } else if(btnStatus =='saveOrConfirmedByMGF' && applicationStatus=='PricingRound2Review') {

        var checkedStatus = NWF$('#'+varQualifyNextStage + ' input:checked').val();
        if(!!checkedStatus) {
            var yesNoBool = checkedStatus=='Yes'?1:0;
        }

        updatedRecords.push({
            "ID":id,
            "ApplicationStatus":formStatusLists[index+1],
            "IsReviewInvited":yesNoBool,
            "MasterStatus":'Completed'
        })
        
    } else {
        
        updatedRecords.push({
            "ID":id,
            "ApplicationStatus":formStatusLists[index+1]
        })

    }

    //varQualifyNextStage

   
   

    updateListItems(prefixTabsListName+'MasterFormList', updatedRecords,
    function (items) {
        alert("List Updated Successfully");
        NWF$(".nd-dialog-panel").css("display","none");
    },
    function (sender, args) {
        console.log('Error occured while creating tasks:MasterFormList' + args.get_message());
        NWF$(".nd-dialog-panel").css("display","none");
    }
    );
    //NWF$(".mainSubmitButton").click(); 
}

function updateMultipleListItemsSuccess() {

    NWF$(".nd-dialog-panel").css("display","none");
    console.log('Items Updated');
}

function updateMultipleListItemsFailed(sender, args) {

    NWF$(".nd-dialog-panel").css("display","none");
    alert('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
}

async function submitForm() {


    //'Submitted','QandAReview'
    NWF$(".nd-dialog-panel").css("display","block");
    if(applicationStatus == 'Submitted') {
        updateQnA('saveOrConfirmedByMGF');
        NWF$(".closeWithoutSaveAFF input").click();
    } else {

        const p1 = AirWhaseTerminalCreateOrUpdate();
        const p2 = CommunicationMatrixCreateOrUpdate();
        const p3 = AffOriginProfileUpdate();
        const p4 = OriginLocalChargeUpdate();
        const p5 = FirmCodeUpdate();
        const p6 = AccessorialChargeUpdate();
        const p7 = AirRFQUpdate();
        const p8 = updateQnA('Submit');
        const p9 = updateMasterListSuccess('submit');
        await Promise.all([p1,p2,p3,p4,p5,p6,p7, p8, p9]);

        NWF$(".closeWithoutSaveAFF input").click();

    }
   
   

}


async function saveAsDraft() {

    NWF$(".nd-dialog-panel").css("display","block");
    if(applicationStatus == 'QandAReview') {
        updateQnA('saveAsDraft');
        alert("List Updated Successfully");
        NWF$(".nd-dialog-panel").css("display","none");
    } else {

        const p1 =  updateQnA('saveAsDraft');
        const p2 = updateFeedbackDraft();
        await Promise.all([p1, p2]);
        alert("List Updated Successfully");
        NWF$(".nd-dialog-panel").css("display","none");

    }
}

async function saveAsDraftByAFF() {


    NWF$(".nd-dialog-panel").css("display","block");
    if(applicationStatus == 'Submitted') {
        updateQnA('saveAsDraftByAFF');

        alert("List Updated Successfully");
        NWF$(".nd-dialog-panel").css("display","none");
    } else {
        console.log("AFF Save as Draft");
        const p1 = AirWhaseTerminalCreateOrUpdate();
        const p2 = CommunicationMatrixCreateOrUpdate();
        const p3 = AffOriginProfileUpdate();
        const p4 = OriginLocalChargeUpdate();
        const p5 = FirmCodeUpdate();
        const p6 = AccessorialChargeUpdate();
        const p7 = AirRFQUpdate();
        const p8 =  updateQnA('saveAsDraftByAFF');
        await Promise.all([p1,p2,p3,p4,p5,p6,p7,p8]);

        alert("List Updated Successfully");
        NWF$(".nd-dialog-panel").css("display","none");

    }
}

async function confirmedByMGF() {

    NWF$(".nd-dialog-panel").css("display","block");
    const p1 = updateQnA('saveOrConfirmedByMGF');
    const p2 = updateFeedback();
    await Promise.all([p1,p2]);

    NWF$(".closeWithoutSaveLogistic input").click();

}

function closeWithoutSave() {
    
    alert("close without save");
}

async function returnForm() {

    NWF$(".nd-dialog-panel").css("display","block");
    const p1 = updateMasterListSuccess('return');
    const p2 = updateFeedback();
    await Promise.all([p1,p2]);

    NWF$(".closeWithoutSaveLogistic input").click();
}

async function ruleOut() {

    NWF$(".nd-dialog-panel").css("display","block");
    const p1 = updateMasterListSuccess('ruleOut');
    await Promise.all([p1]);

    NWF$(".closeWithoutSaveLogistic input").click();

}

async function submitOnBehalf() {

    NWF$(".nd-dialog-panel").css("display","block");
    const p1 = updateMasterListSuccess('submitOnBehalf');

    await Promise.all([p1]);

    NWF$(".closeWithoutSaveLogistic input").click();
}

function awardSummary() {
        var clientContext = new SP.ClientContext.get_current();
        var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'AwardSummary');
        var camlQuery = new SP.CamlQuery();
       // camlQuery.set_viewXml();
        camlQuery.set_viewXml("<View>" +
        "<Query>" +
        "<Where>" +
        "<Eq>" +
        "<FieldRef Name='RFQ_AFFID' />" +
        "<Value Type='Lookup'>"+RFQ_ID+"</Value>" +
        "</Eq>" +
        "</Where>" +
        "</Query>" +
        "</View>");

        this.collListItemAwardSummary = oList.getItems(camlQuery);
        clientContext.load(collListItemAwardSummary);
        clientContext.executeQueryAsync(
            Function.createDelegate(this, this.onQuerySucceededAwardSummary),
            Function.createDelegate(this, this.onQueryFailed));
   
}

function onQuerySucceededAwardSummary() {
   
    var listItemEnumerator = collListItemAwardSummary.getEnumerator();
    var id;

    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();
        id =  oListItem.get_item('ID');
    }

    var append_url = '//Lists/MGF_RFQ_AFF_AwardSummary/EditForm.aspx?ID='+id;
    NWF$('.AwardSumOpen').attr('href',site_URL+append_url);
    NWF$('.AwardSumOpen').click();

}

/***Batch Update function on Air RFQ */

function surchargeUpdate() {

    //alert(NWF$("#" + surchargeValue).val());

    var surcharge_value = parseInt(NWF$("#" + surchargeValue).val());

    NWF$("#AirRFQMTable tbody tr").each(function (index) {
       // if (index != 0) {
            var isDisabled = NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.surchargeSSC input').prop('disabled');
            if (!isDisabled) {
                NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.surchargeSSC input').val(surcharge_value);
            }

       // }
    });

    var destination = NWF$("#" + var_destination).find("option:selected").text();

    for (let index = 0; index < this.AirRFQJSON.length; index++) {
        //const element = array[index];

        if (this.AirRFQJSON[index]["Destination Gateway"].trim() == destination && this.AirRFQJSON[index]["Surcharge SSC (fixed)"] != 'incl.') {
            this.AirRFQJSON[index]["Surcharge SSC (fixed)"] = surcharge_value;
        }

    }

}

function lastMileUpdateZoneWise() {
    //var x = 1;
    //alert(NWF$(".batchUpdateZone"+x).val());

    var errmsg = [];
    var invalidval = false;

    var lastmilerates = ['Zone 1', 'Zone 2', 'Zone 3', 'Zone 4', 'Zone 5'];
    lastmilerates.forEach(function (target, idx) {


        var geqval = 0;
        var geqzone = -1;

        var leqval = 100;
        var leqzone = -1;

        var indexHold = parseInt(target.split(' ')[1]);
        var testNum = parseInt(NWF$(".batchUpdateZone" + indexHold).val());

        for (var geqidx = indexHold - 1; geqidx > 0; geqidx--) {
            var geqcmp = parseInt(NWF$(".batchUpdateZone" + geqidx).val());
            if (geqcmp > testNum) {
                geqzone = geqidx;
                geqval = geqcmp;
                break;
            }
        }

        for (var leqidx = indexHold + 1; leqidx < lastmilerates.length; leqidx++) {
            var leqcmp = parseInt(NWF$(".batchUpdateZone" + leqidx).val());
            if (leqcmp < testNum) {
                leqzone = leqidx;
                leqval = leqcmp;
                break;
            }
        }


        // alert(testNum);
        if (!isNaN(testNum) && testNum > 0 && testNum <= 100 && testNum >= geqval && testNum <= leqval) {

        } else {
            var basemsg = "Last Mile Rate Zone " + (idx + 1) + " value";
            if (isNaN(testNum)) {
                errmsg.push(basemsg + " is not number");
            }
            if (testNum < 0) {
                errmsg.push(basemsg + " is less than 0");
            }
            if (testNum >= 100) {
                errmsg.push(basemsg + " is greater than or equal 100");
            }
            if (geqzone !== -1 && testNum < geqval) {
                errmsg.push(basemsg + " is less than Zone " + (geqzone));
            }
            if (leqzone !== -1 && testNum > leqval) {
                errmsg.push(basemsg + " is greater than Zone " + (leqzone));
            }
            invalidval = true;
        }
    });

    if (invalidval) {
        window.alert(errmsg.join("\n"));
    } else {
        var zone1 = parseInt(NWF$(".batchUpdateZone1").val());
        var zone2 = parseInt(NWF$(".batchUpdateZone2").val());
        var zone3 = parseInt(NWF$(".batchUpdateZone3").val());
        var zone4 = parseInt(NWF$(".batchUpdateZone4").val());
        var zone5 = parseInt(NWF$(".batchUpdateZone5").val());

        NWF$("#AirRFQMTable tbody tr").each(function (index) {
           // if (index != 0) {
                var ataRateValue = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.ataRate input').val());

                if (NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.nqClass input').prop('checked') == false) {

                    NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone1 input').val(zone1);
                    NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.aTDRate_Zone1 input').val(zone1 + ataRateValue);

                    NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone2 input').val(zone2);
                    NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.aTDRate_Zone2 input').val(zone2 + ataRateValue);

                    NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone3 input').val(zone3);
                    NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.aTDRate_Zone3 input').val(zone3 + ataRateValue);

                    NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone4 input').val(zone4);
                    NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.aTDRate_Zone4 input').val(zone4 + ataRateValue);

                    NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone5 input').val(zone5);
                    NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.aTDRate_Zone5 input').val(zone5 + ataRateValue);
                }
            //}
        });

        var destination = NWF$("#" + var_destination).find("option:selected").text();

        for (let index = 0; index < this.AirRFQJSON.length; index++) {
            //const element = array[index];

            if (this.AirRFQJSON[index]["Destination Gateway"].trim() == destination && parseInt(this.AirRFQJSON[index]["NQ"]) == 0) {
                this.AirRFQJSON[index]["Last Mile Rate/kg Zone 1"] = zone1;
                this.AirRFQJSON[index]["Last Mile Rate/kg Zone 2"] = zone2;
                this.AirRFQJSON[index]["Last Mile Rate/kg Zone 3"] = zone3;
                this.AirRFQJSON[index]["Last Mile Rate/kg Zone 4"] = zone4;
                this.AirRFQJSON[index]["Last Mile Rate/kg Zone 5"] = zone5;

                this.AirRFQJSON[index]["ATD Rate/kg Zone 1"] = zone1 + parseInt(this.AirRFQJSON[index]["ATA Rate/kg"]);
                this.AirRFQJSON[index]["ATD Rate/kg Zone 2"] = zone2 + parseInt(this.AirRFQJSON[index]["ATA Rate/kg"]);
                this.AirRFQJSON[index]["ATD Rate/kg Zone 3"] = zone3 + parseInt(this.AirRFQJSON[index]["ATA Rate/kg"]);
                this.AirRFQJSON[index]["ATD Rate/kg Zone 4"] = zone4 + parseInt(this.AirRFQJSON[index]["ATA Rate/kg"]);
                this.AirRFQJSON[index]["ATD Rate/kg Zone 5"] = zone5 + parseInt(this.AirRFQJSON[index]["ATA Rate/kg"]);
            }

        }

    }

}

function updateRFQJsonAtaRate(ataRateValue, index) {

    var originCityGrid = NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.originCityGrid input').val();

    var lastMileZone1 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone1 input').val());
    var lastMileZone2 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone2 input').val());
    var lastMileZone3 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone3 input').val());
    var lastMileZone4 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone4 input').val());
    var lastMileZone5 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone5 input').val());



    for (let index = 0; index < this.AirRFQJSON.length; index++) {

        if ((this.AirRFQJSON[index]["Service level"].trim() == this.serviceFilter) && (this.AirRFQJSON[index]["Destination Gateway"].trim() == this.destinationFilter) && (this.AirRFQJSON[index]["Origin City"].trim() == originCityGrid)) {

            this.AirRFQJSON[index]["ATD Rate/kg Zone 1"] = lastMileZone1 + ataRateValue;
            this.AirRFQJSON[index]["ATD Rate/kg Zone 2"] = lastMileZone2 + ataRateValue;
            this.AirRFQJSON[index]["ATD Rate/kg Zone 3"] = lastMileZone3 + ataRateValue;
            this.AirRFQJSON[index]["ATD Rate/kg Zone 4"] = lastMileZone4 + ataRateValue;
            this.AirRFQJSON[index]["ATD Rate/kg Zone 5"] = lastMileZone5 + ataRateValue;
            this.AirRFQJSON[index]["ATA Rate/kg"] = ataRateValue;
        }

    }
}

function updateRFQJsonNQ(isChecked, index) {

    var originCityGrid = NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.originCityGrid input').val();

    for (let index = 0; index < this.AirRFQJSON.length; index++) {

        if ((this.AirRFQJSON[index]["Service level"].trim() == this.serviceFilter) && (this.AirRFQJSON[index]["Destination Gateway"].trim() == this.destinationFilter) && (this.AirRFQJSON[index]["Origin City"].trim() == originCityGrid)) {

            this.AirRFQJSON[index]["ATD Rate/kg Zone 1"] = 0;
            this.AirRFQJSON[index]["ATD Rate/kg Zone 2"] = 0;
            this.AirRFQJSON[index]["ATD Rate/kg Zone 3"] = 0;
            this.AirRFQJSON[index]["ATD Rate/kg Zone 4"] = 0;
            this.AirRFQJSON[index]["ATD Rate/kg Zone 5"] = 0;
            this.AirRFQJSON[index]["ATA Rate/kg"] = 0;

            this.AirRFQJSON[index]["NQ"] = isChecked == true ? 1 : 0;

            this.AirRFQJSON[index]["Last Mile Rate/kg Zone 1"] = 0;
            this.AirRFQJSON[index]["Last Mile Rate/kg Zone 2"] = 0;
            this.AirRFQJSON[index]["Last Mile Rate/kg Zone 3"] = 0;
            this.AirRFQJSON[index]["Last Mile Rate/kg Zone 4"] = 0;
            this.AirRFQJSON[index]["Last Mile Rate/kg Zone 5"] = 0;
            if(this.serviceFilter == 'DEF') {
                this.AirRFQJSON[index]["Transit Solution ATA TT (GAC = Day 1)"] = 0;
                this.AirRFQJSON[index]["Transit Solution ATD TT (GAC = Day 1)"] = 0;
            }    
            this.AirRFQJSON[index]["Transit Solution Airline"] = '';
        }

    }

}

function updateRFQJsonLastMile(index) {

    var originCityGrid = NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.originCityGrid input').val();

    var lastMileZone1 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone1 input').val());
    var lastMileZone2 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone2 input').val());
    var lastMileZone3 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone3 input').val());
    var lastMileZone4 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone4 input').val());
    var lastMileZone5 = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.lastMileRate_Zone5 input').val());


    var ataRateValue = parseInt(NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.ataRate input').val());


    for (let index = 0; index < this.AirRFQJSON.length; index++) {

        if ((this.AirRFQJSON[index]["Service level"].trim() == this.serviceFilter) && (this.AirRFQJSON[index]["Destination Gateway"].trim() == this.destinationFilter) && (this.AirRFQJSON[index]["Origin City"].trim() == originCityGrid)) {

            this.AirRFQJSON[index]["ATD Rate/kg Zone 1"] = lastMileZone1 + ataRateValue;
            this.AirRFQJSON[index]["ATD Rate/kg Zone 2"] = lastMileZone2 + ataRateValue;
            this.AirRFQJSON[index]["ATD Rate/kg Zone 3"] = lastMileZone3 + ataRateValue;
            this.AirRFQJSON[index]["ATD Rate/kg Zone 4"] = lastMileZone4 + ataRateValue;
            this.AirRFQJSON[index]["ATD Rate/kg Zone 5"] = lastMileZone5 + ataRateValue;

            this.AirRFQJSON[index]["Last Mile Rate/kg Zone 1"] = lastMileZone1;
            this.AirRFQJSON[index]["Last Mile Rate/kg Zone 2"] = lastMileZone2;
            this.AirRFQJSON[index]["Last Mile Rate/kg Zone 3"] = lastMileZone3;
            this.AirRFQJSON[index]["Last Mile Rate/kg Zone 4"] = lastMileZone4;
            this.AirRFQJSON[index]["Last Mile Rate/kg Zone 5"] = lastMileZone5;
        }

    }

}

function updateRFQJsonATAAndATD(index) {

    var Id = NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.ID input').val();
    var ata = NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.attGac1 input').val();
    var atd = NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.attGac2 input').val();

    for (let index = 0; index < this.AirRFQJSON.length; index++) {
        if (this.AirRFQJSON[index]['ID'] == Id) {
            this.AirRFQJSON[index]["Transit Solution ATA TT (GAC = Day 1)"] = ata;
            this.AirRFQJSON[index]["Transit Solution ATD TT (GAC = Day 1)"] = atd;
        }
    }
}

function updateRFQJsonAirline(index) {

    var Id = NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.ID input').val();
    var airlineVal = NWF$("#AirRFQMTable tbody tr:eq(" + index + ")").find('.airline input').val();

    for (let index = 0; index < this.AirRFQJSON.length; index++) {
        if (this.AirRFQJSON[index]['ID'] == Id) {
            this.AirRFQJSON[index]["Transit Solution Airline"] = airlineVal;
        }
    }
    
}

function updateLocalChargeAmountMinJSON(index) {


    //console.log(this.OriginLocalChargeJSON);
    var chargeAmountMin = NWF$(".originLocalChargeRepeater .nf-repeater-row:eq(" + index + ")").find('.localChargeAmountMin input').val();
    var chargeUSMin = NWF$(".originLocalChargeRepeater .nf-repeater-row:eq(" + index + ")").find('.localChargeUsMin input').val();
    var Id = NWF$(".originLocalChargeRepeater .nf-repeater-row:eq(" + index + ")").find('.ID input').val();

    for (let index = 0; index < this.OriginLocalChargeJSON.length; index++) {
        if (this.OriginLocalChargeJSON[index]['Location'].trim() == this.Location && this.OriginLocalChargeJSON[index]['ID'] == Id) {
            this.OriginLocalChargeJSON[index]['Charge Amount Min'] = chargeAmountMin;
            this.OriginLocalChargeJSON[index]['(US) Min'] = chargeUSMin;
        }
    }
}

function updateLocalChargeAmountPerUOMJSON(index) {

    var chargeAmountperUOM = NWF$(".originLocalChargeRepeater .nf-repeater-row:eq(" + index + ")").find('.localChargeAmountUOM input').val();
    var chargeUSperUOM = NWF$(".originLocalChargeRepeater .nf-repeater-row:eq(" + index + ")").find('.localChargeUsPom input').val();
    var Id = NWF$(".originLocalChargeRepeater .nf-repeater-row:eq(" + index + ")").find('.ID input').val();

    for (let index = 0; index < this.OriginLocalChargeJSON.length; index++) {
        if (this.OriginLocalChargeJSON[index]['Location'].trim() == this.Location && this.OriginLocalChargeJSON[index]['ID'] == Id) {
            this.OriginLocalChargeJSON[index]['Charge Amount per UOM'] = chargeAmountperUOM;
            this.OriginLocalChargeJSON[index]['(US) per UOM'] = chargeUSperUOM;
        }
    }
}

function updateFirmcodeJSON(value, index) {

    
    var firmCodeVal = NWF$(".firmCodeRepeater .nf-repeater-row:eq(" + index + ")").find('.firmCodeSecond input').val();
    var Id = NWF$(".firmCodeRepeater .nf-repeater-row:eq(" + index + ")").find('.ID input').val();


    for (let index = 0; index < this.FirmCodeJSON.length; index++) {
        if (this.FirmCodeJSON[index]['ID'] == Id) {
            this.FirmCodeJSON[index]['Firm Code'] = firmCodeVal;
        }
    }

}


function formatfloat(num, n) {
    return Number(parseFloat(Math.round(num * Math.pow(10, n)) / Math.pow(10, n)).toFixed(n));
}

function convertCurrency(currencyMap, currCurrency, currValue) {
    if (currCurrency === "USD") {
        return formatfloat(currValue, 2);
    }
    if (typeof currencyMap[currCurrency] !== "undefined" && currencyMap[currCurrency]) {
        return formatfloat(currencyMap[currCurrency] * currValue, 2);
    }
    return 0;
}



function AirWhaseTerminalCreateOrUpdate() {

   var updatedRecords = [];
   var insertedRecords = [];
   var deletedRecord = [];
    NWF$(".airWhaseNTerminalRepeater .nf-repeater-row").each(function (index) {
        if (index != 0) {
            var initialCondition = NWF$(".airWhaseNTerminalRepeater .nf-repeater-row:eq("+ index + ")");
            if(initialCondition.find('.ID input').val()) {   
                updatedRecords.push({
                'ID':initialCondition.find('.ID input').val(),
                'Country':initialCondition.find('.country input').val(),
                'Location':initialCondition.find('.location input').val(),
                'WarehouseAddress_Eng':initialCondition.find('.warehouseAddressEng input').val(),
                'WarehouseAddress_Chi':initialCondition.find('.warehouseAddressCH input').val(),
                'BookingReceivingHour_MonToFri':initialCondition.find('.bookingRCVMON_FRI input').val(),
                'BookingReceivingHour_Sat':initialCondition.find('.bookingRCVSAT input').val(),
                'BookingReceivingHour_SunOrPH':initialCondition.find('.bookingRCVSUN input').val(),
                'WarehouseOperatingHour_MonToFri':initialCondition.find('.wareHouseAddMON_FRI input').val(),
                'WarehouseOperatingHour_MonToFri_':initialCondition.find('.wareHouseCargoReception input').val(),
                'WarehouseOperatingHour_Sat':initialCondition.find('.wareHouseCargoSAT input').val(),
                'WarehouseOperatingHour_Sat_Cargo':initialCondition.find('.wareHouseCargoReception2 input').val(),
                'WarehouseOperatingHour_SunOrPH':initialCondition.find('.wareHouseSUNPH input').val(),
                'WarehouseOperatingHour_SunOrPH_C':initialCondition.find('.wareHouseCargoReception3 input').val(),
                'WarehouseBondedStatus':initialCondition.find('.bondedNonbondedWH input').val(),
                'RemarksComment':initialCondition.find('.remarksComment input').val(),                    
                })

            } else {
                insertedRecords.push({
                    'Country':initialCondition.find('.country input').val(),
                    'RFQ_AFFID':RFQ_ID,
                    'Location':initialCondition.find('.location input').val(),
                    'WarehouseAddress_Eng':initialCondition.find('.warehouseAddressEng input').val(),
                    'WarehouseAddress_Chi':initialCondition.find('.warehouseAddressCH input').val(),
                    'BookingReceivingHour_MonToFri':initialCondition.find('.bookingRCVMON_FRI input').val(),
                    'BookingReceivingHour_Sat':initialCondition.find('.bookingRCVSAT input').val(),
                    'BookingReceivingHour_SunOrPH':initialCondition.find('.bookingRCVSUN input').val(),
                    'WarehouseOperatingHour_MonToFri':initialCondition.find('.wareHouseAddMON_FRI input').val(),
                    'WarehouseOperatingHour_MonToFri_':initialCondition.find('.wareHouseCargoReception input').val(),
                    'WarehouseOperatingHour_Sat':initialCondition.find('.wareHouseCargoSAT input').val(),
                    'WarehouseOperatingHour_Sat_Cargo':initialCondition.find('.wareHouseCargoReception2 input').val(),
                    'WarehouseOperatingHour_SunOrPH':initialCondition.find('.wareHouseSUNPH input').val(),
                    'WarehouseOperatingHour_SunOrPH_C':initialCondition.find('.wareHouseCargoReception3 input').val(),
                    'WarehouseBondedStatus':initialCondition.find('.bondedNonbondedWH input').val(),
                    'RemarksComment':initialCondition.find('.remarksComment input').val(),
                })
            }
        }
    });

    this.AirWhaseTerminalJSON.forEach(element => {
        var jsonID = element['ID'];
        var flag = false;
        updatedRecords.forEach(element2=>{
            var ID = element2['ID'];
            if(ID==jsonID) {
                flag = true;
            }
        });

        if(flag == false) {
            deletedRecord.push(jsonID);
        }
        
    });


    if(insertedRecords.length>=1 &&insertedRecords[0]['Country'].trim().length>0) {

        createListItems(prefixTabsListName+'WareHouse', insertedRecords,
        function (items) {
            console.log("List Created Successfully airwhase terminal");
        },
        function (sender, args) {
            alert('Error occured while creating tasks:airwhase terminal' + args.get_message());
        }
        );

    }

   
    if(updatedRecords.length>0) {

        updateListItems(prefixTabsListName+'WareHouse', updatedRecords,
        function (items) {
            console.log("List Updated Successfully airwhase terminal");
        },
        function (sender, args) {
            console.log('Error occured while creating tasks: airwhase terminal' + args.get_message());
        }
        );

    }


    if(deletedRecord.length>=1) {
        deleteListItems(prefixTabsListName+'WareHouse', deletedRecord,
        function (items) {
            console.log("List Deleted Successfully airwhase terminal");
        },
        function (sender, args) {
            console.log('Error occured while Deleting tasks: airwhase terminal' + args.get_message());
        }
        );

    }
   


}


function CommunicationMatrixCreateOrUpdate() {
    var updatedRecords = [];
    var insertedRecords = [];
    var deletedRecord = [];
    
    NWF$(".communicationMatrixRepeater .nf-repeater-row").each(function (index) {
        if (index != 0) {
            var initialCondition = NWF$(".communicationMatrixRepeater .nf-repeater-row:eq("+ index + ")");
            if(initialCondition.find('.ID input').val()) {   
                updatedRecords.push({
                    "ID":initialCondition.find('.ID input').val(),
                    "Country": initialCondition.find('.country input').val(),
                    "Location": initialCondition.find('.location input').val(),
                    "Name": initialCondition.find('.name input').val(),
                    "LevelofCommunication":initialCondition.find('.levelOfCommunication input').val(),
                    "Function": initialCondition.find('.function input').val(),
                    "Titles": initialCondition.find('.title input').val(),
                    "Email": initialCondition.find('.email input').val(),
                    "OfficeContact": initialCondition.find('.officeContact input').val(),
                    "MobileContact": initialCondition.find('.mobileContact input').val()

                })
            } else {
                insertedRecords.push({
                    "RFQ_AFFID":RFQ_ID,
                    "Country": initialCondition.find('.country input').val(),
                    "Location": initialCondition.find('.location input').val(),
                    "Name": initialCondition.find('.name input').val(),
                    "LevelofCommunication":initialCondition.find('.levelOfCommunication input').val(),
                    "Function": initialCondition.find('.function input').val(),
                    "Titles": initialCondition.find('.title input').val(),
                    "Email": initialCondition.find('.email input').val(),
                    "OfficeContact": initialCondition.find('.officeContact input').val(),
                    "MobileContact": initialCondition.find('.mobileContact input').val()
                    
                })
            }    
        }
    });

    
    this.communicationMatrixJSON.forEach(element => {
        var jsonID = element['ID'];
        var flag = false;
        updatedRecords.forEach(element2=>{
            var ID = element2['ID'];
            if(ID==jsonID) {
                flag = true;
            }
        });

        if(flag == false) {
            deletedRecord.push(jsonID);
        }
        
    });

    if(insertedRecords.length>=1 && insertedRecords[0]['Country'].trim().length>0) {

        createListItems(prefixTabsListName+'CommunicationMatrix', insertedRecords,
        function (items) {
            console.log("List Created Successfully CommunicationMatrix");
        },
        function (sender, args) {
            alert('Error occured while creating tasks: CommunicationMatrix' + args.get_message());
        }
        );

    }

    if(updatedRecords.length>=1) {
            updateListItems(prefixTabsListName+'CommunicationMatrix', updatedRecords,
        function (items) {
            console.log("List Updated Successfully CommunicationMatrix");
        },
        function (sender, args) {
            console.log('Error occured while creating tasks: CommunicationMatrix' + args.get_message());
        }
        );
    }

    if(deletedRecord.length>=1) {
        deleteListItems(prefixTabsListName+'CommunicationMatrix', deletedRecord,
        function (items) {
            console.log("List Deleted Successfully CommunicationMatrix");
        },
        function (sender, args) {
            console.log('Error occured while Deleting tasks: CommunicationMatrix' + args.get_message());
        }
        );

    }
    

}

function AffOriginProfileUpdate() {
    
    var updatedRecords = [];

    NWF$(".OriginProfileRepeater .nf-repeater-row").each(function (index) {
        if (index != 0) {
            var initialCondition = NWF$(".OriginProfileRepeater .nf-repeater-row:eq("+ index + ")");

            if(initialCondition.find('.ID input').val()) {   
                updatedRecords.push({
                    "ID":initialCondition.find('.ID input').val(),
                    "BusinessEntityType":initialCondition.find('.bussinessEntityOriginProfile select').val(),
                    "PercentageOfShare":initialCondition.find('.perShareOriginProfile input').val(),
                    "CFSFacilityType":initialCondition.find('.cfsFacilityTypeOriginProfile select').val(),
                    "CFSFacilitySegregationArea":initialCondition.find('.cfsFacilitySegOriginProfile select').val(),
                    "ReceivingAgent":initialCondition.find('.recievingAgentOriginProfile select').val()
                })
            }    

        }
    });


    updateListItems(prefixTabsListName+'AFFOriginProfile', updatedRecords,
    function (items) {
        console.log("List Updated Successfully AffOriginProfile");
    },
    function (sender, args) {
        alert('Error occured while creating tasks: AffOriginProfile' + args.get_message());
    }
    );


    // var itemArray = [];
    // var clientContext = new SP.ClientContext.get_current();
    // var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'Profile');

    // for (var i = 0; i < jsonData.length; i++) {
    //     //console.log();
    //     var oListItem = oList.getItemById(jsonData[i].ID);
    //     oListItem.set_item('BusinessEntityType', jsonData[i]["Business Entity Type"]);
    //     oListItem.set_item('PercentageOfShare', jsonData[i]["% of Share (please indicate if local office is JV)"]);
    //     oListItem.set_item('CFSFacilityType', jsonData[i]["CFS Facility Type"]);
    //     oListItem.set_item('CFSFacilitySegregationArea', jsonData[i]["CFS Facility Segregation Mode"]);
    //     oListItem.set_item('ReceivingAgent', jsonData[i]["Receiving Agent"]);
    //     oListItem.update();
    //     itemArray.push(oListItem);
    //     clientContext.load(itemArray[itemArray.length - 1]);
    // }
    // clientContext.executeQueryAsync(updateMultipleListItemsSuccess, updateMultipleListItemsFailed);

}

function OriginLocalChargeUpdate() {

    // var itemArray = [];
    // var clientContext = new SP.ClientContext.get_current();
    // var oList = clientContext.get_web().get_lists().getByTitle(prefixTabsListName+'LocalCharge');

    // for (var i = 0; i < jsonData.length; i++) {
    //     //console.log();
    //     var oListItem = oList.getItemById(jsonData[i].ID);
    //     oListItem.set_item('RawChargeAmount_Min', jsonData[i]["Charge Amount Min"]);
    //     oListItem.set_item('RawChargeAmount_PerUOM', jsonData[i]["Charge Amount per UOM"]);
    //     oListItem.set_item('ChargeAmount_Min', jsonData[i]["Charge Amount Min"]);
    //     oListItem.set_item('ChargeAmount_PerUOM', jsonData[i]["Charge Amount per UOM"]);
    //     oListItem.update();
    //     itemArray.push(oListItem);
    //     clientContext.load(itemArray[itemArray.length - 1]);
    // }
    // clientContext.executeQueryAsync(updateMultipleListItemsSuccess, updateMultipleListItemsFailed);

    var updatedRecords = [];

    OriginLocalChargeJSON.forEach(element => {

        if(element.ID) {

            updatedRecords.push({
                "ID":element['ID'],
                "RawChargeAmount_Min":element["Charge Amount Min"],
                "RawChargeAmount_PerUOM":element["Charge Amount per UOM"],
                "ChargeAmount_Min":element["(US) Min"],
                "ChargeAmount_PerUOM":element["(US) per UOM"]

            })

        }
        
    });


    updateListItems(prefixTabsListName+'OriginLocalCharge', updatedRecords,
    function (items) {
        console.log("List Updated Successfully OriginLocalCharge");
    },
    function (sender, args) {
        alert('Error occured while creating tasks: OriginLocalCharge' + args.get_message());
    }
    );

    // for (var i = 0; i < jsonData.length; i++) {

    //     for (var j = 0; j < OriginLocalChargeJSON.length; j++) {

    //         if (jsonData[i]["ID"] == OriginLocalChargeJSON[j]["ID"]) {

    //             OriginLocalChargeJSON[j]["Charge Amount Min"] = jsonData[i]["Charge Amount Min"];
    //             OriginLocalChargeJSON[j]["Charge Amount per UOM"] = jsonData[i]["Charge Amount per UOM"];
    //             OriginLocalChargeJSON[j]["(US) Min"] = jsonData[i]["(US) Min"];
    //             OriginLocalChargeJSON[j]["(US) per UOM"] = jsonData[i]["(US) per UOM"];
    //         }
    //     }
    // }


}

function FirmCodeUpdate() {


    var updatedRecords = [];

    FirmCodeJSON.forEach(element => {

        if(element.ID) {

            updatedRecords.push({
                "ID":element['ID'],
                "FirmCode":element['Firm Code']

            })

        }
        
    });


    // NWF$(".firmCodeRepeater .nf-repeater-row").each(function (index) {

    //     if (index != 0) {
    //         var initialCondition = NWF$(".firmCodeRepeater .nf-repeater-row:eq("+ index + ")");

    //         if(initialCondition.find('.ID input').val()) {   
    //             updatedRecords.push({
    //                 "ID":initialCondition.find('.ID input').val(),
    //                 "FirmCode":initialCondition.find('.firmCodeSecond input').val(),
    //             })
    //         }    

    //     }
    // });

    updateListItems(prefixTabsListName+'FirmCode', updatedRecords,
    function (items) {
        console.log("List Updated Successfully FirmCode");
    },
    function (sender, args) {
        alert('Error occured while creating tasks: FirmCode' + args.get_message());
    }
    );

}

function AccessorialChargeUpdate() {
    
    var updatedRecords = [];


    NWF$(".accessorialChargeRepeater .nf-repeater-row").each(function (index) {
        if (index != 0) {
            var initialCondition = NWF$(".accessorialChargeRepeater .nf-repeater-row:eq("+ index + ")");

            if(initialCondition.find('.ID input').val()) {   
                updatedRecords.push({
                    "ID":initialCondition.find('.ID input').val(),
                    "RawChargeAmount_PerUOM":initialCondition.find('.accPerUom input').val(),
                })
            }    

        }
    });

    updateListItems(prefixTabsListName+'AccessorialCharge', updatedRecords,
    function (items) {
        console.log("List Updated Successfully AccessorialCharge");
    },
    function (sender, args) {
        alert('Error occured while creating tasks: AccessorialCharge' + args.get_message());
    }
    );

}

function AirRFQUpdate() {
     
    var updatedRecords = [];

    AirRFQJSON.forEach(element => {

        if(element.ID) {

            updatedRecords.push({
                "ID":element['ID'],
                "ATARate":element["ATA Rate/kg"],
                "LastMileRate_Zone1":element["Last Mile Rate/kg Zone 1"],
                "LastMileRate_Zone2":element["Last Mile Rate/kg Zone 2"],
                "LastMileRate_Zone3":element["Last Mile Rate/kg Zone 3"],
                "LastMileRate_Zone4":element["Last Mile Rate/kg Zone 4"],
                "LastMileRate_Zone5":element["Last Mile Rate/kg Zone 5"],
                "Surcharge_SSC":element["Surcharge SSC (fixed)"] == 'incl.'?'incl':element["Surcharge SSC (fixed)"],
                "Surcharge_FSC":element["Surcharge FSC (Vatos)"] == 'incl.'?'incl':element["Surcharge FSC (Vatos)"],
                "Surcharge_SSC_FSC":element["Surcharge Sea-Air SSC/FSC"] =='incl.'?'incl':element["Surcharge Sea-Air SSC/FSC"],
                "Transit_ATATT":element["Transit Solution ATA TT (GAC = Day 1)"],
                "Transit_ATDTT":element["Transit Solution ATD TT (GAC = Day 1)"],
                "Transit_Airline":element["Transit Solution Airline"],
                "ATDRate_Zone1":element["ATD Rate/kg Zone 1"],
                "ATDRate_Zone2":element["ATD Rate/kg Zone 2"],
                "ATDRate_Zone3":element["ATD Rate/kg Zone 3"],
                "ATDRate_Zone4":element["ATD Rate/kg Zone 4"],
                "ATDRate_Zone5":element["ATD Rate/kg Zone 5"],
                "IsNQ": element["NQ"]

            })

        }
        
    });

    //console.log(updatedRecords);
    //return;

    updateListItems(prefixTabsListName+'AirRFQ', updatedRecords,
    function (items) {
        console.log("List Updated Successfully AirRFQ");
        //NWF$(".nd-dialog-panel").css("display","none");
    },
    function (sender, args) {
        alert('Error occured while creating tasks: AirRFQ' + args.get_message());
        NWF$(".nd-dialog-panel").css("display","none");
    }
    );


}



function createListItems(listTitle, itemsProperties, OnItemsAdded, OnItemsError) {
    var context = new SP.ClientContext.get_current();
    var web = context.get_web();
    var list = web.get_lists().getByTitle(listTitle);

    var items = [];
    NWF$.each(itemsProperties, function (i, itemProperties) {

        var itemCreateInfo = new SP.ListItemCreationInformation();
        var listItem = list.addItem(itemCreateInfo);
        for (var propName in itemProperties) {
            listItem.set_item(propName, itemProperties[propName])
        }
        listItem.update();
        context.load(listItem);
        items.push(listItem);

    });

    context.executeQueryAsync(
        function () {
            OnItemsAdded(items);
        },
        OnItemsError
    );

}

function updateListItems(listTitle, itemsProperties, OnItemsAdded, OnItemsError) {
    var context = new SP.ClientContext.get_current();
    var web = context.get_web();
    var list = web.get_lists().getByTitle(listTitle);

    var items = [];
    NWF$.each(itemsProperties, function (i, itemProperties) {

        var listItem = list.getItemById(itemProperties['ID']);
        for (var propName in itemProperties) {

            //console.log(propName);
            if(propName !='ID') {
                listItem.set_item(propName, itemProperties[propName]);
            }
            
        }
        listItem.update();
        items.push(listItem);
        clientContext.load(items[items.length - 1]);

    });

    context.executeQueryAsync(
        function () {
            OnItemsAdded(items);
        },
        OnItemsError
    );

}

function deleteListItems(listTitle, itemsProperties, OnItemsAdded, OnItemsError) {
    var context = new SP.ClientContext.get_current();
    var web = context.get_web();
    var list = web.get_lists().getByTitle(listTitle);

    itemsProperties.forEach(element => {
        var listItem = list.getItemById(element);
        listItem.deleteObject();
    });

    context.executeQueryAsync(
        function () {
            OnItemsAdded();
        },
        OnItemsError
    );

}


RegExp.escape=function(r){return r.replace(/[-/\\^$*+?.()|[\]{}]/g,"\\$&")},function(){var r;(r="undefined"!=typeof NWF$&&NWF$?NWF$:{}).csv={defaults:{separator:",",delimiter:'"',headers:!0},hooks:{castToScalar:function(r,e){if(isNaN(r))return r;if(/\./.test(r))return parseFloat(r);var a=parseInt(r);return isNaN(a)?null:a}},parsers:{parse:function(r,e){var a=e.separator,t=e.delimiter;e.state.rowNum||(e.state.rowNum=1),e.state.colNum||(e.state.colNum=1);var o=[],s=[],n=0,i="",l=!1;function u(){if(n=0,i="",e.start&&e.state.rowNum<e.start)return s=[],e.state.rowNum++,void(e.state.colNum=1);if(void 0===e.onParseEntry)o.push(s);else{var r=e.onParseEntry(s,e.state);!1!==r&&o.push(r)}s=[],e.end&&e.state.rowNum>=e.end&&(l=!0),e.state.rowNum++,e.state.colNum=1}function c(){if(void 0===e.onParseValue)s.push(i);else if(e.headers&&1===e.state.rowNum)s.push(i);else{var r=e.onParseValue(i,e.state);!1!==r&&s.push(r)}i="",n=0,e.state.colNum++}var f=RegExp.escape(a),d=RegExp.escape(t),m=/(D|S|\r\n|\n|\r|[^DS\r\n]+)/,p=m.source;return p=(p=p.replace(/S/g,f)).replace(/D/g,d),m=new RegExp(p,"gm"),r.replace(m,function(r){if(!l)switch(n){case 0:if(r===a){i+="",c();break}if(r===t){n=1;break}if(/^(\r\n|\n|\r)$/.test(r)){c(),u();break}i+=r,n=3;break;case 1:if(r===t){n=2;break}i+=r,n=1;break;case 2:if(r===t){i+=r,n=1;break}if(r===a){c();break}if(/^(\r\n|\n|\r)$/.test(r)){c(),u();break}throw Error("CSVDataError: Illegal State [Row:"+e.state.rowNum+"][Col:"+e.state.colNum+"]");case 3:if(r===a){c();break}if(/^(\r\n|\n|\r)$/.test(r)){c(),u();break}if(r===t)throw Error("CSVDataError: Illegal Quote [Row:"+e.state.rowNum+"][Col:"+e.state.colNum+"]");throw Error("CSVDataError: Illegal Data [Row:"+e.state.rowNum+"][Col:"+e.state.colNum+"]");default:throw Error("CSVDataError: Unknown State [Row:"+e.state.rowNum+"][Col:"+e.state.colNum+"]")}}),0!==s.length&&(c(),u()),o},splitLines:function(e,a){if(e){var t=(a=a||{}).separator||r.csv.defaults.separator,o=a.delimiter||r.csv.defaults.delimiter;a.state=a.state||{},a.state.rowNum||(a.state.rowNum=1);var s=[],n=0,i="",l=!1,u=RegExp.escape(t),c=RegExp.escape(o),f=/(D|S|\n|\r|[^DS\r\n]+)/,d=f.source;return d=(d=d.replace(/S/g,u)).replace(/D/g,c),f=new RegExp(d,"gm"),e.replace(f,function(r){if(!l)switch(n){case 0:if(r===t){i+=r,n=0;break}if(r===o){i+=r,n=1;break}if("\n"===r){m();break}if(/^\r$/.test(r))break;i+=r,n=3;break;case 1:if(r===o){i+=r,n=2;break}i+=r,n=1;break;case 2:var e=i.substr(i.length-1);if(r===o&&e===o){i+=r,n=1;break}if(r===t){i+=r,n=0;break}if("\n"===r){m();break}if("\r"===r)break;throw Error("CSVDataError: Illegal state [Row:"+a.state.rowNum+"]");case 3:if(r===t){i+=r,n=0;break}if("\n"===r){m();break}if("\r"===r)break;if(r===o)throw Error("CSVDataError: Illegal quote [Row:"+a.state.rowNum+"]");throw Error("CSVDataError: Illegal state [Row:"+a.state.rowNum+"]");default:throw Error("CSVDataError: Unknown state [Row:"+a.state.rowNum+"]")}}),""!==i&&m(),s}function m(){if(n=0,a.start&&a.state.rowNum<a.start)return i="",void a.state.rowNum++;if(void 0===a.onParseEntry)s.push(i);else{var r=a.onParseEntry(i,a.state);!1!==r&&s.push(r)}i="",a.end&&a.state.rowNum>=a.end&&(l=!0),a.state.rowNum++}},parseEntry:function(r,e){var a=e.separator,t=e.delimiter;e.state.rowNum||(e.state.rowNum=1),e.state.colNum||(e.state.colNum=1);var o=[],s=0,n="";function i(){if(void 0===e.onParseValue)o.push(n);else{var r=e.onParseValue(n,e.state);!1!==r&&o.push(r)}n="",s=0,e.state.colNum++}if(!e.match){var l=RegExp.escape(a),u=RegExp.escape(t),c=/(D|S|\n|\r|[^DS\r\n]+)/.source;c=(c=c.replace(/S/g,l)).replace(/D/g,u),e.match=new RegExp(c,"gm")}return r.replace(e.match,function(r){switch(s){case 0:if(r===a){n+="",i();break}if(r===t){s=1;break}if("\n"===r||"\r"===r)break;n+=r,s=3;break;case 1:if(r===t){s=2;break}n+=r,s=1;break;case 2:if(r===t){n+=r,s=1;break}if(r===a){i();break}if("\n"===r||"\r"===r)break;throw Error("CSVDataError: Illegal State [Row:"+e.state.rowNum+"][Col:"+e.state.colNum+"]");case 3:if(r===a){i();break}if("\n"===r||"\r"===r)break;if(r===t)throw Error("CSVDataError: Illegal Quote [Row:"+e.state.rowNum+"][Col:"+e.state.colNum+"]");throw Error("CSVDataError: Illegal Data [Row:"+e.state.rowNum+"][Col:"+e.state.colNum+"]");default:throw Error("CSVDataError: Unknown State [Row:"+e.state.rowNum+"][Col:"+e.state.colNum+"]")}}),i(),o}},helpers:{collectPropertyNames:function(r){var e=[],a=[],t=[];for(e in r)for(a in r[e])r[e].hasOwnProperty(a)&&t.indexOf(a)<0&&"function"!=typeof r[e][a]&&t.push(a);return t}},toArray:function(e,a,t){if(void 0!==a&&"function"==typeof a){if(void 0!==t)return console.error("You cannot 3 arguments with the 2nd argument being a function");t=a,a={}}a=void 0!==a?a:{};var o={};o.callback=void 0!==t&&"function"==typeof t&&t,o.separator="separator"in a?a.separator:r.csv.defaults.separator,o.delimiter="delimiter"in a?a.delimiter:r.csv.defaults.delimiter;var s=void 0!==a.state?a.state:{};a={delimiter:o.delimiter,separator:o.separator,onParseEntry:a.onParseEntry,onParseValue:a.onParseValue,state:s};var n=r.csv.parsers.parseEntry(e,a);if(!o.callback)return n;o.callback("",n)},toArrays:function(e,a,t){if(void 0!==a&&"function"==typeof a){if(void 0!==t)return console.error("You cannot 3 arguments with the 2nd argument being a function");t=a,a={}}a=void 0!==a?a:{};var o={};o.callback=void 0!==t&&"function"==typeof t&&t,o.separator="separator"in a?a.separator:r.csv.defaults.separator,o.delimiter="delimiter"in a?a.delimiter:r.csv.defaults.delimiter;var s=[];if(void 0!==(a={delimiter:o.delimiter,separator:o.separator,onPreParse:a.onPreParse,onParseEntry:a.onParseEntry,onParseValue:a.onParseValue,onPostParse:a.onPostParse,start:a.start,end:a.end,state:{rowNum:1,colNum:1}}).onPreParse&&(e=a.onPreParse(e,a.state)),s=r.csv.parsers.parse(e,a),void 0!==a.onPostParse&&(s=a.onPostParse(s,a.state)),!o.callback)return s;o.callback("",s)},toObjects:function(e,a,t){if(void 0!==a&&"function"==typeof a){if(void 0!==t)return console.error("You cannot 3 arguments with the 2nd argument being a function");t=a,a={}}a=void 0!==a?a:{};var o={};o.callback=void 0!==t&&"function"==typeof t&&t,o.separator="separator"in a?a.separator:r.csv.defaults.separator,o.delimiter="delimiter"in a?a.delimiter:r.csv.defaults.delimiter,o.headers="headers"in a?a.headers:r.csv.defaults.headers,a.start="start"in a?a.start:1,o.headers&&a.start++,a.end&&o.headers&&a.end++;var s,n=[];a={delimiter:o.delimiter,separator:o.separator,onPreParse:a.onPreParse,onParseEntry:a.onParseEntry,onParseValue:a.onParseValue,onPostParse:a.onPostParse,start:a.start,end:a.end,state:{rowNum:1,colNum:1},match:!1,transform:a.transform};var i={delimiter:o.delimiter,separator:o.separator,start:1,end:1,state:{rowNum:1,colNum:1},headers:!0};void 0!==a.onPreParse&&(e=a.onPreParse(e,a.state));var l=r.csv.parsers.splitLines(e,i),u=r.csv.toArray(l[0],i);s=r.csv.parsers.splitLines(e,a),a.state.colNum=1,a.state.rowNum=u?2:1;for(var c=0,f=s.length;c<f;c++){for(var d=r.csv.toArray(s[c],a),m={},p=0;p<u.length;p++)m[u[p]]=d[p];void 0!==a.transform?n.push(a.transform.call(void 0,m)):n.push(m),a.state.rowNum++}if(void 0!==a.onPostParse&&(n=a.onPostParse(n,a.state)),!o.callback)return n;o.callback("",n)},fromArrays:function(e,a,t){if(void 0!==a&&"function"==typeof a){if(void 0!==t)return console.error("You cannot 3 arguments with the 2nd argument being a function");t=a,a={}}a=void 0!==a?a:{};var o={};o.callback=void 0!==t&&"function"==typeof t&&t,o.separator="separator"in a?a.separator:r.csv.defaults.separator,o.delimiter="delimiter"in a?a.delimiter:r.csv.defaults.delimiter;var s,n,i,l,u="";for(i=0;i<e.length;i++){for(s=e[i],n=[],l=0;l<s.length;l++){var c=void 0===s[l]||null===s[l]?"":s[l].toString();-1<c.indexOf(o.delimiter)&&(c=c.replace(new RegExp(o.delimiter,"g"),o.delimiter+o.delimiter));var f="\n|\r|S|D";f=(f=f.replace("S",o.separator)).replace("D",o.delimiter),-1<c.search(f)&&(c=o.delimiter+c+o.delimiter),n.push(c)}u+=n.join(o.separator)+"\n"}if(!o.callback)return u;o.callback("",u)},fromObjects:function(e,a,t){if(void 0!==a&&"function"==typeof a){if(void 0!==t)return console.error("You cannot 3 arguments with the 2nd argument being a function");t=a,a={}}a=void 0!==a?a:{};var o={};if(o.callback=void 0!==t&&"function"==typeof t&&t,o.separator="separator"in a?a.separator:r.csv.defaults.separator,o.delimiter="delimiter"in a?a.delimiter:r.csv.defaults.delimiter,o.headers="headers"in a?a.headers:r.csv.defaults.headers,o.sortOrder="sortOrder"in a?a.sortOrder:"declare",o.manualOrder="manualOrder"in a?a.manualOrder:[],o.transform=a.transform,"string"==typeof o.manualOrder&&(o.manualOrder=r.csv.toArray(o.manualOrder,o)),void 0!==o.transform){var s,n=e;for(e=[],s=0;s<n.length;s++)e.push(o.transform.call(void 0,n[s]))}var i,l,u=r.csv.helpers.collectPropertyNames(e);if("alpha"===o.sortOrder&&u.sort(),0<o.manualOrder.length){var c,f=[].concat(o.manualOrder);for(c=0;c<u.length;c++)f.indexOf(u[c])<0&&f.push(u[c]);u=f}var d,m=[];for(o.headers&&m.push(u),i=0;i<e.length;i++){for(l=[],c=0;c<u.length;c++)(d=u[c])in e[i]&&"function"!=typeof e[i][d]?l.push(e[i][d]):l.push("");m.push(l)}return r.csv.fromArrays(m,a,o.callback)}},r.csvEntry2Array=r.csv.toArray,r.csv2Array=r.csv.toArrays,r.csv2Dictionary=r.csv.toObjects,"undefined"!=typeof module&&module.exports&&(module.exports=r.csv)}.call(this);
