
var serviceLevelsArray = [];
var originDestinationArray = [];
var transitMatrixArray = [];
var loadGridFlag = false;

var increment_loop = 0;
var hold_dueDates = [];

var delRowFlag = false;

var mapPartnerIdList = [];
var mapCarrierCdList = [];

NWF$(document).ready(function () {
    //SP.SOD.executeFunc('sp.js', 'SP.ClientContext', retrieveAFF);
    NWF$("#" + DeptRole).change(function () {

        if (loadGridFlag == false) {
            retrieveAFFGrid();
        }

        // if(originDestinationArray.length ==0) {
        //     //retrieveMapOriginDestination();
        //     //retrieveServiceLevel();
        //     retrieveAirTransitMatrix();
        //     //retrieveDefaultSurcharge();
        // }
    });

    NWF$("#"+DeptRole).on('mousedown',function() {
        var usednames = {};
        //alert("here");
        NWF$("select[id="+DeptRole+"]>option").each(function(){
            if(usednames[this.text]){
                NWF$(this).remove();
            }
            else{
                usednames[this.text]=this.title;
            }
        });
    });
    

    NWF$(".checkAllRFQ input").change(function () {
        var isChecked = NWF$(this).is(":checked");
        NWF$(".afflists .nf-repeater-row").each(function (index) {
            if (index != 0) {
                NWF$(".afflists .nf-repeater-row:eq(" + index + ")").find('.CheckUnique input').prop('checked', isChecked);

            }
        });
    });






    NWF$(".affCodeGrid input").keyup(function () {

        console.log(NWF$(this).val());

        var valueKeyUp = NWF$(this).val().toUpperCase();

        NWF$(".afflists .nf-repeater-row").each(function (index) {
            if (index != 0) {
                var aff = NWF$(".afflists .nf-repeater-row:eq(" + index + ")").find('.AFFGrid input').val();
                var rfqCode = NWF$(".afflists .nf-repeater-row:eq(" + index + ")").find('.RFQCodeGrid input').val();

                if (aff.toUpperCase().indexOf(valueKeyUp) > -1 || rfqCode.toUpperCase().indexOf(valueKeyUp) > -1) {
                    NWF$(".afflists .nf-repeater-row:eq(" + index + ")").css("display", "");
                } else {
                    NWF$(".afflists .nf-repeater-row:eq(" + index + ")").css("display", "none");
                }

            }
        });

    });

    NWF$(".nf-repeater-deleterow-image").click(function () {

        // console.log(NWF$(this).prop('className'));

        if (NWF$(this).hasClass('customDelRow') && delRowFlag == true) {

            var dateVal = NWF$(this).prop('className').split("-_")[1];

            var index = hold_dueDates.indexOf(dateVal);

            if (index > -1) {
                hold_dueDates.splice(index, 1);
            }



        }

    });



});






function onQuerySucceeded1() {

    alert('Item created: ' + oListItem1.get_id());
}

function onQueryFailed(sender, args) {

    alert('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
}


function retrieveAFFGrid() {
    loadGridFlag = true;
    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle('LGS_STG_MapPartnerId');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View>" +
        "<Query>" +
        "<Where>" +
        "<Eq>" +
        "<FieldRef Name='ServiceType' />" +
        "<Value Type='Text'>AFF</Value>" +
        "</Eq>" +
        "</Where>" +
        "</Query>" +
        "</View>");
    //camlQuery.set_viewXml();
    this.collListItem = oList.getItems(camlQuery);
    clientContext.load(collListItem);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQuerySucceeded),
        Function.createDelegate(this, this.onQueryFailed)
    );
}
function onQuerySucceeded(sender, args) {

    // Redraw the existing table, remove everything what exists, leave fresh instance
    //redrawRepeatingTable();
    var itemArray = [];
    var listItemEnumerator = collListItem.getEnumerator();
    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();
        itemArray.push(oListItem.get_item('PartnerID'));

        mapPartnerIdList.push({
            PartnerID: oListItem.get_item('PartnerID'),
            Email: oListItem.get_item('Email'),
        })
        //var formID = oListItem.get_item('FormID'); 
        //NWF$(".afflists .nf-repeater-row:last").find('.formIDGrid input').val(formID);
        //NWF$(".afflists").find('a').click();
    }
    //NWF$(".afflists .nf-repeater-row:last").find('.nf-repeater-deleterow-image').click();

    showGrid(itemArray);
}

function showGrid(itemArray) {
    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle('LGS_STG_MapCarrierCd');
    var camlQuery = new SP.CamlQuery();

    var caml = "<View><Query>"
    var categoriesExpressions = itemArray.map(c => CamlBuilder.Expression().TextField("CarrierCD").EqualTo(c));

    caml += new CamlBuilder().Where()
        .TextField("Title").EqualTo('AIR')
        .And()
        .Any(categoriesExpressions)
        .ToString();

    caml += "</Query></View>"
    camlQuery.set_viewXml(caml);

    //camlQuery.set_viewXml();
    this.collListItemSecond = oList.getItems(camlQuery);
    clientContext.load(collListItemSecond);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQuerySucceededSecond),
        Function.createDelegate(this, this.onQueryFailed)
    );

}

function onQuerySucceededSecond(sender, args) {
    var listItemEnumerator = collListItemSecond.getEnumerator();
    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();
        //var formID = oListItem.get_item('FormID'); 
        NWF$(".afflists .nf-repeater-row:last").find('.AFFGrid input').val(oListItem.get_item('Carrier'));
        NWF$(".afflists .nf-repeater-row:last").find('.RFQCodeGrid input').val(oListItem.get_item('CarrierCodeRFQ'));
        NWF$(".afflists .nf-repeater-row:last").find('.nf-repeater-deleterow-image').css("visibility", "hidden");
        NWF$(".afflists").find('a').click();

        mapCarrierCdList.push({
            Carrier: oListItem.get_item('Carrier'),
            // CarrierCodeRFQ:oListItem.get_item('CarrierCodeRFQ'),
            CarrierCD: oListItem.get_item('CarrierCD')
        })
    }
    NWF$(".afflists .nf-repeater-row:last").find('.nf-repeater-deleterow-image').click();
}

function onQueryFailed() {
    alert("Failed");
}

function redrawRepeatingTable() {
    //delete all existing repeating table rows, then build them again
    NWF$(".afflists .nf-repeater-row").each(function (index) {
        if (index != 0) {
            NWF$(this).find('.nf-repeater-deleterow-image').click();
        }
    });
    NWF$(".afflists .nf-repeater-row:last").find('.AFFGrid input').val("");
}


function addReminderdate() {

    delRowFlag = false;
    // NWF$(".showHideDueDateRepeater").css("display", "block");

    var calender = NWF$(".calenderRFQ input").val();
    var sentEmail = parseInt(NWF$(".sentEmail").val());

    //console.log(isNaN(sentEmail));

    //console.log(calender);
    //console.log(sentEmail);

    var mydate = new Date(calender);

    console.log(mydate);

    if (mydate == 'Invalid Date' || isNaN(sentEmail)) {
        return;
    }

    var tomorrow = new Date(calender);

    tomorrow.setDate(mydate.getDate() + sentEmail);

    var increment_date = ((tomorrow.getMonth() > 8) ? (tomorrow.getMonth() + 1) : ('0' + (tomorrow.getMonth() + 1))) + '/' + ((tomorrow.getDate() > 9) ? tomorrow.getDate() : ('0' + tomorrow.getDate())) + '/' + tomorrow.getFullYear();

    console.log(increment_date);

    if (hold_dueDates.indexOf(increment_date) == -1) {
        hold_dueDates.push(increment_date);
    }

    var isDescending = false; //set to false for ascending
    //sort Dates
    hold_dueDates.sort((a, b) => isDescending ? new Date(b).getTime() - new Date(a).getTime() : new Date(a).getTime() - new Date(b).getTime());


    NWF$(".reminderList .nf-repeater-row").each(function (index) {
        NWF$(".reminderList .nf-repeater-row:last").find('.nf-repeater-deleterow-image').click();
    });

    hold_dueDates.forEach(function (item, index) {
        NWF$(".reminderList .nf-repeater-row:last").find('.IncrementDateValue input').val(item);
        NWF$(".reminderList .nf-repeater-row:last").find('.nf-repeater-deleterow-image').addClass('customDelRow delRow-_' + item);
        NWF$(".reminderList").find('a').click();
    });
    NWF$(".reminderList .nf-repeater-row:last").find('.nf-repeater-deleterow-image').click();

    delRowFlag = true;

    // var last_index;
    // NWF$(".reminderList .nf-repeater-row").each(function (index) {
    //     last_index = index;
    // });

    // if (increment_loop == 0) {
    //     increment_loop = increment_loop + 1;
    // } else {
    //     NWF$(".reminderList").find('a').click();
    //     increment_loop = increment_loop + 1;
    // }

    // NWF$(".reminderList .nf-repeater-row:last").find('.IncrementDateValue input').val(increment_date);


}


function submitButton() {

    var listItems = validateForm();


    if (listItems == null) {
        return;
    }

    var confirmBox = window.confirm("Are you sure to send to the selected parties?");
    if (!confirmBox) {
        return;
    }

    NWF$(".showHide").removeClass("showPanelCss");
    var valueAFF = [];
    listItems["ClientList"].forEach(function (item, index) {

        valueAFF.push({
            'ApplicationStatus': 'Submitted',
            'AFF': item,
            'MasterStatus': 'InProgress',
            'DueDate': listItems["DueDate"],
            'FormRole': listItems["RoleList"],
            'Position': NWF$(".getPosition input").val(),
            'Department': NWF$(".getDepartment input").val(),
            'Receiver': getReceiver(item)
        });

    });

    createListItems('MGF_RFQ_AFF_NewRequestFormList', valueAFF,
        function (items) {
            alert("List Updated Successfully");
            NWF$(".closeWithoutSaveAFF input").click();
        },
        function (sender, args) {
            console.log('Error occured while creating tasks:' + args.get_message());
        }
    );


}

function createRecord(valueAFF) {



    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle('MGF_RFQ_AFF_NewRequestFormList');

    var itemCreateInfo = new SP.ListItemCreationInformation();
    this.oListItem1 = oList.addItem(itemCreateInfo);
    oListItem1.set_item('ApplicationStatus', 'Submitted');
    oListItem1.set_item('MasterStatus', 'InProgress');
    // oListItem1.set_item('MasterStatus', 'InProgress');
    oListItem1.set_item('AFF', valueAFF);
    //oListItem1.set_item('Body', 'Hello World!');

    oListItem1.update();

    clientContext.load(oListItem1);

    clientContext.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded1), Function.createDelegate(this, this.onQueryFailed));
}




function createListItems(listTitle, itemsProperties, OnItemsAdded, OnItemsError) {
    var context = new SP.ClientContext.get_current();
    var web = context.get_web();
    var list = web.get_lists().getByTitle(listTitle);

    var items = [];
    NWF$.each(itemsProperties, function (i, itemProperties) {

        var itemCreateInfo = new SP.ListItemCreationInformation();
        var listItem = list.addItem(itemCreateInfo);
        for (var propName in itemProperties) {
            listItem.set_item(propName, itemProperties[propName])
        }
        listItem.update();
        context.load(listItem);
        items.push(listItem);

    });

    context.executeQueryAsync(
        function () {
            OnItemsAdded(items);
        },
        OnItemsError
    );

}

function validateForm() {

    var data = [];

    data["RoleList"] = NWF$("#" + DeptRole).find("option:selected").text();
    if (!data["RoleList"]) {
        window.alert("Please select one user role.");
        return null;
    }

    data["DueDate"] = NWF$("#" + dueDate).val()
    if (!data["DueDate"]) {
        window.alert("Please fill in the due date.");
        return null;
    }

    data["DueDates"] = NWF$(".reminderList .nf-repeater-row:last").find('.IncrementDateValue input').val();
    if (!data["DueDates"]) {
        window.alert("Please add at least one due date.");
        return null;
    }

    var valueAFF = [];
    NWF$(".afflists .nf-repeater-row").each(function (index) {
        if (index != 0) {
            var status = NWF$(".afflists .nf-repeater-row:eq(" + index + ")").find('.CheckUnique input').is(":checked");
            if (status == true) {
                var AffCheckedValue = NWF$(".afflists .nf-repeater-row:eq(" + index + ")").find('.AFFGrid input').val();
                valueAFF.push(AffCheckedValue);

            }

        }
    });

    data["ClientList"] = valueAFF;
    if (data["ClientList"].length <= 0) {
        window.alert("Please select at least one party.");
        return null;

    }
    return data;

}

function retrieveMapOriginDestination() {

    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle('LGS_ARFQ_MapOriginDestination');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml();
    this.originDestination = oList.getItems(camlQuery);
    clientContext.load(originDestination);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQuerySucceededOriginDestn),
        Function.createDelegate(this, this.onQueryFailed)
    );

}

function onQuerySucceededOriginDestn(sender, args) {

    originDestinationArray = [];
    var listItemEnumerator = originDestination.getEnumerator();
    //console.log(listItemEnumerator);
    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();
        originDestinationArray.push({
            'ID': oListItem.get_item('ID'),
            'country': oListItem.get_item('_x004b_ey3'),
            'city': oListItem.get_item('_x004b_ey2'),
            'region': oListItem.get_item('_x004b_ey1'),
            'Value0': oListItem.get_item('Value0'),
            'Value1': oListItem.get_item('Value1'),
            'Value2': oListItem.get_item('Value2'),
            'Value3': oListItem.get_item('Value3'),
            'Value4': oListItem.get_item('Value4'),
            'Value5': oListItem.get_item('Value5'),
            'Value6': oListItem.get_item('Value6'),
        })
    }




}


function retrieveServiceLevel() {

    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle('LGS_ARFQ_ServiceLevel');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml();
    this.serviceLevels = oList.getItems(camlQuery);
    clientContext.load(serviceLevels);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQuerySucceededServiceLevels),
        Function.createDelegate(this, this.onQueryFailed)
    );

}


function onQuerySucceededServiceLevels(sender, args) {

    serviceLevelsArray = [];
    var listItemEnumerator = serviceLevels.getEnumerator();
    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();
        serviceLevelsArray.push(oListItem.get_item('Title'))
    }

}

function retrieveAirTransitMatrix() {

    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getByTitle('LGS_STG_MapAirTransitMatrix');
    var camlQuery = new SP.CamlQuery();

    camlQuery.set_viewXml("<View>" +
        "<Query>" +
        "<GroupBy Collapse='TRUE'>" +
        "<FieldRef Name='Load_x0020_Port_x0020_Name' />" +
        "</GroupBy>" +
        "<GroupBy Collapse='TRUE'>" +
        "<FieldRef Name='Gateway' />" +
        "</GroupBy>" +
        "</Query>" +
        "</View>");

    //camlQuery.set_viewXml();
    this.transitMatrix = oList.getItems(camlQuery);
    clientContext.load(transitMatrix);
    clientContext.executeQueryAsync(
        Function.createDelegate(this, this.onQuerySucceededTransitMatrix),
        Function.createDelegate(this, this.onQueryFailed)
    );

}

function onQuerySucceededTransitMatrix(sender, args) {

    transitMatrixArray = [];
    var listItemEnumerator = transitMatrix.getEnumerator();
    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();
        transitMatrixArray.push({
            'country': oListItem.get_item('Country'),
            'loadPortName': oListItem.get_item('Load_x0020_Port_x0020_Name'),
            'gateway': oListItem.get_item('Gateway'),
            'EXPATATT': oListItem.get_item('EXPATATT'),
            'EXPATDTT': oListItem.get_item('EXPATDTT'),
            'STDATATT': oListItem.get_item('STDATATT'),
            'STDATDTT': oListItem.get_item('STDATDTT'),
            'ECOATATT': oListItem.get_item('ECOATATT'),
            'ECOATDTT': oListItem.get_item('ECOATDTT'),
            'DEFATATT': oListItem.get_item('DEFATATT'),
            'DEFATDTT': oListItem.get_item('DEFATDTT'),
            'SEAAIRATATT': oListItem.get_item('SEAAIRATATT'),
            'SEAAIRATDTT': oListItem.get_item('SEAAIRATDTT')

        })
    }

    console.log(transitMatrixArray);

}


function buildAirRFQRecords() {

    var OriginalArray = [];

    for (let index = 1; index < originDestinationArray.length; index++) {
        const element = originDestinationArray[index];

        for (let inc = 0; inc < 7; inc++) {

            if (element["Value" + inc] == 'Yes') {
                OriginalArray.push({
                    "Region": element['region'],
                    "OriginCountry": element['country'],
                    "OriginCity": element['city'],
                    "Dstn": originDestinationArray[0]["Value" + inc].split(" ")[0]
                })

            }

        }

    }


    var airRFQ = []

    serviceLevelsArray.forEach(iterator => {

        for (let index = 0; index < OriginalArray.length; index++) {
            const element = OriginalArray[index];
            airRFQ.push({
                "Region": element['Region'],
                "OriginCountry": element['OriginCountry'],
                "OriginCity": element['OriginCity'],
                "Dstn": element['Dstn'],
                "ServiceLevel": iterator
            })

            // airRFQ.push(element);
            // element['ServiceLevel'] = iterator;
            //console.log(element);
            // airRFQ.push(element)

        }

    });


    // console.log(airRFQ);
    console.log(new Date());
    // return;

    createListItems('MGF_RFQ_AFF_AirRFQ', airRFQ,
        function (items) {
            alert("List Updated Successfully");
            console.log(new Date());
        },
        function (sender, args) {
            console.log('Error occured while creating tasks:' + args.get_message());
        }
    );

    //console.log(airRFQ);

}

//mapPartnerIdList
function getReceiver(aff) {

    var carrCD = mapCarrierCdList.filter(element => element.Carrier == aff).map(element => element.CarrierCD);

    var email = mapPartnerIdList.filter(element => element.PartnerID == carrCD).map(element => element.Email);

    return email.toString();


}